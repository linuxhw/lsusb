Most popular USB devices in Notebooks
=====================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in Notebooks ](#usb-devices)
   * [ Audio ](#audio-usb)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Card reader ](#card-reader-usb)
   * [ Cdc data ](#cdc-data-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Chipcard ](#chipcard-usb)
   * [ Converter ](#converter-usb)
   * [ Disk ](#disk-usb)
   * [ Dvb card ](#dvb-card-usb)
   * [ Fingerprint reader ](#fingerprint-reader-usb)
   * [ Gamepad ](#gamepad-usb)
   * [ Hardware key ](#hardware-key-usb)
   * [ Hasp ](#hasp-usb)
   * [ Hub ](#hub-usb)
   * [ Human interface ](#human-interface-usb)
   * [ Imaging ](#imaging-usb)
   * [ Infrared ](#infrared-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Isdn adapter ](#isdn-adapter-usb)
   * [ Joystick ](#joystick-usb)
   * [ Mfp ](#mfp-usb)
   * [ Miscellaneous ](#miscellaneous-usb)
   * [ Modem ](#modem-usb)
   * [ Monitor ](#monitor-usb)
   * [ Net/wimax ](#netwimax-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Phone ](#phone-usb)
   * [ Printer ](#printer-usb)
   * [ Scanner ](#scanner-usb)
   * [ Serial controller ](#serial-controller-usb)
   * [ Smartcard reader ](#smartcard-reader-usb)
   * [ Sound ](#sound-usb)
   * [ Touchpad ](#touchpad-usb)
   * [ Touchscreen ](#touchscreen-usb)
   * [ Tv card ](#tv-card-usb)
   * [ Ups ](#ups-usb)
   * [ Video ](#video-usb)
   * [ Webcam ](#webcam-usb)
   * [ Wireless ](#wireless-usb)
   * [ Others ](#others-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Audio (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0955:7002 | Nvidia     | stereo controller            | 8     |            | A1B30D6B32 |
| 1043:857c | iCreate... | Xonar U7                     | 4     | snd_usb... | 657D8A85B3 |
| 041e:322c | Creativ... | SB Omni Surround 5.1         | 2     | snd_usb... | 2933DDC278 |
| 041e:3f02 | Creativ... | E-Mu 0202                    | 2     | snd_usb... | 877A9F179D |
| 0582:012f | Roland     | QUAD-CAPTURE                 | 2     | snd_usb... | 8ADE04AF96 |
| 1235:8202 | Focusri... | Scarlett 2i2 USB             | 2     | snd_usb... | 2D0E845055 |
| 1235:8205 | Focusri... | Scarlett Solo USB            | 2     | snd_usb... | AB69AA73CD |
| 041e:3f04 | Creativ... | E-Mu 0404                    | 1     | snd_usb... | F380930D83 |
| 041e:3f19 | Creativ... | E-MU 0204 / USB              | 1     | snd_usb... | 933BBAB7DA |
| 0499:1704 | Yamaha     | Steinberg UR44               | 1     | snd_usb... | 652B5E7253 |
| 0499:170b | Yamaha     | Steinberg UR242              | 1     | snd_usb... | AE4C0F8B6B |
| 0582:00e6 | Roland     | EDIROL UA-25EX (Advanced ... | 1     | snd_usb... | 2965F0AE13 |
| 0644:8030 | TEAC       | US-1800                      | 1     |            | 5D4A91211F |
| 0644:8043 | TEAC       | UD-501                       | 1     | usbhid     | 71D8772E62 |
| 0763:201a | M-Audio    | M-Audio Micro                | 1     |            | 4AFB8F7991 |
| 0763:400b | Midiman    | M-Track 2X2                  | 1     | snd_usb... | BE604B2A9C |
| 08e4:0165 | Pioneer    | USB Audio Device             | 1     | snd_usb... | 2F1A3868EA |
| 0b05:17f5 | ASUSTek... | ASUS XONAR U5                | 1     | snd_usb... | E8E7E33BB2 |
| 0d8c:0004 | C-Media... | USB2.0 High-Speed True HD... | 1     | snd_usb... | 586F0C90D9 |
| 1235:8016 | Focusri... | Focusrite Scarlett 2i2       | 1     | snd_usb... | CE863BAC18 |
| 1397:0507 | BEHRING... | UMC202HD 192k                | 1     | snd_usb... | 31261D00B1 |
| 1397:0509 | BEHRING... | UMC404HD 192k                | 1     | snd_usb... | 7CDD355784 |
| 152a:85d3 | Thesyco... | Audinst HUD-DX1              | 1     | snd_usb... | 1B1C811590 |
| 152a:85dd | Thesyco... | SMSL Sanskrit 10th           | 1     | snd_usb... | E3F88D9448 |
| 20b1:0008 | XMOS       | Audio                        | 1     | snd_usb... | CBFF9F25B0 |
| 22e8:dac2 | Cambrid... | USB Audio 2.0                | 1     | snd_usb... | 7984717E58 |
| 22e8:dac6 | Unknown... | DacMagicXS 2.0               | 1     | snd_usb... | 0C93611C78 |
| 249c:930b | M2Tech     | hiFaceTWO UAC2               | 1     | snd_usb... | 53D87B0DB3 |
| 2972:0001 | SmartAc... | FiiO USB Audio Class 2.0 DAC | 1     | snd_usb... | FBC2F0A547 |

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8087:0a2a | Intel      | Bluetooth Device             | 455   | btusb      | 6F94FCD1C0 |
| 0cf3:3005 | Qualcom... | AR3011 Bluetooth             | 451   | btusb      | 3ACD13490F |
| 8087:07dc | Intel      | Bluetooth Device             | 381   | btusb      | 7637F3DE5F |
| 0cf3:3004 | Qualcom... | AR3012 Bluetooth 4.0         | 372   | ath3k, ... | 53302E45B6 |
| 8087:0a2b | Intel      | Bluetooth Device             | 339   | btusb      | C53DF5F083 |
| 8087:07da | Intel      | Bluetooth Device             | 260   | btusb      | 7C0CD24986 |
| 8086:0189 | Intel      | Bluetooth Device             | 207   | btusb      | D2006E7A87 |
| 04ca:300b | Lite-On... | Lite-On Bluetooth Device     | 173   | ath3k, ... | 50FFA5DCEC |
| 105b:e065 | Foxconn... | BCM43142A0 Bluetooth Module  | 161   | btusb      | 06603D3DCE |
| 13d3:3362 | IMC Net... | Atheros AR3012 Bluetooth ... | 156   | ath3k, ... | 92AE8C0F3B |
| 0a12:0001 | Cambrid... | Bluetooth Dongle (HCI mode)  | 153   | btusb      | 279CF88917 |
| 0a5c:219c | Broadcom   | BCM2070 Bluetooth Device     | 134   | btusb      | 3A0FEA4700 |
| 0bda:b009 | Realtek... | 802.11n WLAN Adapter         | 134   | btusb      | D7977A3B0E |
| 0bda:b728 | Realtek... | Bluetooth Radio              | 133   | btusb      | 00C22F3924 |
| 8087:0aaa | Intel      | Bluetooth Device 9460/9560   | 133   | btusb      | 3A4D90A1C3 |
| 03f0:171d | Hewlett... | Bluetooth 2.0 Interface [... | 131   | btusb      | 4A6A073320 |
| 04ca:3015 | Lite-On... | Lite-On Bluetooth Device     | 130   | btusb      | 6E04F26B23 |
| 0489:e04e | Foxconn... | Bluetooth Device             | 126   | ath3k, ... | DE168F8DCC |
| 0a5c:217f | Broadcom   | BCM2045B (BDC-2.1)           | 122   | btusb      | FBF8462F41 |
| 0a5c:2101 | Broadcom   | BCM2045 Bluetooth            | 115   | btusb      | 74624FF493 |
| 03f0:231d | Hewlett... | Broadcom 2070 Bluetooth C... | 111   | btusb      | AB17752168 |
| 0a5c:21b4 | Broadcom   | BCM2070 Bluetooth 2.1 + EDR  | 108   | btusb      | 23E7475693 |
| 0cf3:0036 | Qualcom... | Qualcomm Atheros Bluetoot... | 96    | ath3k, ... | 6BC169574C |
| 8087:0aa7 | Intel      | Bluetooth Device             | 94    | btusb      | D0C54EF6E4 |
| 0cf3:311d | Qualcom... | Bluetooth USB Host Contro... | 88    | ath3k, ... | AB69AA73CD |
| 0cf3:e005 | Qualcom... | Qualcomm Atheros Bluetoot... | 82    | ath3k, ... | A38D1A4CE9 |
| 0930:0508 | Toshiba    | Integrated Bluetooth HCI     | 81    | btusb      | 97D9353E2A |
| 0bda:b002 | Realtek... | Bluetooth Radio              | 80    | btusb      | D607079392 |
| 0a5c:21e3 | Broadcom   | HP Portable Valentine        | 79    | btusb      | 42DF53B120 |
| 0b05:1712 | ASUSTek... | BT-183 Bluetooth 2.0+EDR ... | 79    | btusb      | 4137AC8F54 |
| 0b05:1788 | ASUSTek... | BT-270 Bluetooth Adapter     | 78    | btusb      | 49783F833C |
| 0489:e00d | Foxconn... | Broadcom Bluetooth 2.1 De... | 74    | btusb      | 62697BF35B |
| 0a5c:21e6 | Broadcom   | BCM20702 Bluetooth 4.0 [T... | 74    | btusb      | 6D5F1234C2 |
| 0cf3:e500 | Qualcom... | Qualcomm Atheros Bluetoot... | 73    | btusb      | DE5FB421D9 |
| 0bda:b721 | Realtek... | Bluetooth Radio              | 71    | btusb      | 8BBDB85BE4 |
| 413c:8187 | Dell       | DW375 Bluetooth Module       | 68    | btusb      | 8C536BE4D8 |
| 0489:e00f | Foxconn... | Foxconn T77H114 BCM2070 [... | 64    | btusb      | 801A0452F8 |
| 0b05:1751 | ASUSTek... | BT-253 Bluetooth Adapter     | 63    | btusb      | 96E310C22E |
| 0bda:b001 | Realtek... | Bluetooth Radio              | 62    | btusb      | 4B70A9D252 |
| 0bda:8723 | Realtek... | RT Bluetooth Radio           | 61    | btusb      | 891DE458D7 |
| 13d3:3423 | IMC Net... | Bluetooth Device             | 57    | ath3k, ... | F8FDA4EFC3 |
| 04ca:3006 | Lite-On... | Lite-On Bluetooth Device     | 54    | ath3k, ... | BA9573630E |
| 04ca:2006 | Lite-On... | BCM43142A0 Bluetooth Module  | 53    | btusb      | 2A224752BA |
| 0cf3:e360 | Qualcom... | Qualcomm Atheros Bluetoot... | 49    | btusb      | 7957C03703 |
| 0a5c:21d7 | Broadcom   | BCM43142 Bluetooth 4.0       | 47    | btusb      | 67CD98E6C7 |
| 13d3:3315 | IMC Net... | Bluetooth module             | 47    | btusb      | 4ADFAB3C4E |
| 13d3:3402 | IMC Net... | Bluetooth USB Host Contro... | 46    | ath3k, ... | 2D1845C282 |
| 0489:e036 | Foxconn... | Bluetooth USB Host Contro... | 44    | ath3k, ... | B56B7F6394 |
| 0cf3:e300 | Qualcom... | Qualcomm Atheros Bluetoot... | 43    | btusb      | 3A71C37AE2 |
| 0a5c:2145 | Broadcom   | BCM2045B (BDC-2.1) [Bluet... | 42    | btusb      | A23E000798 |
| 8087:0025 | Intel      | Bluetooth Device             | 42    | btusb      | 94C3F1BC72 |
| 0930:021d | Toshiba    | RT Bluetooth Radio           | 41    | btusb      | 806B890CCF |
| 0a5c:216d | Broadcom   | BCM43142A0 Bluetooth 4.0     | 41    | btusb      | 2EC22FA87A |
| 0cf3:e004 | Qualcom... | Bluetooth USB Host Contro... | 41    | ath3k, ... | F72534E2D2 |
| 0cf3:e009 | Qualcom... | Qualcomm Atheros Bluetoot... | 41    | btusb      | 9402076861 |
| 13d3:3394 | IMC Net... | Bluetooth                    | 41    | btusb      | 3B5C8ADA46 |
| 148f:1000 | Ralink ... | Motorola BC4 Bluetooth 3.... | 41    | btusb      | 36FF276D26 |
| 413c:8140 | Dell       | Wireless 360 Bluetooth       | 41    | btusb      | F205356420 |
| 0bda:b008 | Realtek... | Bluetooth Radio              | 40    | btusb      | 1CC2218397 |
| 13d3:3496 | IMC Net... | Bluetooth Device             | 40    | btusb      | 7D2B995B44 |
| 0bda:b00a | Realtek... | Bluetooth Radio              | 39    | btusb      | 4F9294F4B5 |
| 413c:8126 | Dell       | Wireless 355 Bluetooth       | 39    | btusb      | D400943350 |
| 04ca:3010 | Lite-On... | Lite-On Bluetooth Device     | 37    | ath3k, ... | FDD61F096B |
| 0a5c:2110 | Broadcom   | BCM2045B (BDC-2) [Bluetoo... | 37    | btusb      | 8457BB5791 |
| 0a5c:21e1 | Broadcom   | HP Portable SoftSailing      | 37    | btusb      | 68A86E6E3F |
| 0489:e078 | Foxconn... | Bluetooth Device             | 36    | ath3k, ... | 1C77D7A584 |
| 04ca:3005 | Lite-On... | Bluetooth USB Host Contro... | 36    | ath3k, ... | A1A1F1965A |
| 0bda:0821 | Realtek... | Bluetooth Radio              | 36    | btusb      | 3E47232411 |
| 0930:0214 | Toshiba    | Askey Bluetooth Module       | 35    | btusb      | BD758B3E12 |
| 044e:3017 | Alps El... | BCM2046 Bluetooth Device     | 33    | btusb      | A3C4D07088 |
| 04ca:3016 | Lite-On... | Lite-On Bluetooth Device     | 33    | btusb      | C80CAABFC9 |
| 0bda:c024 | Realtek... | Bluetooth Radio              | 33    | btusb      | C61C9E33F6 |
| 13d3:3408 | IMC Net... | Bluetooth Device             | 33    | ath3k, ... | 412366312B |
| 0a5c:21f4 | Broadcom   | BCM20702A0                   | 31    | btusb      | FA98476936 |
| 0489:e032 | Foxconn... | Broadcom BCM20702 Bluetooth  | 30    | btusb      | F3D7E4E13D |
| 0a5c:21bc | Broadcom   | BCM2070 Bluetooth 2.1 + EDR  | 29    | btusb      | 11784493FE |
| 0cf3:e007 | Qualcom... | Qualcomm Atheros Bluetoot... | 29    | btusb      | EB49973873 |
| 413c:8197 | Dell       | Dell Wireless 380 Bluetoo... | 29    | btusb      | 8C26DF88EE |
| 0bda:b006 | Realtek... | Bluetooth Radio              | 28    | btusb      | 3F7F72D7A2 |
| 0cf3:3121 | Qualcom... | Qualcomm Atheros Bluetoot... | 28    | ath3k, ... | EA670C7B0D |
| 13d3:3526 | IMC Net... | Bluetooth Radio              | 28    | btusb      | B6B40DE397 |
| 0489:e062 | Foxconn... | BCM43142A0                   | 26    | btusb      | 66E469F722 |
| 0a5c:2150 | Broadcom   | BCM2046 Bluetooth Device     | 26    | btusb      | 601D53F9EB |
| 0cf3:3008 | Qualcom... | Bluetooth (AR3011)           | 26    | ath3k, ... | 3CCACD3312 |
| 04ca:3014 | Lite-On... | Qualcomm Atheros Bluetooth   | 25    | ath3k, ... | D91699583D |
| 13d3:3304 | IMC Net... | Asus Integrated Bluetooth... | 24    | ath3k, ... | DB1BD4EC31 |
| 148f:2000 | Ralink ... | CSR BS8510                   | 23    | btusb      | ED98A45D81 |
| 0489:e069 | Foxconn... | BT                           | 22    | mt76xx     | 91D9C778CB |
| 0489:e0a2 | Foxconn... | Bluetooth Device             | 22    | btusb      | 5D2CB1E1B0 |
| 13d3:3490 | IMC Net... | Bluetooth Device             | 22    | ath3k, ... | 949FD12104 |
| 0489:e046 | Foxconn... | BCM20702A0                   | 21    | btusb      | 2DABFA1484 |
| 04ca:2007 | Lite-On... | Broadcom BCM43142A0 Bluet... | 21    | btusb      | 65D329A2C6 |
| 05ac:821d | Apple      | Bluetooth USB Host Contro... | 21    | btusb      | 16BDB52C40 |
| 0a5c:216c | Broadcom   | BCM43142A0 Bluetooth Device  | 21    | btusb      | DC6C804E81 |
| 0930:0220 | Toshiba    | Bluetooth Device             | 20    | ath3k, ... | D4EC4EAE87 |
| 413c:8160 | Dell       | Wireless 365 Bluetooth       | 20    | btusb      | 6722706FA2 |
| 05ac:8213 | Apple      | Bluetooth Host Controller    | 19    | btusb      | 9F753AC669 |
| 0930:0219 | Toshiba    | Bluetooth USB Host Contro... | 19    | ath3k, ... | 4038A574AC |
| 0a5c:219b | Broadcom   | Bluetooth 2.1 Device         | 19    | btusb      | 20D371D0D1 |
| 0bda:b00b | Realtek... | Bluetooth Radio              | 19    | btusb      | 0BEFDCEA98 |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 5986:0295 | Acer       | Lenovo EasyCamera            | 141   | uvcvideo   | AE0E410A7F |
| 1bcf:2c18 | Sunplus... | HD WebCam                    | 136   | uvcvideo   | 11B9C937C9 |
| 13d3:5a01 | IMC Net... | USB2.0 VGA UVC WebCam        | 115   | uvcvideo   | 0FAD7AC4EB |
| 064e:a103 | Suyin      | Acer/HP Integrated Webcam... | 112   | uvcvideo   | 77E3B20E26 |
| 13d3:5710 | IMC Net... | UVC VGA Webcam               | 111   | uvcvideo   | 826F5492EB |
| 0402:9665 | ALi        | Gateway Webcam               | 101   | uvcvideo   | D023F50697 |
| 058f:a014 | Alcor M... | Asus Integrated Webcam       | 101   | uvcvideo   | 6B25B8982D |
| 13d3:5130 | IMC Net... | Integrated Webcam            | 98    | uvcvideo   | 527313B5FF |
| 1bcf:2883 | Sunplus... | ASUS USB2.0 Webcam           | 91    | uvcvideo   | 92AE8C0F3B |
| 2232:1020 | Silicon... | WebCam SC-0311139N           | 91    | uvcvideo   | EE56C112BB |
| 04f2:b071 | Chicony... | 2.0M UVC Webcam / CNF7129    | 86    | uvcvideo   | 2D4D5EFCB2 |
| 1210:25f4 | DigiTech   | USB 2.0 PC Camera            | 84    | uvcvideo   | BC68546696 |
| 04f2:b272 | Chicony... | Lenovo EasyCamera            | 82    | uvcvideo   | BD698398A7 |
| 04f2:b1d6 | Chicony... | CNF9055 Toshiba Webcam       | 78    | uvcvideo   | FE2AB22987 |
| 0bda:57b5 | Realtek... | USB Camera                   | 76    | uvcvideo   | FDD61F096B |
| 13d3:5702 | IMC Net... | UVC VGA Webcam               | 76    | uvcvideo   | B658C90327 |
| 2232:1008 | Silicon... | WebCam SCB-1100N             | 73    | uvcvideo   | 82EA1BF753 |
| 04f2:b47f | Chicony... | VGA Webcam                   | 72    | uvcvideo   | A1D2E02773 |
| 04f2:b1d8 | Chicony... | 1.3M WebCam                  | 69    | uvcvideo   | E01CCE8165 |
| 04f2:b008 | Chicony... | USB 2.0 Camera               | 67    | uvcvideo   | 2664F9638B |
| 0bda:5728 | Realtek... | Lenovo EasyCamera            | 67    | uvcvideo   | 4FC85D1C31 |
| 13d3:5188 | IMC Net... | USB2.0 UVC HD Webcam         | 67    | uvcvideo   | 2D1845C282 |
| 04f2:b40a | Chicony... | USB2.0 HD UVC WebCam         | 66    | uvcvideo   | F71E42FC3C |
| 064e:a101 | Suyin      | Acer CrystalEye Webcam       | 65    | uvcvideo   | 5A52F8FD85 |
| 0c45:62c0 | Microdia   | Sonix USB 2.0 Camera         | 65    | uvcvideo   | 4A6A073320 |
| 13d3:5711 | IMC Net... | USB 2.0 UVC VGA WebCam       | 65    | uvcvideo   | A84D413088 |
| 064e:a219 | Suyin      | 1.3M WebCam (notebook ema... | 63    | uvcvideo   | 690FFF1815 |
| 04f2:b374 | Chicony... | HD WebCam                    | 62    | uvcvideo   | DE3BBCF659 |
| 04f2:b337 | Chicony... | HD WebCam                    | 61    | uvcvideo   | E67DFC255A |
| 2232:1006 | Image P... | WebCam SCB-0355N             | 61    | uvcvideo   | 137FB608D0 |
| 05c8:021e | Cheng U... | HP Webcam-101                | 60    | uvcvideo   | 7306A03690 |
| 2232:1028 | Silicon... | WebCam SC-03FFL11939N        | 59    | uvcvideo   | 359A01778D |
| 5986:0652 | Acer       | Lenovo EasyCamera            | 59    | uvcvideo   | 8FF0A9A350 |
| 04e8:6860 | Samsung... | GT-I9100 Phone [Galaxy S ... | 58    | usbfs      | D7977A3B0E |
| 04f2:b52b | Chicony... | USB2.0 VGA UVC WebCam        | 56    | uvcvideo   | 5AEE6B7CA7 |
| 0ac8:c33f | Z-Star ... | Webcam                       | 56    | uvcvideo   | 3A0FEA4700 |
| 058f:5608 | Alcor M... | USB 2.0 Web Camera           | 53    | uvcvideo   | 4414412FCB |
| 058f:b002 | Alcor M... | Acer Integrated Webcam       | 52    | uvcvideo   | 4C990A61B6 |
| 0bda:57b3 | Realtek... | Acer 640 x 480 laptop camera | 51    | uvcvideo   | 2619209EC5 |
| 0bda:57de | Realtek... | USB2.0 VGA UVC WebCam        | 50    | uvcvideo   | F8FDA4EFC3 |
| 04f2:b044 | Chicony... | Acer CrystalEye Webcam       | 49    | uvcvideo   | 6029A4A18A |
| 5986:0292 | Acer       | Lenovo EasyCamera            | 49    | uvcvideo   | F9F3745636 |
| 04f2:b230 | Chicony... | Integrated HP HD Webcam      | 48    | uvcvideo   | 15E5D2BB9D |
| 2232:1029 | Silicon... | WebCam SC-13HDL11939N        | 48    | uvcvideo   | 0F042024B4 |
| 04f2:b1e5 | Chicony... | USB2.0 0.3M UVC WebCam       | 47    | uvcvideo   | F47F34752E |
| 04f2:b469 | Chicony... | HD WebCam                    | 46    | uvcvideo   | 8B3744250A |
| 0408:a060 | Quanta ... | HD WebCam                    | 44    | uvcvideo   | C80CAABFC9 |
| 04f2:b43b | Chicony... | USB 2.0 Camera               | 44    | uvcvideo   | D607079392 |
| 04f2:b52d | Chicony... | HP Webcam                    | 44    | uvcvideo   | 1CC2218397 |
| 05ac:12a8 | Apple      | iPhone5/5C/5S/6              | 43    | ipheth     | 753952364D |
| 04f2:b3f6 | Chicony... | HD WebCam (Acer)             | 42    | uvcvideo   | 07CD36611F |
| 13d3:5122 | IMC Net... | 2M Integrated Webcam         | 42    | uvcvideo   | 49783F833C |
| 13d3:5165 | IMC Net... | USB Camera                   | 42    | uvcvideo   | F695A76E03 |
| 0402:7675 | ALi        | WebCam                       | 41    | uvcvideo   | 62BB2E7B3C |
| 04f2:b2e1 | Chicony... | Lenovo EasyCamera            | 41    | uvcvideo   | F3D7E4E13D |
| 04f2:b40e | Chicony... | HP Truevision HD camera      | 41    | uvcvideo   | 4B70A9D252 |
| 2232:1005 | Silicon... | WebCam SCB-0385N             | 40    | uvcvideo   | 4B260556B7 |
| 04f2:b23b | Chicony... | CNFA078                      | 39    | uvcvideo   | 9A249DA8EE |
| 04f2:b209 | Chicony... | WebCam                       | 38    | uvcvideo   | 2CD19D38C6 |
| 05a9:2640 | OmniVis... | OV2640 Webcam                | 38    | uvcvideo   | 2967AF01D8 |
| 0bda:579a | Realtek... | Lenovo EasyCamera            | 38    | uvcvideo   | 09141B681A |
| 04f2:b249 | Chicony... | HP Webcam-101                | 37    | uvcvideo   | A7E86DE30D |
| 0c45:6513 | Microdia   | Lenovo EasyCamera            | 37    | uvcvideo   | 7A87F02269 |
| 04f2:b221 | Chicony... | integrated camera            | 36    | uvcvideo   | FBF8462F41 |
| 04f2:b307 | Chicony... | TOSHIBA Web Camera - HD      | 36    | uvcvideo   | 7C82C563B9 |
| 058f:a016 | Alcor M... | ASUS USB2.0 WebCam           | 36    | uvcvideo   | 6A47875DF8 |
| 064e:c21c | Suyin      | 1.3M HD WebCam               | 36    | uvcvideo   | F0F215F1E4 |
| 064e:e263 | Suyin      | HP TrueVision HD Integrat... | 36    | uvcvideo   | 0C02DFD17B |
| 1bcf:2880 | Sunplus... | Laptop_Integrated_Webcam_HD  | 36    | uvcvideo   | 2C903F36D0 |
| 04f2:b483 | Chicony... | USB2.0 VGA UVC WebCam        | 35    | uvcvideo   | F8E8617476 |
| 05ac:8507 | Apple      | Built-in iSight              | 35    | uvcvideo   | 9F753AC669 |
| 064e:e330 | Suyin      | HD WebCam                    | 35    | uvcvideo   | D3813AFBF5 |
| 04f2:b012 | Chicony... | 1.3 MPixel UVC Webcam        | 34    | uvcvideo   | 926753D3C7 |
| 04f2:b404 | Chicony... | USB2.0 HD UVC WebCam         | 34    | uvcvideo   | EF445A792B |
| 05ac:8509 | Apple      | FaceTime HD Camera           | 34    | uvcvideo   | 16BDB52C40 |
| 064e:d214 | Suyin      | WebCam                       | 34    | uvcvideo   | 8CE41DB531 |
| 0bda:5801 | Realtek... | USB2.0-Camera                | 34    | uvcvideo   | DDF30C670A |
| 04f2:b217 | Chicony... | Lenovo Integrated Camera ... | 33    | uvcvideo   | 23DAC0F1B6 |
| 04f2:b2e2 | Chicony... | Lenovo EasyCamera            | 33    | uvcvideo   | 891002E8F2 |
| 04f2:b2fa | Chicony... | Integrated Camera            | 33    | uvcvideo   | B9CC4FFB0F |
| 05c8:0403 | Cheng U... | Webcam                       | 33    | uvcvideo   | 23E7475693 |
| 04f2:b26f | Chicony... | USB2.0 0.3M UVC WebCam       | 32    | uvcvideo   | 6FB4432F5A |
| 04f2:b3fd | Chicony... | HD WebCam (Asus N-series)    | 31    | uvcvideo   | 1F0902BCED |
| 064e:a117 | Suyin      | Acer HD Crystal Eye webcam   | 31    | uvcvideo   | 542B22772F |
| 064e:d20c | Suyin      | 1.3M HD WebCam               | 31    | uvcvideo   | B8FB16B9F4 |
| 0ac8:c349 | Z-Star ... | WebCam SC-03FFL11739P        | 31    | uvcvideo   | 6D637319C4 |
| 04f2:b2ea | Chicony... | Integrated Camera [ThinkPad] | 30    | uvcvideo   | 7C0CD24986 |
| 04f2:b34f | Chicony... | HP Truevision HD             | 30    | uvcvideo   | FEF9011BE9 |
| 064e:a102 | Suyin      | Acer/Lenovo Webcam [CN0316]  | 30    | uvcvideo   | FA4B987B6E |
| 5986:0294 | Acer       | Lenovo EasyCamera            | 30    | uvcvideo   | 06814257C1 |
| 04f2:b354 | Chicony... | UVC 1.00 device HD UVC We... | 29    | uvcvideo   | 9A401175B3 |
| 0c45:64ad | Microdia   | Laptop_Integrated_Webcam_HD  | 29    | uvcvideo   | 103788F3AC |
| 17ef:480f | Lenovo     | Integrated Webcam [R5U877]   | 29    | uvcvideo   | DA5E944C6C |
| 04f2:b446 | Chicony... | TOSHIBA Web Camera - HD      | 28    | uvcvideo   | 61625F7FA6 |
| 064e:a116 | Suyin      | UVC 1.3MPixel WebCam         | 28    | uvcvideo   | A8B9AB8CD7 |
| 5986:02ac | Acer       | HP TrueVision HD             | 28    | uvcvideo   | D2006E7A87 |
| 04f2:b5f7 | Chicony... | HD WebCam                    | 27    | uvcvideo   | 856A212CAA |
| 05c8:0348 | Cheng U... | HP Truevision HD             | 27    | uvcvideo   | A5A9D7BCAF |
| 0bda:58c2 | Realtek... | Laptop_Integrated_Webcam_HD  | 27    | uvcvideo   | 112EB80A9E |
| 0c45:651b | Microdia   | HP Webcam                    | 27    | uvcvideo   | 4677C67DBA |

### Card reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:0129 | Realtek... | RTS5129 Card Reader Contr... | 1583  | rtsx_usb   | 7957C03703 |
| 0bda:0139 | Realtek... | RTS5139 Card Reader Contr... | 356   | rtsx_usb   | F6A627C008 |
| 0cf2:6250 | ENE Tec... | SD card reader (UB6250)      | 6     | ums_ene... | A6F434EBC3 |
| 0c4b:0500 | Reiner ... | cyberJack RFID standard d... | 3     |            | 9F13812729 |
| 0c4b:0501 | Reiner ... | cyberJack RFID comfort du... | 1     |            | 307C8A6908 |
| 174f:1105 | Syntek     | SM-MS/Pro-MMC-XD Card Reader | 1     | uvcvideo   | E718456945 |

### Cdc data (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0e8d:0023 | MediaTek   | S103                         | 1     | option     | 457F8BF8F7 |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8564:1000 | Transcend  | TS32GJF370 JetFlash 32GB     | 321   | uas, us... | E9BD04F647 |
| 152d:2329 | JMicron... | JM20329 SATA Bridge          | 52    | usb_sto... | F7B38B6B8B |
| 174c:55aa | ASMedia... | Name: ASM1051E SATA 6Gb/s... | 46    | usb_sto... | D7977A3B0E |
| 04c5:2028 | Fujitsu    | External HDD 128GB           | 38    | uas, us... | 2AFD83B0D3 |
| 152d:2338 | JMicron... | JM20337 Hi-Speed USB to S... | 23    | uas, us... | 36981C6FBC |
| 1bcf:0c31 | Sunplus... | SPIF30x Serial-ATA bridge    | 21    | uas, us... | 9BE61E9A2D |
| 152d:0578 | JMicron... | JMS567 SATA 6Gb/s bridge     | 17    | uas, us... | AFD92BF265 |
| 13fd:0840 | Initio     | INIC-1618L SATA 100GB        | 16    | uas, us... | 6E5C32F6B0 |
| 0e8d:1806 | MediaTek   | Samsung SE-208AB Slim Por... | 15    | usb_sto... | 64F7963A8B |
| 0e8d:1887 | MediaTek   | DVDRAM GP60NS50              | 15    | usb_sto... | 11B9C937C9 |
| 19d2:1403 | ZTE WCD... | USB SCSI CD-ROM              | 14    | uas, us... | 7B7903FC02 |
| 0928:0010 | PLX Tec... | Ext Hard Disk 500GB          | 13    | uas, us... | FE2AB22987 |
| 174c:1351 | ASMedia... | DVDRW DA8AESH                | 10    | uas, us... | E8755C7EF8 |
| 054c:02d5 | Sony       | DVD-RAM UJ862PS              | 8     | uas, us... | 3284C77676 |
| 14cd:6600 | Super Top  | M110E PATA bridge            | 8     | usb_sto... | 782AFB3ED5 |
| 152d:2339 | JMicron... | JM20339 SATA Bridge          | 8     | uas, us... | 19BF4CEBAD |
| 0781:5406 | SanDisk    | Cruzer Micro U3 2GB          | 6     | uas, us... | CF39A00503 |
| 0e8d:1836 | MediaTek   | Samsung SE-S084 Super Wri... | 6     | usb_sto... | DFA0A14F07 |
| 13fd:3940 | Initio     | external DVD burner ECD81... | 6     | uas, us... | 41AADBADF6 |
| 2e04:c025 | Linux      | File-CD Gadget               | 6     | uas, us... | 561770352F |
| 04b7:0100 | Compal ... | DVDRW DA8AESH                | 5     | uas, us... | DEFB86D43A |
| 04fc:0c25 | Sunplus... | SATALink SPIF225A 320GB      | 5     | uas, us... | D56881C8B0 |
| 12d1:1082 | Huawei ... | File-CD Gadget               | 5     | uas, us... | 7872901FE9 |
| 13fd:0940 | Initio     | ASUS SBW-06D2X-U 500GB       | 5     | uas, us... | 32444C0848 |
| 1f75:0611 | Innosto... | EZEX-75WN4A0 1TB             | 5     | uas, us... | 8B4583B70A |
| 048d:1176 | Integra... | USB Flash Disk 16GB          | 4     | uas, us... | 34905CB301 |
| 054c:0377 | Sony       | BD-CMB UJ-120                | 4     | uas, us... | 25C759104C |
| 12d1:107e | Huawei ... | File-CD Gadget               | 4     | uas, us... | 5A6938019A |
| 13fd:1040 | Initio     | INIC-1511L PATA Bridge       | 4     | usb_sto... | AF27D8603A |
| 13fd:3e40 | Initio     | ZALMAN ZM-VE350 256GB        | 4     | uas, us... | CA27A3BAF8 |
| 152e:2571 | LG (HLDS)  | DVDRAM GP08NU6B              | 4     | uas, us... | 123447177A |
| 0b05:7774 | ASUSTek... | Zenfone GO (ZB500KL) (RND... | 3     | rndis_host | 92550C4EB8 |
| 19d2:1410 | ZTE WCD... | LTE Technologies MSM         | 3     | uas, us... | 0E5869D47C |
| 1c6b:a222 | Philips... | DVD Writer Slimtype eTAU108  | 3     | uas, us... | 9600A62F92 |
| 054c:03dc | Sony       | DVD RW DRX-S70U              | 2     | usb_sto... | 53D04B1004 |
| 05c6:92fe | Qualcomm   | Disk                         | 2     | uas, us... | D06E1D8C5B |
| 0789:0197 | Logitec    | DVDRAM GP67N                 | 2     | usb_sto... | 266D78E723 |
| 0b05:1797 | ASUSTek... | DVDRAM GU60N                 | 2     | usb_sto... | E2A6782664 |
| 0ecd:a100 | Lite-On IT | LDW-411SX DVD/CD Rewritab... | 2     | usb_sto... | 664332711F |
| 13fd:0842 | Initio     | CDDVDW SE-T084P              | 2     | usb_sto... | 02B9759D77 |
| 152d:2519 | JMicron... | Transcend                    | 2     | uas, us... | 7D028BB762 |
| 03f0:4907 | Hewlett... | DVD Writer 556s              | 1     | uas, us... | 0504CC20A9 |
| 03f0:5907 | Hewlett... | DVD Writer 557s              | 1     | usb_sto... | B91A0B2CB2 |
| 0402:5621 | ALi        | M5621 High-Speed IDE Cont... | 1     | uas, us... | 2458B122E5 |
| 0408:ea42 | Quanta ... | modem CD-ROM                 | 1     | option,... | 8EC72CB3E0 |
| 0409:0056 | NEC Com... | DW-224E-C                    | 1     | usb_sto... | 83B5C14B39 |
| 0409:0840 | NEC Com... | CD/DVDW SE-T084L             | 1     | usb_sto... | A67AF78289 |
| 0411:0094 | BUFFALO    | DVD-RAM GH22NP20             | 1     | usb_sto... | FAD3CD1AA5 |
| 0476:059b | AESP       | DVD RW AD-7580S              | 1     | uas, us... | AC0478FF7E |
| 04b3:4487 | IBM        | RW/DVD GCC-4247N             | 1     | uas, us... | 1501370AB2 |
| 04da:250a | Panason... | DVD-RAM UJ-833S              | 1     | usb_sto... | 497FA7F67C |
| 04e8:685b | Samsung... | GT-I9100 Phone [Galaxy S ... | 1     | usb_sto... | F788E2E8F1 |
| 067b:2571 | Prolifi... | DVDRAM GE24NU30              | 1     | uas, us... | 00CA4E66E5 |
| 0930:0c06 | Toshiba    | SuperMultiPA3761             | 1     | usb_sto... | 174467AEFE |
| 0b05:5600 | ASUSTek... | File-CD Gadget               | 1     | uas, us... | 635226B290 |
| 0b05:5601 | ASUSTek... | Device CD-ROM                | 1     | uas, us... | 286A4000F8 |
| 0b05:5f03 | ASUSTek... | File-CD Gadget               | 1     | usb_sto... | 92D18FD140 |
| 0b05:5f05 | ASUSTek... | Device CD-ROM                | 1     | uas, us... | AA0E38B197 |
| 0bb4:0ccb | HTC (Hi... | Android Phone                | 1     | uas, us... | 12494330BD |
| 0bb4:0edb | HTC (Hi... | File-CD Gadget               | 1     | uas, us... | CAF51C474E |
| 0bb4:0f25 | HTC (Hi... | One M8                       | 1     | uas, us... | E3687E435F |
| 0ecd:40c0 | Lite-On IT | DVD A DS8A1H                 | 1     | uas, us... | C08B7EBB15 |
| 1058:070a | Western... | My Passport Essential (WD... | 1     | usb_sto... | B42452302F |
| 12d1:107f | Huawei ... | File-CD Gadget               | 1     | uas, us... | BD698398A7 |
| 13fd:0841 | Initio     | Samsung SE-T084M DVD-RW      | 1     | usb_sto... | CB6D7B613B |
| 13fd:084c | Initio     | DVDWBD SE-406AB              | 1     | usb_sto... | 0C15DF9936 |
| 152d:0562 | JMicron... | MK2035GSS                    | 1     | usb_sto... | 3EA811E273 |
| 152d:2337 | JMicron... | ATA/ATAPI Bridge             | 1     | uas, us... | 26399C140A |
| 152e:1640 | LG (HLDS)  | DVDRAM GE20NU11              | 1     | usb_sto... | 81BAC67CEE |
| 17ef:7302 | Lenovo     | USB_DVD_Burner5              | 1     | usb_sto... | F0022150E8 |
| 17ef:730a | Lenovo     | Ultraslim DVD                | 1     | usb_sto... | E8E7E33BB2 |
| 17ef:7883 | Lenovo     | File-Stor Gadget             | 1     | uas, us... | 7E72BCC706 |
| 17ef:7a30 | Lenovo     | File-CD Gadget               | 1     | uas, us... | 01CE356619 |
| 19d2:0501 | ZTE WCD... | File-Stor Gadget             | 1     | uas, us... | 88BD859F3F |
| 19d2:1081 | ZTE WCD... | CD-Rom                       | 1     | usb_sto... | CF3199C253 |
| 1c6b:a223 | Philips... | SDRW-08D2S-U                 | 1     | usb_sto... | 5E19B5A0BD |
| 22d9:2773 | Linux      | File-CD Gadget               | 1     | uas, us... | EF252D4CF9 |
| 22d9:2774 | Linux      | File-CD Gadget               | 1     | uas, us... | EF19896EDA |
| 2e04:c026 | Linux      | File-CD Gadget               | 1     | uas, us... | 5E0DC6DFA3 |
| 413c:9016 | Dell       | DVD+/-RW DW316               | 1     | usb_sto... | C7C3D61955 |

### Chipcard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0a5c:5800 | Broadcom   | BCM5880 Secure Applicatio... | 156   | usbfs      | CA6EFEBBD7 |
| 058f:9540 | Alcor M... | AU9540 Smartcard Reader      | 70    | usbfs      | 0F9287651D |
| 0b97:7772 | O2 Micro   | OZ776 CCID Smartcard Reader  | 69    | usbfs      | F40C3D18FB |
| 147e:2020 | Upek       | TouchChip Fingerprint Cop... | 58    |            | DF1FD87B87 |
| 17ef:1003 | Lenovo     | Integrated Smart Card Reader | 52    | usbfs      | 037CAC7A3D |
| 0a5c:5801 | Broadcom   | BCM5880 Secure Applicatio... | 41    | usbfs      | FFEF7E12D0 |
| 0a5c:5832 | Broadcom   | 5880                         | 23    | usbfs      | B9281326D7 |
| 0b97:7762 | O2 Micro   | Oz776 SmartCard Reader       | 18    |            | BF8ACC676E |
| 0a5c:5834 | Broadcom   | 5880                         | 12    | usbfs      | 90E9F8DFAD |
| 08e6:34ec | Gemalto... | Compact Smart Card Reader... | 8     | usbfs      | 85B9549CFF |
| 0a5c:5804 | Broadcom   | BCM5880 Secure Applicatio... | 6     |            | 4926835893 |
| 0529:0620 | Aladdin... | Token JC                     | 3     |            | BF37749FEB |
| 0a5c:5805 | Broadcom   | 5880                         | 3     |            | 5E0DC6DFA3 |
| 058f:9520 | Alcor M... | EMV Certified Smart Card ... | 2     | usbfs      | 1EFF42042A |
| 076b:1021 | OmniKey    | CardMan 1021                 | 2     |            | 24366329C2 |
| 048d:1365 | Integra... | SmartCard Reader             | 1     |            | DB4B891E15 |
| 072f:2200 | Advance... | ACR122U                      | 1     | pn533_usb  | 6D7AF3CC11 |
| 072f:90cc | Advance... | ACR38 SmartCard Reader       | 1     | usbfs      | 0952D5E50B |
| 076b:4321 | OmniKey    | CardMan 4321                 | 1     |            | 3191678465 |
| 0a5c:5802 | Broadcom   | BCM5880 Secure Applicatio... | 1     |            | 4D67416D5E |
| 0a5c:5842 | Broadcom   | 58200                        | 1     |            | 8FB4429DD6 |
| 0bda:0165 | Realtek... | Smart Card Reader Interface  | 1     |            | E9507D662A |
| 1050:0111 | Yubico.com | Yubikey NEO(-N) OTP+CCID     | 1     | usbfs      | 92C423B62E |
| 1050:0406 | Yubico.com | Yubikey 4 U2F+CCID           | 1     | usbfs      | 424D23C5FE |
| 1a44:0870 | VASCO D... | DIGIPASS 870                 | 1     |            | 1ED6532125 |
| 24dc:0101 | ARDS       | JaCarta                      | 1     |            | 92A6009CC0 |

### Converter (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 110a:1110 | Moxa Te... | UPort 1110 1-port RS-232 ... | 1     | ti_usb_... | F815AEE35E |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 058f:6366 | Alcor M... | Multi Flash Reader           | 446   | uas, us... | 8F2E060D39 |
| 090c:1000 | Silicon... | Flash Voyager                | 331   | uas, us... | 3A4D90A1C3 |
| 058f:6387 | Alcor M... | Transcend JetFlash Flash ... | 312   | uas, us... | 425176719A |
| 0bda:0158 | Realtek... | USB 2.0 multicard reader     | 164   | ums_rea... | BD55864B86 |
| 13fe:4200 | Kingsto... | Silicon-Power32G 32GB        | 146   | uas, us... | DE168F8DCC |
| 0951:1666 | Kingsto... | DataTraveler 100 G3/G4/SE... | 120   | uas, us... | DE5FB421D9 |
| 0781:5567 | SanDisk    | Cruzer Blade 16GB            | 114   | uas, us... | 69A8A11098 |
| 1005:b113 | Apacer ... | Handy Steno/AH123 / Handy... | 112   | uas, us... | CA77267E34 |
| 0930:6545 | Toshiba    | Kingston DataTraveler 102... | 101   | uas, us... | 9B82C44F0C |
| 13fe:4100 | Kingsto... | Silicon-Power16G 16GB        | 93    | uas, us... | 1C77D7A584 |
| 0bda:0138 | Realtek... | RTS5138 Card Reader Contr... | 91    | ums_rea... | F7B38B6B8B |
| 0930:6544 | Toshiba    | TransMemory-Mini / Kingst... | 64    | uas, us... | CC12571867 |
| 0781:5581 | SanDisk    | Ultra 128GB                  | 63    | usb_sto... | 79E53D2DB5 |
| 0951:1665 | Kingsto... | Digital DataTraveler SE9 ... | 57    | uas, us... | 26EC3BF654 |
| 0781:5571 | SanDisk    | Cruzer Fit 16GB              | 55    | uas, us... | AB17752168 |
| 0bda:0116 | Realtek... | RTS5116 Card Reader Contr... | 53    | uas, us... | 54C92BF69C |
| 13fe:5500 | Kingsto... | Silicon-Power32G 31GB        | 41    | uas, us... | 94C3F1BC72 |
| 0781:5583 | SanDisk    | Ultra Fit 123GB              | 40    | uas, us... | 087A8F3344 |
| 0bda:0159 | Realtek... | RTS5159 Card Reader Contr... | 40    | ums_rea... | C2A4730E33 |
| 12d1:14dc | Huawei ... | E33372 LTE/UMTS/GSM HiLin... | 40    | uas, us... | 54C92BF69C |
| 13fe:3e00 | Kingsto... | Silicon-Power16G 16GB        | 36    | uas, us... | 0BFEB87324 |
| 14cd:6116 | Super Top  | M6116 SATA Bridge            | 35    | uas, us... | 891002E8F2 |
| 0951:1643 | Kingsto... | DataTraveler G3 15GB         | 33    | uas, us... | DE3BBCF659 |
| 0bda:0316 | Realtek... | SD/MMC 128GB                 | 33    | uas, us... | 3A4D90A1C3 |
| 13fe:3600 | Kingsto... | flash drive (4GB, EMTEC) ... | 33    | usb_sto... | 7C0CD24986 |
| 0bda:0177 | Realtek... | SD/MMC/MS PRO 31GB           | 32    | uas, us... | 0AB5BF5B1A |
| 0781:5575 | SanDisk    | Cruzer Glide 32GB            | 30    | uas, us... | 856A212CAA |
| 174c:5106 | ASMedia... | Transcend StoreJet 25M3 3... | 29    | uas, us... | 6416557409 |
| abcd:1234 | Chipsbnk   | Alu Line 16GB                | 24    | uas, us... | CF41AB5F1A |
| 0781:556b | SanDisk    | Cruzer Edge 16GB             | 23    | uas, us... | D400943350 |
| 18a5:0302 | Verbatim   | STORE N GO 16GB              | 23    | uas, us... | 54C92BF69C |
| 1908:0226 | GEMBIRD    | Mass-Storage 134GB           | 23    | uas, us... | B456448488 |
| 0951:1642 | Kingsto... | DT101 G2 16GB                | 22    | uas, us... | F699A76638 |
| 05ac:8403 | Apple      | Internal Memory Card Reader  | 21    | uas, us... | 9F753AC669 |
| 05e3:0723 | Genesys... | GL827L SD/MMC/MS Flash Ca... | 21    | usb_sto... | 8EB32C768B |
| 0781:5590 | SanDisk    | Ultra Dual 15GB              | 18    | uas, us... | 2288125313 |
| 0781:5591 | SanDisk    | Ultra Flair USB 3.0 124GB    | 17    | uas, us... | AB97B7B7FD |
| 8644:8003 | Intenso... | Micro Line 16GB              | 17    | uas, us... | 4FAA6112C3 |
| 1058:25a2 | Western... | Elements 25A2 500GB          | 16    | uas, us... | AE1CE65D11 |
| 05ac:8406 | Apple      | SD Card Reader               | 15    | uas, us... | B57A70D35E |
| 0781:5572 | SanDisk    | Cruzer Switch 16GB           | 14    | uas, us... | 54B7D3A895 |
| 0bc2:a013 | Seagate    | Backup+ SL 500GB             | 14    | uas, us... | D61505C04F |
| 1234:0123 | Brain A... | Flash Disk 4GB               | 14    | usb_sto... | 0B9D2F9024 |
| 125f:c08a | A-DATA ... | C008 Flash Drive 32GB        | 14    | uas, us... | BC2C6A0308 |
| 0951:1607 | Kingsto... | DataTraveler 100 32GB        | 13    | uas, us... | F95BBD54DA |
| 0bda:0186 | Realtek... | Card Reader                  | 13    | uas, us... | 569B9B8B32 |
| 1307:0165 | Transcend  | 2GB/4GB/8GB Flash Drive 16GB | 13    | uas, us... | 142AD90B1A |
| 13fd:1840 | Initio     | INIC-1608 SATA bridge        | 13    | uas, us... | 6F9FB4A3FD |
| 174c:1153 | ASMedia... | ASM2115 SATA 6Gb/s bridge    | 13    | uas, us... | 5DA90F3771 |
| 3538:0901 | pqi        | IntelligentStick 8GB         | 13    | uas, us... | 6D5F1234C2 |
| 0951:1603 | Kingsto... | DataTraveler 1GB/2GB Pen ... | 12    | uas, us... | 6DE803D560 |
| 0951:1653 | Kingsto... | Data Traveler 100 G2 8 Gi... | 12    | uas, us... | 1C8DCCDCFC |
| 1058:1023 | Western... | Elements SE Portable (WDB... | 12    | uas, us... | 3923D18587 |
| 8644:800b | Intenso... | Micro Line (4GB) 16GB        | 12    | uas, us... | 0EE2D2E951 |
| ffff:5678 | VendorCo   | ProductCode 16GB             | 12    | uas, us... | 6260EAF2EB |
| 0781:5580 | SanDisk    | SDCZ80 Flash Drive 16GB      | 11    | uas, us... | 0AB22425EA |
| 0bc2:2312 | Seagate    | Expansion 500GB              | 11    | uas, us... | 4AFB8F7991 |
| 0bc2:ab24 | Seagate    | Backup Plus Portable Drive   | 11    | uas        | 9A0C589739 |
| 1058:074a | Western... | My Passport 074A 500GB       | 11    | uas, us... | 74B5E8085F |
| 13fe:4300 | Kingsto... | USB DISK 2.0 16GB            | 11    | uas, us... | 6D637319C4 |
| 1f75:0621 | Innosto... | JPVX-22JC3T0                 | 11    | uas, us... | F98913A9D4 |
| 0480:a00c | Toshiba... | External USB 3.0 750GB       | 10    | uas, us... | 1184F5572B |
| 054c:0281 | Sony       | USB HS-CARD                  | 10    | uas, us... | 46F4E56E3F |
| 058f:6335 | Alcor M... | SD/MMC Card Reader           | 10    | uas, us... | 0340667BFD |
| 05dc:a81d | Lexar M... | LJDTT16G [JumpDrive 16GB]... | 10    | uas, us... | 6FDCFE86C2 |
| 05dc:a838 | Lexar M... | USB JumpDrive Tough 128GB    | 10    | uas, us... | 945AA6C3E8 |
| 05e3:0745 | Genesys... | Logilink CR0012 32GB         | 10    | usb_sto... | E5321DA7F6 |
| 090c:6200 | Silicon... | microSD card reader          | 10    | uas, us... | 36E2DD9EE4 |
| 0bda:0328 | Realtek... | SD/MMC CRW                   | 10    | uas, us... | 57C89FEBD9 |
| 1058:1042 | Western... | Elements SE Portable (WDB... | 10    | uas, us... | ED98A45D81 |
| 125f:385a | A-DATA ... | USB Flash Drive 16GB         | 10    | uas, us... | 83F2FF723D |
| 125f:c96a | A-DATA ... | C906 Flash Drive 16GB        | 10    | uas, us... | E218134423 |
| 13fe:1d00 | Kingsto... | DataTraveler 2.0 1GB/4GB ... | 10    | uas, us... | 41D0F23B8D |
| 14cd:125d | Super Top  | Storage Device 32GB          | 10    | uas, us... | 8B0DFDBE84 |
| 152d:2336 | JMicron... | Hard Disk Drive              | 10    | uas, us... | B643F514C8 |
| 152d:2509 | JMicron... | JMS539 SuperSpeed SATA II... | 10    | uas, us... | B7DDB54F07 |
| 1f75:0917 | Innosto... | Ultra Line 16GB              | 10    | uas, us... | A232297845 |
| 23a9:ef18 | USB2.0     | DISK1.8.5.0.319 16GB         | 10    | uas, us... | 262BB6B100 |
| 4971:8017 | SimpleTech | USB to ATA/ATAPI Bridge      | 10    | uas, us... | 0231723A3E |
| 0080:a001 | winstars   | HDD Docking Station          | 9     | uas        | 1CD14256CC |
| 0480:a202 | Toshiba... | Canvio Basics HDD 500GB      | 9     | uas, us... | EE8564DC35 |
| 048d:1234 | Integra... | PenDrive 8GB                 | 9     | uas, us... | B5FB03D2E4 |
| 05e3:0718 | Genesys... | IDE/SATA Adapter             | 9     | uas, us... | 2D4AA64C10 |
| 05e3:0727 | Genesys... | microSD Reader/Writer        | 9     | uas, us... | ED81FBB6E1 |
| 05e3:0751 | Genesys... | microSD Card Reader          | 9     | uas, us... | 9CE29D0583 |
| 0781:5530 | SanDisk    | Cruzer                       | 9     | uas, us... | 726BCEC2EA |
| 0781:5576 | SanDisk    | Cruzer Facet 32GB            | 9     | uas, us... | 38D6C02732 |
| 0951:1625 | Kingsto... | DataTraveler 101 II 4GB      | 9     | uas, us... | 2219D112DF |
| 0bb4:0001 | HTC (Hi... | Android Phone via mass st... | 9     | uas, us... | 02E0DBC8D3 |
| 0bc2:231a | Seagate    | Expansion Portable           | 9     | uas        | C31329A508 |
| 1058:0704 | Western... | My Passport Essential (WD... | 9     | usb_sto... | 611FC0FD30 |
| 1221:3234 | Unknown... | Disk (Thumb drive)           | 9     | uas, us... | 33775F7BA5 |
| 18a5:0300 | Verbatim   | STORE N GO 8GB               | 9     | usb_sto... | 7AA2883722 |
| aaaa:8816 | MXT        | Storage Device 32GB          | 9     | uas, us... | A6DD71FDD3 |
| 05e3:0702 | Genesys... | USB 2.0 IDE Adapter [GL811E] | 8     | usb_sto... | C31329A508 |
| 0781:5597 | SanDisk    | Cruzer Glide 3.0 124GB       | 8     | uas, us... | D193147053 |
| 0bc2:2322 | Seagate    | SRD0NF1 Expansion Portabl... | 8     | uas        | BA450606A3 |
| 1058:10b8 | Western... | Elements Portable (WDBU6Y... | 8     | uas, us... | 3A39EF0147 |
| 125f:cb10 | A-DATA ... | Dash Drive UV100 16GB        | 8     | uas, us... | C7627C95A5 |
| 125f:db8a | A-DATA ... | USB Flash Drive 64GB         | 8     | uas, us... | B8F0A4691D |

### Dvb card (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:2838 | Realtek... | RTL2838 DVB-T                | 10    | dvb_usb... | CA184276A7 |
| 07ca:a309 | AVerMed... | AVerTV DVB-T (A309)          | 8     | dvb_usb... | 500C2BCFBC |
| 1164:1f08 | YUAN Hi... | STK7700D                     | 5     | dvb_usb... | 7D19EDDC5C |
| 07ca:0810 | AVerMed... | H810 USB Hybrid DVB-T        | 2     |            | B46F2FEE35 |
| 07ca:a310 | AVerMed... | A310                         | 2     | dvb_usb... | 4F1C235318 |
| 1164:0871 | YUAN Hi... | STK7700D                     | 2     | dvb_usb... | 97D9353E2A |
| 048d:9005 | Integra... | DVB-T TV Stick               | 1     | dvb_usb... | AE491DB991 |
| 048d:9135 | Integra... | Zolid Mini DVB-T Stick       | 1     | dvb_usb... | B586DBA598 |
| 0572:c688 | Conexan... | Geniatech T230 DVB-T2 TV ... | 1     | dvb_usb... | F96DB0D7EF |
| 07ca:8150 | AVerMed... | A815O                        | 1     | dvb_usb... | 69CC9D8136 |
| 0bda:2832 | Realtek... | RTL2832U DVB-T               | 1     | dvb_usb... | DA28D4A9D0 |
| 14aa:0221 | WideVie... | WT-220U DVB-T dongle         | 1     | dvb_usb... | 7FA8E3A5E5 |
| 187f:0202 | Siano M... | Nice                         | 1     | smsusb     | 7F239B5732 |
| 2040:5200 | Hauppauge  | NovaT 500Stick               | 1     | dvb_usb... | A5A0DA657B |
| 2040:7070 | Hauppauge  | Nova-T Stick 3               | 1     | dvb_usb... | DAE29736DB |
| 2304:022e | Pinnacl... | PCTV 320cx                   | 1     | dvb_usb... | 9DA08CE4A5 |
| 2304:0237 | Pinnacl... | PCTV 73e [DiBcom DiB7000PC]  | 1     | dvb_usb... | CC885D64D1 |
| 2304:023a | Pinnacl... | PCTV 801e                    | 1     | dvb_usb... | DC0159ECF4 |
| eb1a:5013 | eMPIA T... | USB 2883 Device              | 1     |            | 5DD14F9164 |

### Fingerprint reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 138a:0011 | Validit... | VFS5011 Fingerprint Reader   | 130   |            | 10FB53EFCA |
| 138a:0018 | Validit... | Fingerprint scanner          | 120   |            | D2006E7A87 |
| 138a:003f | Validit... | VFS495 Fingerprint Reader    | 109   | usbfs      | AC96DD45F9 |
| 147e:2016 | Upek       | Biometric Touchchip/Touch... | 96    |            | C28241E4F0 |
| 0483:2016 | STMicro... | Fingerprint Reader           | 86    |            | 7F41843359 |
| 08ff:2580 | AuthenTec  | AES2501 Fingerprint Sensor   | 86    |            | 084F7E13DA |
| 08ff:2810 | AuthenTec  | AES2810                      | 78    |            | A23E000798 |
| 147e:1002 | Upek       | Biometric Touchchip/Touch... | 69    |            | 7C0CD24986 |
| 08ff:1600 | AuthenTec  | AES1600                      | 68    |            | 4A6A073320 |
| 138a:0007 | Validit... | VFS451 Fingerprint Reader    | 53    | usbfs      | E2C04796A0 |
| 138a:003c | Validit... | VFS471 Fingerprint Reader    | 53    |            | A31B1E8E5E |
| 138a:003d | Validit... | VFS491                       | 50    |            | 68A86E6E3F |
| 138a:0017 | Validit... | VFS 5011 fingerprint sensor  | 47    | usbfs      | 7637F3DE5F |
| 138a:0005 | Validit... | VFS301 Fingerprint Reader    | 43    |            | DDF30C670A |
| 1c7a:0801 | LighTun... | Fingerprint Reader           | 40    |            | E1D809E15F |
| 138a:0001 | Validit... | VFS101 Fingerprint Reader    | 37    |            | 500C2BCFBC |
| 1c7a:0603 | LighTun... | EgisTec ES603                | 36    |            | 47C94E2F61 |
| 138a:0050 | Validit... | Swipe Fingerprint Sensor     | 33    |            | 734FCAFBD7 |
| 1c7a:0570 | LighTun... | EgisTec Touch Fingerprint... | 26    |            | 376C2CCA98 |
| 147e:1000 | Upek       | Biometric Touchchip/Touch... | 21    |            | C15AB53D21 |
| 08ff:168f | AuthenTec  | AES1660 Fingerprint Sensor   | 19    |            | 801A0452F8 |
| 04f3:0903 | Elan Mi... | ELAN:Fingerprint             | 16    |            | 632AA405C1 |
| 04f3:0c1a | Elan Mi... | ELAN:Fingerprint             | 13    |            | A159026F22 |
| 08ff:2550 | AuthenTec  | AES2550 Fingerprint Sensor   | 13    |            | EEFC712947 |
| 06cb:0078 | Synaptics  | WBDI Device                  | 12    |            | 4CED599618 |
| 147e:1001 | Upek       | TCS5B Fingerprint sensor     | 12    |            | 2AFD83B0D3 |
| 04f3:0c03 | Elan Mi... | ELAN:Fingerprint             | 11    |            | E6C392B427 |
| 138a:0090 | Validit... | VFS7500 Touch Fingerprint... | 10    |            | 7E4A6B2354 |
| 138a:0010 | Validit... | VFS Fingerprint sensor       | 9     |            | 467F389FCE |
| 138a:0008 | Validit... | VFS300 Fingerprint Reader    | 8     |            | 6722706FA2 |
| 138a:0091 | Validit... | VFS7552 Touch Fingerprint... | 8     |            | 200F73880C |
| 08ff:168b | AuthenTec  | Fingerprint Sensor           | 6     |            | 4398E73607 |
| 08ff:2665 | AuthenTec  | Fingerprint Sensor           | 5     |            | 42DF53B120 |
| 08ff:2683 | AuthenTec  | Fingerprint Sensor           | 3     |            | 0A5A3C6337 |
| 06cb:0082 | Synaptics  | My Lockey                    | 2     |            | D302412809 |
| 08ff:1686 | AuthenTec  | Fingerprint Sensor           | 2     |            | 73CAFC58A2 |
| 08ff:168c | AuthenTec  | Fingerprint Sensor           | 2     |            | 7C26C8530E |
| 06cb:00b7 | Synaptics  | Synaptics VFS7552 Touch F... | 1     |            | 0D375562BD |
| 08ff:1660 | AuthenTec  | AES1660 Fingerprint Sensor   | 1     |            | 89EC4F0398 |
| 08ff:168a | AuthenTec  | Fingerprint Sensor           | 1     |            | 9A145A3041 |
| 08ff:2500 | AuthenTec  | AES2501                      | 1     |            | 4DCFB332DF |
| 08ff:2691 | AuthenTec  | Fingerprint Sensor           | 1     |            | 79E75DF44E |

### Gamepad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 045e:028e | Microsoft  | Xbox360 Controller           | 8     | xpad       | D4EC4EAE87 |
| 0079:0011 | DragonRise | Gamepad                      | 5     | usbhid     | 419900511F |
| 046d:c21f | Logitech   | F710 Wireless Gamepad [XI... | 5     | xpad       | 0A3B451A83 |
| 046d:c21d | Logitech   | F310 Gamepad [XInput Mode]   | 2     | xpad       | 098FB68F9C |
| 0e8f:0012 | GreenAsia  | Joystick/Gamepad             | 2     | usbhid     | 265B796EFD |
| 0e8f:310d | GreenAsia  | GAMEPAD 3 TURBO              | 2     | usbhid     | 71A5A103B0 |
| 045e:0291 | Microsoft  | Xbox 360 Wireless Receive... | 1     |            | E9B5CADBB4 |
| 0810:e501 | Persona... | SNES Gamepad                 | 1     | usbhid     | 37CED39AA5 |
| 0e6f:011f | Logic3     | Rock Candy Gamepad for Xb... | 1     | xpad       | 80B359DC57 |
| 2563:0575 | ShanWan    | USB WirelessGamepad          | 1     | usbhid     | BC8D22EADA |

### Hardware key (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0471:485d | Philips... | Senselock SenseIV v2.x       | 3     |            | E4A6E39276 |
| 0a89:0003 | Aktiv      | Guardant Stealth/Net II      | 1     |            | 9BF4CABAF0 |
| 0a89:0020 | Aktiv      | Rutoken S                    | 1     | usbfs      | B22E899324 |

### Hasp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0529:0001 | Aladdin... | HASP copy protection dongle  | 7     |            | 1CE26C391A |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 14320 | hub        | C61C9E33F6 |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 7731  | hub        | C61C9E33F6 |
| 1d6b:0001 | Linux F... | 1.1 root hub                 | 5090  | hub        | E9BD04F647 |
| 8087:0024 | Intel      | Integrated Rate Matching Hub | 3620  | hub        | B9A4817612 |
| 8087:8000 | Intel      | Hub                          | 1073  | hub        | 7637F3DE5F |
| 8087:0020 | Intel      | Integrated Rate Matching Hub | 1060  | hub        | 86987CF1ED |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 562   | hub        | D7977A3B0E |
| 8087:8008 | Intel      | Hub                          | 438   | hub        | 2D1845C282 |
| 0438:7900 | AMD        | Hub                          | 418   | hub        | C61C9E33F6 |
| 1a40:0101 | incompl... | 4-Port HUB                   | 406   | hub        | 71C7CF2056 |
| 8087:8001 | Intel      | Hub                          | 242   | hub        | 6F94FCD1C0 |
| 0a5c:4500 | Broadcom   | BCM2046B1 USB 2.0 Hub (pa... | 198   | hub        | 6722706FA2 |
| 05e3:0610 | Genesys... | 4-port hub                   | 116   | hub        | 94C3F1BC72 |
| 0b97:7761 | O2 Micro   | Oz776 1.1 Hub                | 87    | hub        | F40C3D18FB |
| 8087:07e6 | Intel      | Hub                          | 79    | hub        | CC7193A7B9 |
| 058f:6254 | Alcor M... | USB Hub                      | 72    | hub        | 7D2B995B44 |
| 0424:2514 | Standar... | USB 2.0 Hub                  | 54    | hub        | 2458B122E5 |
| 2109:2812 | VIA Labs   | USB2.0 VL812 Hub             | 44    | hub        | 632B661A7E |
| 0409:005a | NEC Com... | HighSpeed Hub                | 41    | hub        | B20F51D9C3 |
| 0424:2513 | Standar... | 2.0 Hub                      | 38    | hub        | 16BDB52C40 |
| 0424:2134 | Standar... | USB2134B                     | 36    | hub        | AC96DD45F9 |
| 0424:5534 | Standar... | USB5534B                     | 35    | hub        | AC96DD45F9 |
| 05e3:0606 | Genesys... | USB 2.0 Hub / D-Link DUB-... | 35    | hub        | 0FCCED0386 |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 32    | hub        | 4CED599618 |
| 2109:0812 | VLI Labs   | USB 3.0 VL812 HUB            | 32    | hub        | 632B661A7E |
| 1a40:0201 | Terminu... | FE 2.1 7-port Hub            | 31    | hub        | BF4A9DEE07 |
| 2109:2813 | VIA Labs   | USB2.0 Hub                   | 29    | hub        | 4CED599618 |
| 413c:2513 | Dell       | internal USB Hub of E-Por... | 29    | hub        | 8B920A3699 |
| 214b:7000 |            | USB2.0 HUB                   | 27    | hub        | 31400D27D2 |
| 413c:a005 | Dell       | Internal 2.0 Hub             | 27    | hub        | BF8ACC676E |
| 05e3:0612 | Genesys... | USB3.0 Hub 123               | 25    | hub        | 94C3F1BC72 |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 24    | hub        | 331420F489 |
| 2109:0813 | VIA Labs   | USB3.0 Hub                   | 24    | hub        | 5C4E5B6717 |
| 17ef:100a | Lenovo     | ThinkPad Mini Dock Plus S... | 22    | hub        | C28241E4F0 |
| 14cd:8601 | Super Top  | USB 2.0 Hub                  | 21    | hub        | 8B3744250A |
| 058f:9254 | Alcor M... | Hub                          | 19    | hub        | E6323014B6 |
| 0a05:7211 | Unknown... | hub                          | 17    | hub        | 053A010037 |
| 2109:0811 | VIA Labs   | 4-Port USB 3.0 Hub           | 15    | hub        | 3F35BDC85A |
| 05ac:1006 | Apple      | Hub in Aluminum Keyboard     | 14    | hub        | FECEC5C77A |
| 0bda:5401 | Realtek... | RTL 8153 USB 3.0 hub with... | 13    | hub        | B6DCA8C94D |
| 413c:5534 | Dell       | USB5534                      | 13    | hub        | 3C07832726 |
| 0424:2807 | Standar... | USB2807 Hub                  | 12    | hub        | 5D2CB1E1B0 |
| 0424:5807 | Standar... | USB5807 Hub                  | 12    | hub        | 5D2CB1E1B0 |
| 044e:3011 | Alps El... | BCM2045B2                    | 12    | hub        | C257EC2DD4 |
| 04b4:6560 | Cypress... | CY7C65640 USB-2.0 "TetraHub" | 12    | hub        | 4A6A073320 |
| 05e3:0616 | Genesys... | hub                          | 11    | hub        | AACC137FAA |
| 0bda:0401 | Realtek... | USB3.0 Hub                   | 11    | hub        | B6DCA8C94D |
| 214b:7250 |            | USB2.0 HUB                   | 11    | hub        | 0D36FA2146 |
| 0424:2512 | Standar... | USB 2.0 Hub                  | 10    | hub        | FECEC5C77A |
| 05e3:0617 | Genesys... | USB3.0 Hub                   | 10    | hub        | CF32C265C9 |
| 17ef:1005 | Lenovo     | Hub                          | 9     | hub        | E9ACD76C2C |
| 17ef:100f | Lenovo     | ThinkPad Dock                | 9     | hub        | 364B159B80 |
| 17ef:1010 | Lenovo     | ThinkPad Dock                | 9     | hub        | 364B159B80 |
| 2109:2811 | VIA Labs   | USB2.0 Hub                   | 9     | hub        | 8D2BA1E47F |
| 413c:2134 | Dell       | USB2134                      | 9     | hub        | 3C07832726 |
| 0424:2503 | Standar... | USB 2.0 Hub                  | 8     | hub        | CF41AB5F1A |
| 0451:8142 | Texas I... | TUSB8041 4-Port Hub          | 8     | hub        | 4926835893 |
| 05e3:0605 | Genesys... | USB 2.0 Hub                  | 8     | hub        | 04BC793D62 |
| 0424:2504 | Standar... | USB 2.0 Hub                  | 7     | hub        | DBBBD6582E |
| 04b3:4485 | IBM        | ThinkPad Dock Hub            | 7     | hub        | 037CAC7A3D |
| 2109:8110 | VIA Labs   | USB 3.0 Hub                  | 7     | hub        | 8D2BA1E47F |
| 8564:4000 | Transcend  | RDF8 16GB                    | 7     | uas, us... | 1EFF42042A |
| 0424:2744 | Standar... | USB2744                      | 6     | hub        | 6B2F198C59 |
| 0424:5744 | Standar... | USB5744                      | 6     | hub        | 6B2F198C59 |
| 0451:8140 | Texas I... | TUSB8041 4-Port Hub          | 6     | hub        | 6C4DC05E0C |
| 0835:8500 | Action ... | USB2.0 Hub                   | 6     | hub        | F2981C1775 |
| 2001:f103 | D-Link     | DUB-H7 7-port USB 2.0 hub    | 6     | hub        | 106387761E |
| 2109:0817 | VIA Labs   | USB3.0 Hub                   | 6     | hub        | CB9F8EC46D |
| 2109:2817 | VIA Labs   | USB2.0 Hub                   | 6     | hub        | CB9F8EC46D |
| 0424:5434 | Standar... | Hub                          | 5     | hub        | 612620FA7A |
| 05e3:0618 | Genesys... | USB2.0 Hub                   | 5     | hub        | 4ECF87F4C0 |
| 05e3:0620 | Genesys... | USB3.0 Hub                   | 5     | hub        | 6C4DC05E0C |
| 17ef:1011 | Lenovo     | ThinkPad Dock                | 5     | hub        | 4E65ACCEC8 |
| 17ef:1012 | Lenovo     | ThinkPad Dock                | 5     | hub        | 4E65ACCEC8 |
| 0424:2137 | Standar... | USB2137B                     | 4     | hub        | 57349A1931 |
| 0424:2742 | Standar... | USB2742                      | 4     | hub        | 200F73880C |
| 0424:5537 | Standar... | USB5537B                     | 4     | hub        | 57349A1931 |
| 0424:5742 | Standar... | USB5742                      | 4     | hub        | 200F73880C |
| 04b4:6570 | Cypress... | USB2.0 Hub                   | 4     | hub        | 734FCAFBD7 |
| 2109:3431 | VIA Labs   | USB2.0 Hub                   | 4     | usbcore    | 3F35BDC85A |
| 413c:1003 | Dell       | Keyboard Hub                 | 4     | hub        | D1F4879B6F |
| 03eb:0902 | Atmel      | 4-Port Hub                   | 3     | hub        | 47293CF17D |
| 0409:0059 | NEC Com... | HighSpeed Hub                | 3     | hub        | 3F04C2C04F |
| 0424:2502 | Standar... | Hub                          | 3     | hub        | 836238ED9F |
| 0424:2734 | Standar... | USB2734                      | 3     | hub        | 8B270D4228 |
| 0451:8043 | Texas I... | Hub                          | 3     | hub        | 734FCAFBD7 |
| 04b4:2050 | Cypress... | hub                          | 3     | hub        | 8A274CAE76 |
| 05ac:1003 | Apple      | Hub in Pro Keyboard [Mits... | 3     | hub        | 3AE1406C26 |
| 08e4:01e7 | Pioneer    | USB-C PD Docking             | 3     | hub        | BF4A9DEE07 |
| 08e4:6504 | Pioneer    | USB-C PD Docking             | 3     | hub        | BF4A9DEE07 |
| 08e4:6506 | Pioneer    | USB-C PD Docking             | 3     | hub        | BF4A9DEE07 |
| 08e4:6508 | Pioneer    | USB-C PD Docking             | 3     | hub        | BF4A9DEE07 |
| 0e8f:0016 | GreenAsia  | 4 port USB 1.1 hub UH-174    | 3     | hub        | 5658B8D546 |
| 1a40:0801 | Terminu... | USB 2.0 Hub                  | 3     | hub        | 62F0543FBA |
| 1d5c:5002 | Fresco ... | USB3.0 Hub                   | 3     | hub        | 6C4DC05E0C |
| 1d5c:5012 | Fresco ... | USB2.0 Hub                   | 3     | hub        | 6C4DC05E0C |
| 413c:1010 | Dell       | USB 2.0 Hub [MTT]            | 3     | hub        | 0590FE5AAB |
| 03eb:3301 | Atmel      | at43301 4-Port Hub           | 2     | hub        | 0B838CBF78 |
| 0424:2517 | Standar... | Hub                          | 2     | hub        | 7CDD355784 |
| 0451:8041 | Texas I... | Hub                          | 2     | hub        | 734FCAFBD7 |

### Human interface (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05ac:8242 | Apple      | Built-in IR Receiver         | 72    | usbhid     | 9F753AC669 |
| 1770:ff00 | MSI EPF... | MSI EPF USB / LED controller | 72    | usbhid     | 3DF62034D2 |
| 0483:91d1 | STMicro... | SGS Thomson Microelectronics | 50    | usbhid     | 734FCAFBD7 |
| 2047:0855 | Texas I... | Invensense Embedded Motio... | 15    | usbhid     | B19B3500F2 |
| 048d:8350 | Integra... | ITE Device(8350)             | 10    | usbhid     | CC12571867 |
| 05ac:8240 | Apple      | Built-in IR Receiver         | 10    | usbhid     | FDEA938323 |
| 0079:0006 | DragonRise | PC TWIN SHOCK Gamepad        | 8     | usbhid     | B9B37F4A61 |
| 03eb:8417 | Atmel      | maXTouch Digitizer           | 8     | usbhid     | BB40D4536A |
| 0765:5010 | X-Rite     | X-Rite Pantone Color Sensor  | 7     | usbhid     | C087621D89 |
| 1038:1122 | SteelSe... | SteelSeries KLC              | 7     | usbhid     | DD39CC35AF |
| 0457:102b | Silicon... | SiS HID Touch Controller     | 6     | usbhid     | 1C95C123C9 |
| 048d:8386 | Integra... | ITE Device(8386)             | 6     | usbhid     | E1CD9FD6C1 |
| 187c:0524 | Alienware  | M17x                         | 6     | usbhid     | A1B30D6B32 |
| 0457:10c1 | Silicon... | SiS HID Touch Controller     | 5     | usbhid     | 7C8F86D1BD |
| 056a:00e6 | Wacom      | TPCE6                        | 5     | usbhid     | 659C24D76A |
| 17ef:3066 | Lenovo     | ThinkPad Thunderbolt 3 Do... | 5     | usbhid     | C15E0482CA |
| 187c:0530 | Alienware  | AW1517                       | 5     | usbhid     | 3C817B6AB2 |
| 187c:0550 | Alienware  | AW-ELC                       | 5     | usbhid     | 8B270D4228 |
| 0451:ca01 | Texas I... | USBtoI2C Solution            | 4     | usbhid     | 4926835893 |
| 056a:5193 | Wacom      | Pen and multitouch sensor    | 4     | usbhid     | AAB68A3B33 |
| 03eb:8807 | Atmel      | maXTouch Digitizer           | 3     | usbhid     | 19AB1B4351 |
| 03eb:8810 | Atmel      | maXTouch Digitizer           | 3     | usbhid     | 94000B6660 |
| 03eb:8814 | Atmel      | maXTouch Digitizer           | 3     | usbhid     | B19B3500F2 |
| 03eb:8819 | Atmel      | maXTouch Digitizer           | 3     | usbhid     | FE571546DB |
| 03eb:8a06 | Atmel      | maXTouch Digitizer           | 3     | usbhid     | 46408ABBB6 |
| 03eb:8a0b | Atmel      | maXTouch Digitizer           | 3     | usbhid     | 15AF322AD8 |
| 03eb:8a19 | Atmel      | maXTouch Digitizer           | 3     | usbhid     | DE79BB68E4 |
| 03eb:8a96 | Atmel      | maXTouch Digitizer           | 3     | usbhid     | A3E8CC3C5F |
| 03eb:8b0d | Atmel      |                              | 3     | usbhid     | 30A8F9D279 |
| 0424:274c | Standar... | Hub Controller               | 3     | usbhid     | 8B270D4228 |
| 0457:10c0 | Silicon... | SiS HID Touch Controller     | 3     | usbhid     | 4018847A8C |
| 0486:0186 | ASUS Co... | MultiTouch(TTI)              | 3     | usbhid     | 742C2CD59E |
| 054c:0268 | Sony       | Batoh Device / PlayStatio... | 3     | usbhid     | B9E21A89DA |
| 0765:5001 | X-Rite     | Huey PRO Colorimeter         | 3     | usbhid     | F3B59C4994 |
| 0810:0003 | Persona... | PlayStation Gamepad          | 3     | usbhid     | D7E4C674FB |
| 0b05:1726 | ASUSTek... | Laptop OLED Display          | 3     | usbhid     | 4AC478DF18 |
| 20b3:0a18 | Hanvon     | 10.1 Touch screen overlay    | 3     | usbhid     | 33A13ED635 |
| 2386:310e | Raydium    | Touch System                 | 3     | usbhid     | C731CA9F7F |
| 8087:0a04 | Intel      | Sensor Solution              | 3     | usbhid     | 686053FC6E |
| 03eb:211c | Atmel      | maXTouch Digitizer           | 2     | usbhid     | DCF693F16F |
| 03eb:843d | Atmel      | maXTouch Digitizer           | 2     | usbhid     | A7F7276E3D |
| 03eb:8a03 | Atmel      | maXTouch Digitizer           | 2     | usbhid     | C21BC28613 |
| 03eb:8a41 | Atmel      |                              | 2     | usbhid     | 109A5A43A0 |
| 03eb:8b03 | Atmel      |                              | 2     | usbhid     | 57144F6E19 |
| 043e:9802 | LG Elec... |                              | 2     | usbhid     | 76F306DE39 |
| 0457:102a | Silicon... | SiS HID Touch Controller     | 2     | usbhid     | A7E2CB0850 |
| 046d:c216 | Logitech   | F310 Gamepad [DirectInput... | 2     | usbhid     | 7FA8E3A5E5 |
| 04d8:0b28 | Microch... | Composite HID + CDC, APP-... | 2     | cdc_acm    | A154D9D080 |
| 05c6:f006 | Qualcomm   | ZTE HSUSB Device             | 2     | usbhid     | 6299339038 |
| 064f:2af9 | WIBU-Sy... | CmStick (HID, article no.... | 2     | usbhid     | 192FBE0755 |
| 06cb:11ef | Synaptics  | Touch Digitizer V04          | 2     | usbhid     | 1211294E7C |
| 06cb:1d10 | Synaptics  | Large Touch Screen           | 2     | usbhid     | 5C4E5B6717 |
| 06cb:7406 | Synaptics  | Synaptics                    | 2     | usbhid     | 33C33EF483 |
| 0b05:175b | ASUSTek... | Laptop OLED Display          | 2     | usbhid     | CA7D90434B |
| 1038:1124 | SteelSe... | SteelSeries ALC              | 2     | usbhid     | DD39CC35AF |
| 10d5:55a2 | Uni Cla... | 2Port KVMSwitcher            | 2     | usbhid     | 9D1BD57875 |
| 187c:0514 | Alienware  | Phantom                      | 2     | usbhid     | B285740A65 |
| 187c:0520 | Alienware  | M17x                         | 2     | usbhid     | F12FE135C9 |
| 187c:0521 | Alienware  | M14x                         | 2     | usbhid     | C8941B23F0 |
| 187c:0523 | Alienware  | M18x                         | 2     | usbhid     | 023510307E |
| 187c:0527 | Alienware  | AW13                         | 2     | usbhid     | 3FFC07F627 |
| 1b96:0006 | N-Trig     | DuoSense                     | 2     | usbhid     | D9F4490B79 |
| 2386:0403 | Raydium    | Touch System                 | 2     | usbhid     | 3831423C95 |
| 2687:fb01 | Fitbit     | Base Station                 | 2     | usbhid     | 5265C3F848 |
| 03eb:212a | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 39CA2BE7BB |
| 03eb:8206 | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 8CDA110B5E |
| 03eb:8434 | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 1A4FC8147A |
| 03eb:8801 | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 0A6F4AEE88 |
| 03eb:8803 | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 246B15A3A4 |
| 03eb:880c | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 469E04E0D5 |
| 03eb:880f | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 570FB5E431 |
| 03eb:8818 | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 060E65F5FA |
| 03eb:881a | Atmel      | maXTouch Digitizer           | 1     | usbhid     | E68D86F10F |
| 03eb:883c | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 954E70C2AD |
| 03eb:8a17 | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 7097FC58F7 |
| 03eb:8a1b | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 196F2953A5 |
| 03eb:8a1d | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 88868B4AEC |
| 03eb:8a1e | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 3A4276985F |
| 03eb:8b06 | Atmel      |                              | 1     | usbhid     | 6B2F198C59 |
| 03eb:8c0c | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 2BD69A999F |
| 03eb:8c1d | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 7BC4693CF8 |
| 0408:3001 | Quanta ... | Optical Touch Screen         | 1     | usbhid     | 7CB95E1C6E |
| 0409:02b9 | NEC Com... | PA271W                       | 1     | usbhid     | C3A3A1B313 |
| 0416:5020 | Winbond... | HID Transfer                 | 1     | usbhid     | 990275357C |
| 044f:b315 | ThrustM... | Firestorm Dual Analog 3      | 1     | usbhid     | F214FD4D73 |
| 0457:1013 | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 08D7361B9F |
| 0457:1029 | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 5E2D316669 |
| 0457:1067 | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 755C2430DA |
| 0457:1068 | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 48322C9E10 |
| 0457:10cc | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | 5B1076EA3C |
| 0457:10cd | Silicon... | SiS HID Touch Controller     | 1     | usbhid     | A4202D4AF8 |
| 046d:0a59 | Logitech   | UE ROLL                      | 1     | usbhid     | 3CAEE36AD0 |
| 046d:c20c | Logitech   | WingMan Precision            | 1     | usbhid     | C119897406 |
| 046d:c21a | Logitech   | Precision Gamepad            | 1     | usbhid     | 642A715A14 |
| 046d:c21c | Logitech   | G13 Advanced Gameboard       | 1     | usbhid     | 773805FC90 |
| 046d:c222 | Logitech   | G15 Keyboard / LCD           | 1     | usbhid     | 9E7EE522A9 |
| 046d:c283 | Logitech   | WingMan Force 3D             | 1     | usbhid     | 9D05747E79 |
| 0483:cdab | STMicro... | U2F-token (EFM32)            | 1     | usbhid     | E6373D6E84 |
| 04b4:fd13 | Cypress... | Programmable power socket    | 1     | usbhid     | 26FBFEC0EA |
| 04d8:0033 | Microch... | PICkit2                      | 1     | usbhid     | 63BB1E00C9 |

### Imaging (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0421:051a | Nokia M... | Nokia N9                     | 1     | cdc_pho... | B9B81226FB |
| 04a7:04d0 | Visioneer  | Xerox DocuMate 5460          | 1     |            | 4ABAC242CC |

### Infrared (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 147a:e017 | Formosa... | eHome Infrared Receiver      | 2     | mceusb     | A1471119F3 |
| 066f:4200 | SigmaTel   | STIr4200 IrDA Bridge         | 1     |            | 6682A76496 |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c52b | Logitech   | Unifying Receiver            | 632   | usbhid     | 2B5F0594D3 |
| 062a:4101 | MosArt ... | Wireless Keyboard/Mouse      | 424   | usbhid     | 1579402536 |
| 09da:054f | A4Tech     | USB Device                   | 353   | usbhid     | DCE9CDAF8E |
| 046d:c534 | Logitech   | Unifying Receiver            | 350   | usbhid     | 7637F3DE5F |
| 045e:0745 | Microsoft  | Nano Transceiver v1.0 for... | 144   | usbhid     | B19B3500F2 |
| 09da:9090 | A4Tech     | XL-730K / XL-750BK / XL-7... | 140   | usbhid     | AE0E410A7F |
| 1c4f:0002 | SiGma M... | Keyboard TRACER Gamma Ivory  | 108   | usbhid     | 8B3744250A |
| 046d:c31c | Logitech   | Keyboard K120 for Business   | 57    | usbhid     | 11B9C937C9 |
| 046d:c52e | Logitech   | MK260 Wireless Combo Rece... | 57    | usbhid     | 891002E8F2 |
| 24ae:2000 | RAPOO      | 2.4G Wireless Device         | 55    | usbhid     | 415CFBF22F |
| 24ae:2001 | RAPOO      | 5G Wireless Device           | 52    | usbhid     | B8392B7335 |
| 045e:07b2 | Microsoft  | 2.4GHz Transceiver v8.0 u... | 50    | usbhid     | F03FCA81E0 |
| 045e:07fd | Microsoft  | Nano Transceiver 1.1         | 49    | usbhid     | 71C7CF2056 |
| 0e8f:00a7 | GreenAsia  | 2.4G RX                      | 49    | usbhid     | 1CA46CE89B |
| 0a5c:4502 | Broadcom   | Keyboard (Boot Interface ... | 47    | usbhid     | 55CD396670 |
| 062a:3286 | MosArt ... | Nano Receiver [Sandstrom ... | 46    | usbhid     | F47F34752E |
| 1d57:fa20 | Xenta      | 2.4G Receiver                | 44    | usbhid     | F043044546 |
| 04d9:1702 | Holtek ... | Keyboard LKS02               | 38    | usbhid     | 28C1100253 |
| 1a81:1004 | Holtek ... | Wireless Dongle              | 37    | usbhid     | ABFD882CFE |
| 248a:8566 | Maxxter    | SVEN RX 360 Art Wireless ... | 37    | usbhid     | F63B72DAC0 |
| 413c:8161 | Dell       | Integrated Keyboard          | 32    | usbhid     | 6722706FA2 |
| 03f0:a407 | Hewlett... | Wireless Optical Comfort ... | 27    | usbhid     | AB33DE8D3B |
| 1a2c:2d23 | China R... | USB Keyboard                 | 27    | usbhid     | 40EE672280 |
| 1d57:fa60 | Xenta      | 2.4G Receiver                | 27    | usbhid     | 1A1634FC24 |
| 04f3:0103 | Elan Mi... | ActiveJet K-2024 Multimed... | 26    | usbhid     | 1CC0697D7C |
| 09da:90a0 | A4Tech     | USB Device                   | 25    | usbhid     | F801DA09F7 |
| 1a2c:0e24 | China R... | USB Keyboard                 | 25    | usbhid     | CB9F8EC46D |
| 1c4f:0026 | SiGma M... | Keyboard                     | 25    | usbhid     | DB9250D9E0 |
| 0603:0002 | Novatek... | USB Composite Device         | 23    | usbhid     | CEAC334581 |
| 0e8f:00a5 | GreenAsia  | 2.4G RX                      | 22    | usbhid     | 8BF0975FB2 |
| 04d9:1503 | Holtek ... | Shortboard Lefty             | 21    | usbhid     | 9ED9651BBD |
| 09da:9066 | A4Tech     | F3 V-Track Gaming Mouse      | 21    | usbhid     | C8471427CB |
| 04d9:1603 | Holtek ... | Keyboard                     | 20    | usbhid     | 58906B69C1 |
| 1a2c:2124 | China R... | USB Keyboard                 | 20    | usbhid     | 6F9734843A |
| 1ea7:0002 | SHARKOO... | Trust Wireless Keyboard &... | 20    | usbhid     | 0994A3EEB3 |
| 05ac:0236 | Apple      | Internal Keyboard/Trackpa... | 19    | bcm5974    | 9F753AC669 |
| 062a:4182 | MosArt ... | 2.4G Keyboard Mouse          | 19    | usbhid     | D607079392 |
| 1a2c:0c23 | China R... | USB Keyboard                 | 19    | usbhid     | 06814257C1 |
| 05ac:0253 | Apple      | Internal Keyboard/Trackpa... | 18    | bcm5974    | 16BDB52C40 |
| 062a:5918 | MosArt ... | 2.4G Keyboard Mouse          | 18    | usbhid     | 8F2E060D39 |
| 0e8f:00a4 | GreenAsia  | 2.4G RX                      | 18    | usbhid     | 3A39EF0147 |
| 1a2c:2c27 | China R... | USB Keyboard                 | 18    | usbhid     | 5D84E9444A |
| 1ea7:0066 | SHARKOO... | [Mediatrack Edge Mini Key... | 18    | usbhid     | E5C9F7A373 |
| 09da:f613 | A4Tech     | USB Device                   | 17    | usbhid     | 3F32C54B81 |
| 05ac:0237 | Apple      | Internal Keyboard/Trackpa... | 16    | bcm5974    | 80ECBDE76E |
| 0458:6001 | KYE Sys... | GF3000F Ethernet Adapter     | 15    | usbhid     | 806B890CCF |
| 09da:0260 | A4Tech     | KV-300H Isolation Keyboard   | 15    | usbhid     | F9F36BC2A4 |
| 413c:8157 | Dell       | Integrated Keyboard          | 15    | usbhid     | ABDBB02998 |
| 6080:8060 | SINO WE... | USB KEYBOARD                 | 15    | usbhid     | 514C494F7F |
| 1017:1006 | Speedy ... | Generic USB Mouse            | 14    | usbhid     | E5412A533C |
| 248a:ff0f | Maxxter    | Wireless Receiver            | 14    | usbhid     | 7C8E8CFB76 |
| 2571:4101 | KOSEL      | KSL81P304                    | 14    | usbhid     | D0EE7E3A4B |
| 09da:9033 | A4Tech     | X-718BK Optical Mouse        | 13    | usbhid     | EAC50792BA |
| 0b05:1869 | ASUSTek... | ITE Device(8910)             | 13    | usbhid     | C53DF5F083 |
| 1915:0f01 | Nordic ... | Wireless Receiver            | 13    | usbhid     | 37A9E829F4 |
| 413c:2107 | Dell       | USB Entry Keyboard           | 13    | usbhid     | 2E9A3BAB26 |
| 040b:2013 | Weltren... | 2.4G Wireless Keyboard&Mouse | 12    | usbhid     | E6FEEBB106 |
| 044e:3013 | Alps El... | Electric Keyboard            | 12    | usbhid     | C257EC2DD4 |
| 045e:00db | Microsoft  | Natural Ergonomic Keyboar... | 12    | usbhid     | 8EB32C768B |
| 045e:07f8 | Microsoft  | Wired Keyboard 600 (model... | 12    | usbhid     | 97D9353E2A |
| 05ac:0252 | Apple      | Internal Keyboard/Trackpa... | 12    | bcm5974    | EB523651C8 |
| 258a:6a88 | EBITS-E... | USB KEYBOARD                 | 12    | usbhid     | 4414412FCB |
| 25a7:2402 | 2.4G       | Wireless Device              | 12    | usbhid     | 3EF3EE8B41 |
| 03f0:0024 | Hewlett... | KU-0316 Keyboard             | 11    | usbhid     | AF5F7096D5 |
| 04b3:3025 | IBM        | NetVista Full Width Keyboard | 11    | usbhid     | 0A9618F99E |
| 0518:0001 | EzKEY      | USB to PS2 Adaptor v1.09     | 11    | usbhid     | 1A816E6EBB |
| 0b05:1854 | ASUSTek... | ITE Device(8910)             | 11    | usbhid     | 1A816E6EBB |
| 1ea7:0004 | SHARKOO... | GiGa Hid                     | 11    | usbhid     | 79D7D692AC |
| 25a7:0701 | Smart      | Wireless Device              | 11    | usbhid     | D02CB45D1E |
| 25a7:fa61 | Compx      | 2.4G Receiver                | 11    | usbhid     | 0D36FA2146 |
| 045e:07a5 | Microsoft  | 2.4GHz Transceiver v9.0      | 10    | usbhid     | 0FCCED0386 |
| 0b05:1866 | ASUSTek... | N-KEY Device                 | 10    | usbhid     | 383A34EDD1 |
| 1018:1006 |            | Keyboard                     | 10    | usbhid     | 176970ABB8 |
| 1a2c:0c21 | China R... | USB Keyboard                 | 10    | usbhid     | 1B1C811590 |
| 1bcf:05ca | Sunplus... | G-CUBE Wireless 2.4G Opti... | 10    | usbhid     | DECB236525 |
| 413c:2003 | Dell       | Keyboard                     | 10    | usbhid     | 4B70A9D252 |
| 413c:2501 | Dell       | KM632 Wireless Keyboard a... | 10    | usbhid     | 1536A4CF47 |
| 413c:8505 | Dell       | Universal Receiver           | 10    | usbhid     | 1675040CCD |
| 040b:2014 | Weltren... | 2.4GHz keyboard and mouse    | 9     | usbhid     | 0D37B17DDF |
| 045e:00dd | Microsoft  | Comfort Curve Keyboard 20... | 9     | usbhid     | 3C07832726 |
| 0566:3002 | Montere... | Keyboard                     | 9     | usbhid     | A6CA528D43 |
| 0603:00f2 | Novatek... | Keyboard (Labtec Ultra Fl... | 9     | usbhid     | AC96DD45F9 |
| 046d:c31d | Logitech   | Media Keyboard K200          | 8     | usbhid     | 837B8CC572 |
| 046d:c517 | Logitech   | LX710 Cordless Desktop Laser | 8     | usbhid     | 32842E60B0 |
| 062a:4127 | MosArt ... | 2.4G Keyboard Mouse          | 8     | usbhid     | 0C33FBFDFF |
| 0911:2188 | Philips... | Hantick Wireless Device      | 8     | usbhid     | 30C34AB5A7 |
| 1d57:32da | Xenta      | 2.4GHz Receiver (Keyboard... | 8     | usbhid     | 447CFAC278 |
| 258a:0001 | SINO WE... | USB KEYBOARD                 | 8     | usbhid     | 2AA811D1A8 |
| 3938:1032 | MOSART ... | 2.4G RF Keyboard & Mouse     | 8     | usbhid     | 1CCC5C7074 |
| 413c:2113 | Dell       | KB216 Wired Keyboard         | 8     | usbhid     | CA4D5C11F7 |
| 045e:0750 | Microsoft  | Wired Keyboard 600           | 7     | usbhid     | D3138923BC |
| 04f2:0976 | Chicony... | Wireless Device              | 7     | usbhid     | B0FBAF7694 |
| 04fc:05d8 | Sunplus... | Wireless keyboard/mouse      | 7     | usbhid     | C0C73D01BA |
| 062a:4c01 | MosArt ... | 2.4G Keyboard Mouse          | 7     | usbhid     | E189A54BAB |
| 09da:057f | A4Tech     | USB Device                   | 7     | usbhid     | C00DAFA25C |
| 0c45:0520 | Microdia   | MaxTrack Wireless Mouse      | 7     | usbhid     | 8F3BFABCFA |
| 1050:0407 | Yubico.com | Yubikey 4 OTP+U2F+CCID       | 7     | usbhid     | AAB68A3B33 |
| 1c4f:0016 | SiGma M... | USB Keyboard                 | 7     | usbhid     | DD25592A09 |
| 1d57:fa21 | Xenta      | 2.4G Receiver                | 7     | usbhid     | 50FFA5DCEC |
| 258a:000c | HAILUCK    | USB KEYBOARD                 | 7     | usbhid     | 9E88CD9DFE |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c52f | Logitech   | Unifying Receiver            | 707   | usbhid     | 6F94FCD1C0 |
| 093a:2510 | Pixart ... | Optical Mouse                | 453   | usbhid     | DC6C804E81 |
| 0458:003a | KYE Sys... | NetScroll+ Mini Traveler ... | 401   | usbhid     | 048229855F |
| 046d:c077 | Logitech   | M105 Optical Mouse           | 209   | usbhid     | 4F9294F4B5 |
| 09da:000a | A4Tech     | Optical Mouse Opto 510D /... | 203   | usbhid     | 73E5B46F66 |
| 046d:c05a | Logitech   | M90/M100 Optical Mouse       | 196   | usbhid     | D2006E7A87 |
| 275d:0ba6 |            | USB OPTICAL MOUSE            | 178   | usbhid     | 86987CF1ED |
| 0000:0538 |            | USB OPTICAL MOUSE            | 177   | usbhid     | 4D00578B8B |
| 1ea7:0064 | SHARKOO... | 2.4G Wireless Mouse          | 160   | usbhid     | 6722706FA2 |
| 062a:4102 | MosArt ... | 2.4G Wireless Mouse          | 142   | usbhid     | 0FCCED0386 |
| 1c4f:0034 | SiGma M... | Usb Mouse                    | 130   | usbhid     | 240FE2C985 |
| 093a:2521 | Pixart ... | Optical Mouse                | 113   | usbhid     | A1D2E02773 |
| 248a:8367 | Maxxter    | SVEN Comfort 3400 Wireless   | 113   | usbhid     | C5ADBBB2B3 |
| 248a:8366 | Maxxter    | Wireless Optical Mouse AC... | 109   | usbhid     | 1B0893029F |
| 1bcf:0005 | Sunplus... | USB Optical Mouse            | 100   | usbhid     | BF4A9DEE07 |
| 09da:c10a | A4Tech     | USB Mouse                    | 99    | usbhid     | E8755C7EF8 |
| 10c4:8105 | Cygnal ... | USB OPTICAL MOUSE            | 87    | usbhid     | 51388B8407 |
| 15d9:0a4f | Trust I... | Optical Mouse                | 67    | usbhid     | B2A7C69D92 |
| 04b4:0060 | Cypress... | Wireless optical mouse       | 63    | usbhid     | 5A0A25C9FF |
| 0e8f:00fb | GreenAsia  | USB Mouse                    | 63    | usbhid     | 0D71E52A34 |
| 1bcf:0007 | Sunplus... | Optical Mouse                | 61    | usbhid     | 7414D62366 |
| 0101:0007 |            | USB OPTICAL MOUSE            | 60    | usbhid     | B95AF01C35 |
| 1a2c:0042 | China R... | Usb Mouse                    | 59    | usbhid     | 153247F60A |
| 13ee:0001 | MosArt     | Optical Mouse                | 52    | usbhid     | 2E7D16AEC3 |
| 25a7:fa23 | Compx      | 2.4G Receiver                | 52    | usbhid     | 05F2682C5B |
| 046d:c05b | Logitech   | M-U0004 810-001317 [B110 ... | 51    | usbhid     | 861B54C65A |
| 046d:c016 | Logitech   | Optical Wheel Mouse          | 49    | usbhid     | D7977A3B0E |
| 046d:c06c | Logitech   | Optical Mouse                | 48    | usbhid     | 9A249DA8EE |
| 2188:0ae1 | CalDigit   | USB OPTICAL MOUSE            | 48    | usbhid     | 42DF53B120 |
| 0a5c:4503 | Broadcom   | Mouse (Boot Interface Sub... | 47    | usbhid     | 55CD396670 |
| 276d:1160 | YSTEK      | G Mouse                      | 46    | usbhid     | 4038A574AC |
| 046d:c050 | Logitech   | RX 250 Optical Mouse         | 45    | usbhid     | 15E5D2BB9D |
| 192f:0916 | Avago T... | USB Optical Mouse            | 43    | usbhid     | 03073BAAD2 |
| 3938:1031 | MOSART ... | 2.4G Wireless Mouse          | 43    | usbhid     | 26FD2C3388 |
| 04f3:0235 | Elan Mi... | Optical Mouse                | 41    | usbhid     | 61625F7FA6 |
| 18f8:0f97 | [Maxxter]  | USB OPTICAL MOUSE            | 39    | usbhid     | 6F9734843A |
| 046d:c062 | Logitech   | M-UAS144 [LS1 Laser Mouse]   | 37    | usbhid     | A31B1E8E5E |
| 046d:c019 | Logitech   | Optical Tilt Wheel Mouse     | 36    | usbhid     | E1D809E15F |
| 15d9:0a4d | Trust I... | Optical Mouse                | 36    | usbhid     | 4B70A9D252 |
| 279e:024e | 2.4G wi... | 2.4G wireless USB Device     | 35    | usbhid     | 6FC7FB8C0E |
| 093a:2516 | Pixart ... | USB OPTICAL MOUSE            | 32    | usbhid     | 98E313C3D3 |
| 248a:8514 | Maxxter    | Wireless Receiver            | 32    | usbhid     | 4414412FCB |
| 413c:8162 | Dell       | Integrated Touchpad [Syna... | 32    | usbhid     | 6722706FA2 |
| 1d57:0008 | Xenta      | 2.4G Wireless Optical Mouse  | 31    | usbhid     | 268D53A8B4 |
| 0000:3825 |            | USB OPTICAL MOUSE            | 29    | usbhid     | 54C92BF69C |
| 046d:c018 | Logitech   | Optical Wheel Mouse          | 29    | usbhid     | 74624FF493 |
| 045e:00cb | Microsoft  | Basic Optical Mouse v2.0     | 27    | usbhid     | 3A4D90A1C3 |
| 04fc:05da | Sunplus... | SPEEDLINK SNAPPY Wireless... | 27    | usbhid     | C77FA19C2B |
| 1d57:0001 | Xenta      | wireless device              | 27    | usbhid     | E3B1F6D810 |
| 046d:c069 | Logitech   | M-U0007 [Corded Mouse M500]  | 25    | usbhid     | 4CED599618 |
| 15d9:0a4c | Trust I... | USB+PS/2 Optical Mouse       | 25    | usbhid     | 9E9396445A |
| 1c4f:0003 | SiGma M... | HID controller               | 25    | usbhid     | 3822FA2401 |
| 045e:0040 | Microsoft  | Wheel Mouse Optical          | 24    | usbhid     | 1CCEC0E2DB |
| 0461:4d0f | Primax ... | HP Optical Mouse             | 23    | usbhid     | 52E1A1211A |
| 046d:c526 | Logitech   | Nano Receiver                | 23    | usbhid     | 4C990A61B6 |
| 04d9:0499 | Holtek ... | Optical Mouse                | 23    | usbhid     | 8B3F86E523 |
| 248a:8564 | Maxxter    | SVEN RX 305 Wireless Mouse   | 22    | usbhid     | 6F74CC29B9 |
| 045e:0083 | Microsoft  | Basic Optical Mouse          | 21    | usbhid     | EC62FDB035 |
| 046d:c03e | Logitech   | Premium Optical Wheel Mou... | 21    | usbhid     | 500C2BCFBC |
| 1bcf:053a | Sunplus... | Targa Silvercrest OMC807-... | 21    | usbhid     | 48F4462204 |
| 046d:c51b | Logitech   | V220 Cordless Optical Mou... | 20    | usbhid     | 1FF111737A |
| 18f8:0f99 | [Maxxter]  | Optical gaming mouse         | 20    | usbhid     | C15E0482CA |
| 24ae:1100 | RAPOO      | 2.4G Wireless Device         | 20    | usbhid     | F4928B0ED4 |
| 0458:0186 | KYE Sys... | Wired Mouse                  | 19    | usbhid     | 7637F3DE5F |
| 1c4f:0032 | SiGma M... | Usb Mouse                    | 19    | usbhid     | F72534E2D2 |
| 04f3:0234 | Elan Mi... | Optical Mouse                | 18    | usbhid     | C75876E318 |
| 09da:8090 | A4Tech     | X-718BK Oscar Optical Gam... | 18    | usbhid     | 163611DC70 |
| 17ef:6019 | Lenovo     | Lenovo USB Optical Mouse     | 18    | usbhid     | 8533A07584 |
| 046d:c00e | Logitech   | M-BJ58/M-BJ69 Optical Whe... | 17    | usbhid     | 059F7C034E |
| 04fc:0538 | Sunplus... | Wireless Optical Mouse 2.... | 17    | usbhid     | 2ACD24AD30 |
| 09da:000e | A4Tech     | X-F710F Optical Mouse 3xF... | 17    | usbhid     | 643DD308BF |
| 3938:1080 | YK         | 2.4G Wireless Device         | 17    | usbhid     | F7B38B6B8B |
| 413c:301a | Dell       | MS116 USB Optical Mouse      | 17    | usbhid     | EB49973873 |
| 046d:c05f | Logitech   | M115 Optical Mouse           | 16    | usbhid     | DFCB7E23DA |
| 04b4:0033 | Cypress... | Mouse                        | 16    | usbhid     | F5F1021D04 |
| 09da:0006 | A4Tech     | Optical Mouse WOP-35 / Tr... | 16    | usbhid     | A3E8CC3C5F |
| 0458:00e3 | KYE Sys... | 2.4G Wireless Mouse          | 15    | usbhid     | E5CBC5EE02 |
| 045e:0797 | Microsoft  | Optical Mouse 200            | 15    | usbhid     | 7B7C9D230C |
| 062a:0000 | MosArt ... | Optical mouse                | 15    | usbhid     | 2202C8E39F |
| 10c4:8108 | Cygnal ... | USB OPTICAL MOUSE            | 15    | usbhid     | AF291D02CF |
| 192f:0416 | Avago T... | ADNS-5700 Optical Mouse C... | 15    | usbhid     | 227C77AB8B |
| 2635:0601 |            | 2.4G RF MOUSE                | 15    | usbhid     | 2BC400D84F |
| 413c:8158 | Dell       | Integrated Touchpad / Tra... | 15    | usbhid     | ABDBB02998 |
| 89e5:101b |            | USB OPTICAL MOUSE            | 15    | usbhid     | E3825A8890 |
| 046d:c058 | Logitech   | M115 Mouse                   | 14    | usbhid     | 31EDA21A72 |
| 046d:c521 | Logitech   | Cordless Mouse Receiver      | 14    | usbhid     | AC0C824624 |
| 0458:014f | KYE Sys... | 2.4G Wireless Mouse          | 13    | usbhid     | 9AC4ABA49A |
| 045e:00e1 | Microsoft  | Wireless Laser Mouse 6000... | 13    | usbhid     | 1EF9D813A6 |
| 0581:0101 | Racal D... | Racal Generic USB Mouse      | 13    | usbhid     | 26399C140A |
| 044e:3012 | Alps El... | Electric Generic USB Mouse   | 12    | usbhid     | C257EC2DD4 |
| 046d:c040 | Logitech   | Corded Tilt-Wheel Mouse      | 12    | usbhid     | B019AFD4C1 |
| 046d:c408 | Logitech   | Marble Mouse (4-button)      | 12    | usbhid     | 5320C2BFA7 |
| 04b3:310c | IBM        | Wheel Mouse                  | 12    | usbhid     | 6E4F973484 |
| 062a:4106 | MosArt ... | SVEN RX-525 SILENT WIRELE... | 12    | usbhid     | ED3F66DCA7 |
| 1a2c:0044 | China R... | Usb Mouse                    | 12    | usbhid     | 3E0ED41BC7 |
| 1d57:0016 | Xenta      | HID Wireless Mouse           | 12    | usbhid     | BAC4825744 |
| 03f0:0641 | Hewlett... | X1200 USB Optical Mouse      | 11    | usbhid     | 6ADE50F197 |
| 045e:0084 | Microsoft  | Basic Optical Mouse          | 11    | usbhid     | DDF30C670A |
| 045e:0737 | Microsoft  | Compact Optical Mouse 500    | 11    | usbhid     | 7D8551E612 |
| 046d:c045 | Logitech   | Optical Mouse                | 11    | usbhid     | 08920BC878 |

### Isdn adapter (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| ffff:ffff | RAPOO      | AVM FRITZ!Card PCMCIA        | 4     | usbhid     | 7BAB5ED394 |

### Joystick (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 07b5:0317 | Mega Wo... | USB Game Controllers         | 1     | usbhid     | E03A8C0C89 |
| 0e6f:0119 | Logic3     | MW3 Wireless Controller f... | 1     | usbhid     | 2933DDC278 |
| 145f:01bb | Trust      | Gamepad                      | 1     | usbhid     | 708CD13E0A |

### Mfp (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:222a | Hewlett... | LaserJet Pro MFP M125rnw     | 6     | usblp      | 2B156B0191 |
| 0924:42af | Xerox      | WorkCentre 3045B             | 5     | usblp      | 0547EE6C45 |
| 0482:0495 | Kyocera    | FS-1020MFP                   | 4     | usblp      | DCFF794CD3 |
| 03f0:042a | Hewlett... | LaserJet M1132 MFP           | 3     | usblp      | C14F03D2C0 |
| 03f0:052a | Hewlett... | LaserJet Professional M12... | 3     | usblp      | EC9F5A20B2 |
| 03f0:3b17 | Hewlett... | LaserJet M1005 MFP           | 2     | usblp      | 1C1D0D6198 |
| 0924:42c4 | Xerox      | WorkCentre 3615              | 2     | usblp      | 0BF7A6E91D |
| 03f0:1d2a | Hewlett... | Color LaserJet flow MFP M880 | 1     | usblp      | 9F2622FF85 |
| 03f0:242a | Hewlett... | Color LaserJet Pro MFP M176n | 1     | usblp      | 45CE8F3BAB |
| 03f0:252a | Hewlett... | LaserJet 500 colorMFP M570dw | 1     | usblp      | 8EA70251FB |
| 03f0:2d2a | Hewlett... | LaserJet Pro MFP M225dw      | 1     | usblp      | 589D629D00 |
| 03f0:2e2a | Hewlett... | LaserJet Pro MFP M435nw      | 1     | usblp      | A457D54FAD |
| 03f0:322a | Hewlett... | LaserJet Pro MFP M127fn      | 1     | usblp      | 35F08E0E86 |
| 03f0:362a | Hewlett... | Officejet Color FlowMFP X585 | 1     | usblp      | 4966DF06F4 |
| 03f0:3b2a | Hewlett... | Color LaserJet MFP M277dw    | 1     | usblp      | A76C26FACB |
| 03f0:442a | Hewlett... | Color LaserJet Flow MFP M680 | 1     | usblp      | BF4C79032E |
| 03f0:9e17 | Hewlett... | LaserJet 500 MFP M525        | 1     | usblp      | 80E0F6373A |
| 03f0:9f17 | Hewlett... | LaserJet 500 color MFP M575  | 1     | usblp      | A043EA04EA |
| 03f0:bf2a | Hewlett... | LaserJet MFP M28-M31         | 1     | usblp      | 7C26C8530E |
| 04b8:083f | Seiko E... | Stylus CX4300/CX4400/CX55... | 1     | usblp      | DAA3F452F5 |
| 0924:3d6b | Xerox      | WorkCentre 6505DN            | 1     | usblp      | 66BC368A83 |
| 0924:3d6f | Xerox      | WorkCentre 6605DN            | 1     | usblp      | E45A12C489 |
| 0924:42d4 | Xerox      | WorkCentre 6027              | 1     | usblp      | E363684B4B |
| 0924:42da | Xerox      | WorkCentre 3025              | 1     | usblp      | AEF3A63493 |
| 0924:42db | Xerox      | WorkCentre 3215              | 1     | usblp      | A9F64AC1B2 |

### Miscellaneous (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 2e04:c008 | HMD Global | Nokia 7 plus                 | 1     | rndis_host | F652C9DD47 |

### Modem (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 12d1:1506 | Huawei ... | E398 LTE/UMTS/GSM Modem/N... | 160   | uas, us... | 7B7C9D230C |
| 0bdb:1911 | Ericsso... | F5521gw                      | 27    | cdc_acm    | FBF8462F41 |
| 0bdb:1926 | Ericsso... | H5321 gw Mobile Broadband... | 19    | cdc_acm    | E70E5228B2 |
| 12d1:1436 | Huawei ... | E173 3G Modem (modem-mode)   | 18    | usb_sto... | 766B0474A3 |
| 0bdb:1900 | Ericsso... | F3507g Mobile Broadband M... | 16    | cdc_acm    | A23E000798 |
| 12d1:1001 | Huawei ... | E161/E169/E620/E800 HSDPA... | 14    | usb_sto... | 7398A928CD |
| 03f0:3d1d | Hewlett... | hs2350 HSPA+ MobileBroadband | 13    | cdc_acm    | 79EFD09885 |
| 27c6:5301 | HTMicro... | Goodix Fingerprint Device    | 13    | cdc_acm    | 4E3F5803B1 |
| 27c6:5395 | HTMicro... | Goodix Fingerprint Device    | 13    | cdc_acm    | 7A9639F003 |
| 03f0:3a1d | Hewlett... | hs2340 HSPA+ mobile broad... | 11    | cdc_acm    | 13780FAE80 |
| 12d1:1003 | Huawei ... | E220 HSDPA Modem / E230/E... | 11    | usb_sto... | 156575EAC4 |
| 0bdb:193e | Ericsso... | N5321 gw                     | 10    | cdc_acm    | B34F861676 |
| 27c6:5385 | HTMicro... | Goodix Fingerprint Device    | 10    | cdc_acm    | E80894BD2A |
| 2341:0043 | Arduino SA | Uno R3 (CDC ACM)             | 8     | cdc_acm    | 926753D3C7 |
| 413c:8147 | Dell       | F3507g Mobile Broadband M... | 8     | cdc_acm    | F474D6B56B |
| 413c:818e | Dell       | Dell Wireless 5560 Single... | 8     | cdc_acm    | 2E0081C6C9 |
| 12d1:1404 | Huawei ... | EM770W miniPCI WCDMA Modem   | 7     | usb_sto... | 1D4EF489F6 |
| 12d1:140c | Huawei ... | E180v                        | 7     | usb_sto... | C9720C1E5D |
| 413c:818d | Dell       | DW5550                       | 7     | cdc_acm    | 8C26DF88EE |
| 413c:8184 | Dell       | F3607gw v2 Mobile Broadba... | 5     | cdc_acm    | 7ED1F06568 |
| 12d1:14ac | Huawei ... | HUAWEI Mobile                | 4     | option     | 167B3FD913 |
| 1edf:6004 | Select ... | MCD-640S-1EDF-6004           | 4     | cdc_acm    | F9D3A09989 |
| 04e8:6872 | Samsung... | Kiera                        | 3     | cdc_acm    | 9A145A3041 |
| 12d1:1570 | Huawei ... | Mobile Broadband Module      | 3     | option     | 63B03B7E15 |
| 1519:0020 | Comneon    | HSIC Device                  | 3     | cdc_acm    | E277646CEF |
| 1546:01a5 | U-Blox     | [u-blox 5]                   | 3     | cdc_acm    | B46F2FEE35 |
| 19d2:1515 | ZTE WCD... | MF192                        | 3     | cdc_acm    | 3C75190E68 |
| 8087:0ab6 | Intel      | UDOO X86                     | 3     | cdc_acm    | AF7921E479 |
| 0bdb:190a | Ericsso... | F3307 Mobile Broadband Mo... | 2     | cdc_acm    | 4C648779A3 |
| 0e8d:0003 | MediaTek   | MT6227 phone                 | 2     | cdc_acm    | 380509822C |
| 27c6:5381 | HTMicro... | Goodix Fingerprint Device    | 2     | cdc_acm    | 7872901FE9 |
| 03f0:2f1d | Hewlett... | lc2010 Mobile Broadband M... | 1     | cdc_acm    | 34C71843B7 |
| 0421:01d0 | Nokia M... | E52                          | 1     | cdc_acm    | D649B2DEB3 |
| 0421:0223 | Nokia M... | E72-1                        | 1     | cdc_acm    | A913DFEFEE |
| 0421:0302 | Nokia M... | N8-00                        | 1     | cdc_acm    | A8AF42D6E7 |
| 0421:0348 | Nokia M... | Nokia 5228                   | 1     | cdc_acm    | 4F7EEE94F1 |
| 0421:0360 | Nokia M... | C1-01 Ovi Suite Mode         | 1     | cdc_acm    | 9C2894BEA0 |
| 0421:0524 | Nokia M... | Nokia 300                    | 1     | cdc_acm    | B96E92098E |
| 0421:05fd | Nokia M... | 202                          | 1     | cdc_acm    | 1A8A955A5B |
| 0421:0638 | Nokia M... | Nokia USB Modem              | 1     | cdc_acm    | 2D146B746C |
| 0461:0033 | Primax ... | USB_Focus                    | 1     | cdc_acm    | 5278A91530 |
| 04e8:6601 | Samsung... | Mobile Phone                 | 1     | visor, ... | 25239307A1 |
| 04e8:6773 | Samsung... | HSPA Modem                   | 1     | cdc_acm    | 06B0B62AAC |
| 04e8:6843 | Samsung... | E2530 Phone (Samsung Kies... | 1     | cdc_acm    | 55075885CA |
| 0572:1329 | Conexan... | USB Modem                    | 1     | cdc_acm    | 7A44DA1E2F |
| 0572:cb16 | Conexan... | USB-ADSL Modem               | 1     |            | 07217A2477 |
| 0658:0200 | Sigma D... | Aeotec Z-Stick Gen5 (ZW09... | 1     | cdc_acm    | 6B964CD335 |
| 0930:1314 | Toshiba    | F5521gw                      | 1     | cdc_acm    | 6F44CBE917 |
| 0930:1319 | Toshiba    | H5321gw                      | 1     | cdc_acm    | 68749635C1 |
| 0bdb:1902 | Ericsso... | F3507g v2 Mobile Broadban... | 1     | cdc_acm    | 211BE91D80 |
| 0bdb:1905 | Ericsso... | F3607gw v2 Mobile Broadba... | 1     | cdc_acm    | 26A4676050 |
| 12d1:1465 | Huawei ... | K3765 HSPA                   | 1     | option     | 346F4EA91D |
| 12d1:1566 | Huawei ... | HUAWEI_MOBILE                | 1     | option     | B07034F89E |
| 12d1:1573 | Huawei ... | Mobile                       | 1     | option     | 740C1D3CBF |
| 12d1:1c1e | Huawei ... | Mass Storage                 | 1     | uas, us... | C05B8DEF55 |
| 1546:01a7 | U-Blox     | u-blox 7 - GPS/GNSS Receiver | 1     | cdc_acm    | 42FDA138C2 |
| 19d2:0579 | ZTE WCD... | ZXIC Mobile Boardband        | 1     | rndis_host | F00E135A18 |
| 1bbb:2011 | T & A M... | ALCATEL ONETOUCH POP 3 (5.5) | 1     | cdc_acm    | 03EBD4DBF1 |
| 1eaf:0004 | LeafLabs   | Maple                        | 1     | cdc_acm    | B322F04446 |
| 1fc9:00a3 | NXP Sem... |                              | 1     | cdc_acm    | CD4CAE5343 |
| 2341:0042 | Arduino SA | Mega 2560 R3 (CDC ACM)       | 1     | cdc_acm    | 5D7483C11A |
| 2912:0005 | ATOL Group | ATOL USB device              | 1     | cdc_acm    | 3956F73F02 |
| 2a03:0043 | dog hunter | Arduino Uno Rev3             | 1     | cdc_acm    | 5278A91530 |

### Monitor (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 17e9:4107 | Display... | e1649Fwu 16-inch LCD monitor | 1     | udlfb, udl | 6CDBC73C38 |

### Net/wimax (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8086:0180 | Intel      | WiMAX Connection 2400m       | 42    | i2400m_usb | 130D733F55 |
| 8086:1406 | Intel      | WiMAX Connection 2400m       | 37    | i2400m_usb | A6DADFE6AF |
| 8086:0186 | Intel      | WiMAX Connection 2400m       | 21    | i2400m_usb | 195F0109A8 |
| 8087:07d6 | Intel      | Centrino WiMAX 6150          | 11    | i2400m_usb | 1FC56D39F8 |
| 8086:0188 | Intel      | WiMAX Connection 2400m       | 3     | i2400m_usb | C81404F4CA |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:8179 | Realtek... | RTL8188EUS 802.11n Wirele... | 50    | r8188eu    | 4201CDD966 |
| 148f:7601 | Ralink ... | MT7601U Wireless Adapter     | 42    | mt7601u    | 83EE2DA6F3 |
| 0cf3:9271 | Qualcom... | AR9271 802.11n               | 41    | ath9k_htc  | 6E04F26B23 |
| 0bda:8189 | Realtek... | RTL8187B Wireless 802.11g... | 28    | rtl8187    | 279CF88917 |
| 148f:5370 | Ralink ... | RT5370 Wireless Adapter      | 27    | rt2800usb  | D32B7275C3 |
| 0bda:8197 | Realtek... | RTL8187B Wireless Adapter    | 19    | rtl8187    | 6D00C70EFA |
| 148f:3070 | Ralink ... | RT2870/RT3070 Wireless Ad... | 19    | rt2800usb  | 37B056E670 |
| 0bda:8178 | Realtek... | RTL8192CU 802.11n WLAN Ad... | 15    | rtl8192cu  | A28F7B2623 |
| 0bda:8176 | Realtek... | RTL8188CUS 802.11n WLAN A... | 14    | rtl8192cu  | E814DA247A |
| 0b05:17ba | ASUSTek... | N10 Nano 802.11n Network ... | 10    | rtl8192cu  | FC0B23D4DC |
| 0bda:b812 | Realtek... | RTL8812BU USB3.0 802.11ac... | 9     | 88x2bu     | 4B70A9D252 |
| 2357:0109 | Realtek    | RTL8192EU TL-WN823N 802.1... | 9     | rtl8xxxu   | ED8AF41B6E |
| 2357:010c | Realtek    | RTL8188EUS TL-WN722N v2      | 8     | r8188eu    | EC150F49DE |
| 0b05:184c | ASUSTek... | RTL8812BU USB-AC53 Nano 8... | 7     | 88x2bu     | 3E165AC0A4 |
| 0bda:8198 | Realtek... | RTL8187B Wireless Adapter    | 7     | rtl8187    | 46C7EAAD0A |
| 0db0:6877 | Micro S... | RT2573                       | 7     | rt73usb    | DE010A6F04 |
| 148f:5572 | Ralink ... | RT5572 Wireless Adapter      | 7     | rt2800usb  | 42619E82B3 |
| 413c:81a3 | Dell       | Wireless 5570 HSPA+ (42Mb... | 7     | qcserial   | C0C75EF0DF |
| 7392:7811 | Edimax ... | EW-7811Un 802.11n Wireles... | 7     | rtl8192... | 91D9C778CB |
| 0bda:8171 | Realtek... | RTL8188SU 802.11n WLAN Ad... | 6     | r8712u     | 1579402536 |
| 0bda:a811 | Realtek... | RTL8811AU 802.11a/b/g/n/a... | 6     | rtl8812au  | C0C73D01BA |
| 0b05:1786 | ASUSTek... | USB-N10 802.11n Network A... | 5     | r8712u     | CA9FE6768C |
| 0bda:818b | Realtek... | RTL8192EU ACT-WNP-UA-005 ... | 5     | rtl8xxxu   | ED83D72FBE |
| 0bda:8812 | Realtek... | RTL8812AU 802.11a/b/g/n/a... | 5     | 8812au     | C50DF1C2F3 |
| 148f:2070 | Ralink ... | RT2070 Wireless Adapter      | 5     | rt2800usb  | BC1CB15DDC |
| 148f:3572 | Ralink ... | RT3572 Wireless Adapter      | 5     | rt2800usb  | 49697408DC |
| 148f:761a | Ralink ... | MT7610U ("Archer T2U" 2.4... | 5     |            | F940B41C82 |
| 0b05:17ab | ASUSTek... | USB-N13 802.11n Network A... | 4     | rtl8192cu  | F8B5956A65 |
| 0bda:f179 | Realtek... | 802.11n                      | 4     |            | 0201253794 |
| 148f:2573 | Ralink ... | RT2501/RT2573 Wireless Ad... | 4     | rt73usb    | CA495AA6C4 |
| 07d1:3303 | D-Link ... | DWA-131 802.11n Wireless ... | 3     | r8712u     | D625F7E867 |
| 0bda:0811 | Realtek... | RTL8811AU 802.11ac WLAN A... | 3     | 88XXau     | 92F0151B85 |
| 0bf8:100f | Fujitsu... | miniCard D2301 802.11bg W... | 3     |            | FEDA3B155D |
| 148f:2870 | Ralink ... | RT2870 Wireless Adapter      | 3     | rt2800usb  | 57811521FB |
| 148f:3072 | Ralink ... | RT3072 Wireless Adapter      | 3     | rt2800usb  | E8755C7EF8 |
| 18e8:6229 | Qcom       | RT2573                       | 3     | rt73usb    | D06F8EBDC5 |
| 2001:3c20 | D-Link     | 802.11 n WLAN                | 3     | rt2800usb  | 0137A9C687 |
| 413c:81b1 | Dell       | Wireless 5809e Gobi 4G LT... | 3     | qcserial   | 73DB320B5F |
| 8087:07d7 | Intel      | Centrino Wireless-N + WiM... | 3     | i2400m_usb | 7FC7DDDD02 |
| 050d:705c | Belkin ... | F5D7050 Wireless G Adapte... | 2     | zd1211rw   | 265B796EFD |
| 050d:845a | Belkin ... | F7D2101 802.11n Surf & Sh... | 2     | r8712u     | 1CCC5C7074 |
| 07d1:3c0d | D-Link ... | DWA-125 Wireless N 150 Ad... | 2     | rt2800usb  | 569B9B8B32 |
| 07d1:3c16 | D-Link ... | DWA-125 Wireless N 150 Ad... | 2     | rt2800usb  | C23D0EF968 |
| 0ace:1215 | ZyDAS      | ZD1211B 802.11g              | 2     | zd1211rw   | 5CA0B8480F |
| 0b05:1784 | ASUSTek... | USB-N13 802.11n Network A... | 2     | rt2800usb  | 4BE9A346F3 |
| 0bda:0179 | Realtek... | RTL8188ETV Wireless LAN 8... | 2     | r8188eu    | 8CCAAD6FF9 |
| 0bda:c811 | Realtek... | RTL8811CU 802.11ac NIC       | 2     |            | 37460463A1 |
| 0cf3:1006 | Qualcom... | TP-Link TL-WN322G v3 / TL... | 2     | ath9k_htc  | C19D82302D |
| 0e8d:7610 | MediaTek   | MT7610U WiFi                 | 2     |            | F75495E252 |
| 0e8d:7612 | MediaTek   | 802.11ac WLAN                | 2     |            | DC6C804E81 |
| 13b1:0042 | Linksys    | QCA9377 WUSB6100M 802.11a... | 2     | ath10k_usb | B295B98723 |
| 1737:0071 | Linksys    | WUSB600N v1 Dual-Band Wir... | 2     | rt2800usb  | B477CB62A6 |
| 2001:3314 | D-Link     | RTL8821AU DWA-171 802.11n... | 2     | rtl8812au  | FADC441C82 |
| 2001:3315 | D-Link     | RTL8812AU DWA-182 Wireles... | 2     | rtl8812au  | 57B913C25B |
| 2001:3319 | D-Link     | RTL8192EU DWA-131 Wireles... | 2     |            | 4FE14DEFDF |
| 2001:3c1b | D-Link     | DWA-127 Wireless N 150 Hi... | 2     | rt2800usb  | 41C9811E8B |
| 2357:0105 | MediaTek   | MT7610U Archer T1U 802.11... | 2     | mt76x0u    | F4F1C1053C |
| 2357:0115 | TP-Link    | 802.11ac NIC                 | 2     | 88x2bu,... | 060D0346D8 |
| 2717:4106 | MediaTek   | MI WLAN Adapter              | 2     | mt7601u    | 62C7769C30 |
| 2c4e:0100 | Realtek    | RTL8192EU WLAN controller    | 2     |            | 2CE91A523F |
| 413c:8194 | Dell       | Wireless 5630 (EVDO-HSPA)... | 2     | qcserial   | 95A631AAE1 |
| 03f0:251d | Hewlett... | Gobi 2000 Wireless Modem     | 1     | qmi_wwa... | 73252FB7AB |
| 0411:01a2 | BUFFALO    | WLI-UC-GNM Wireless LAN A... | 1     | rt2800usb  | 61CD8222CF |
| 0457:0162 | Silicon... | SiS162 usb Wireless LAN A... | 1     |            | D625F7E867 |
| 0471:1236 | Philips... | SNU5600 802.11bg             | 1     | zd1211rw   | 918907FA0A |
| 050d:705a | Belkin ... | F5D7050 Wireless G Adapte... | 1     | rt73usb    | BB57FF7A6A |
| 050d:815f | Belkin ... | F5D8053 N Wireless USB Ad... | 1     | r8712u     | F5259AEDE4 |
| 050d:935b | Belkin ... | F6D4050 N150 Enhanced Wir... | 1     | rt2800usb  | 16C1000038 |
| 0586:3401 | ZyXEL C... | ZyAIR G-220 802.11bg         | 1     | zd1211rw   | B309035D5A |
| 0586:340f | ZyXEL C... | G-220 v2 802.11bg            | 1     | zd1211rw   | 303A242CF5 |
| 0586:341e | ZyXEL C... | NWD2105 802.11bgn Wireles... | 1     | rt2800usb  | AEC48E6B54 |
| 06f8:e031 | Guillemot  | Hercules HWNUm-300 Wirele... | 1     | r8712u     | AD3BB711D8 |
| 079b:0062 | Sagem      | XG-76NA 802.11bg             | 1     | zd1211rw   | D57B5C86E0 |
| 07b8:8179 | AboCom ... | 802.11n NIC                  | 1     | r8188eu    | 908E49B930 |
| 07d1:3a10 | D-Link ... | DWA-126 802.11n Wireless ... | 1     | ath9k_htc  | F3B5D07EE4 |
| 07d1:3c03 | D-Link ... | AirPlus G DWL-G122 Wirele... | 1     | rt73usb    | AF6C0819E5 |
| 07d1:3c07 | D-Link ... | DWA-110 Wireless G Adapte... | 1     | rt73usb    | 62697BF35B |
| 07d1:3c09 | D-Link ... | DWA-140 RangeBooster N Ad... | 1     | rt2800usb  | 3C2E5705F9 |
| 0846:9001 | NetGear    | WN111(v2) RangeMax Next W... | 1     | carl9170   | 3B64B265CF |
| 0846:9011 | NetGear    | BCM4323 WNDA3100v2 802.11... | 1     |            | 56C1A2EF9B |
| 0846:9021 | NetGear    | WNA3100M(v1) Wireless-N 3... | 1     | rtl8192... | 5EEDC9CEE8 |
| 0846:9030 | NetGear    | WNA1100 Wireless-N 150 [A... | 1     | ath9k_htc  | 1F81EFD5A8 |
| 0b05:1723 | ASUSTek... | WL-167G v2 802.11g Adapte... | 1     | rt73usb    | CB29EAF3FB |
| 0b05:17d1 | ASUSTek... | MT7610U AC51 802.11a/b/g/... | 1     |            | 2FB8A70944 |
| 0b05:17e8 | ASUSTek... | USB-N14 802.11b/g/n (2x2)... | 1     | rt2800usb  | 53302E45B6 |
| 0bda:8172 | Realtek... | RTL8191SU 802.11n WLAN Ad... | 1     | r8712u     | DB2585CD68 |
| 0bda:817f | Realtek... | RTL8188RU 802.11n WLAN Ad... | 1     | rtl8192cu  | B9EC60D456 |
| 0bda:8813 | Realtek... | RTL8814AU 802.11a/b/g/n/a... | 1     |            | DF9651B2B8 |
| 0cf3:7015 | Qualcom... | TP-Link TL-WN821N v3 / TL... | 1     | ath9k_htc  | 9679B46FA0 |
| 0df6:005b | Sitecom... | RTL8188S WLAN Adapter        | 1     | r8712u     | EF7BE10B74 |
| 1385:5f00 | Netgear    | WPN111 RangeMax Wireless ... | 1     | ar5523     | 46225E5036 |
| 13b1:003f | Linksys    | RTL8812AU WUSB6300 802.11... | 1     |            | A0B41E7AB4 |
| 148f:2770 | Ralink ... | RT2770 Wireless Adapter      | 1     | rt2800usb  | C6AC0C6272 |
| 148f:5372 | Ralink ... | RT5372 Wireless Adapter      | 1     | rt2800usb  | 49A28F87B9 |
| 1737:0079 | Linksys    | WUSB600N v2 Dual-Band Wir... | 1     | rt2800usb  | BCC6FBD012 |
| 1761:0b05 | ASUSTek... | 802.11n Network Adapter (... | 1     | rt2800usb  | 7DEC6AEA64 |
| 2001:3308 | D-Link     | DWA-121 802.11n Wireless ... | 1     | rtl8192... | A4739818F4 |
| 2001:3317 | D-Link     | 802.11 n WLAN                | 1     | rt2800usb  | A75132701F |
| 2001:3a02 | D-Link     | DWL-G132 [Atheros AR5523]    | 1     | ar5523     | B6BA04DCE2 |
| 2001:3c19 | D-Link     | DWA-125 Wireless N 150 Ad... | 1     | rt2800usb  | AFB7E9C67E |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:8153 | Realtek... | RTL8153 Gigabit Ethernet ... | 60    | r8152      | 5C4E5B6717 |
| 12d1:14db | Huawei ... | E353/E3131 34GB              | 39    | cdc_ether  | FBD67C80F7 |
| 19d2:1405 | ZTE WCD... | ZTE Mobile Broadband Station | 37    | cdc_ether  | 5D7366E2EF |
| 04e8:6863 | Samsung... | GT-I9500 [Galaxy S4] / GT... | 28    | rndis_host | 3751892E56 |
| 0bda:8152 | Realtek... | RTL8152 Fast Ethernet Ada... | 25    | r8152      | 1579402536 |
| 15a9:002d | Gemtek     | WLTUBA-107 [Yota 4G LTE]     | 18    | cdc_ether  | 3630B640F0 |
| 04e8:6864 | Samsung... | GT-I9070 (network tetheri... | 17    | rndis_host | B7561ADE6F |
| 0bda:b720 | Realtek... | RTL8723BU 802.11n WLAN Ad... | 17    | rtl8xxxu   | 1579402536 |
| 2717:ff80 | Android    | SDM636-MTP _SN:5F93851E      | 15    | rndis_host | 4A6A073320 |
| 0e8d:2004 | MediaTek   | Power Ice Evo                | 14    | rndis_host | 86D04818BE |
| 12d1:108a | Huawei ... | DLI-TL20                     | 14    | rndis_host | 0AB5BF5B1A |
| 0bb4:0003 | HTC (Hi... | Android Incorporated GSM ... | 13    | rndis_host | E6FF05AC01 |
| 8087:0911 | Intel      | PRODUCT_MODEM                | 12    | cdc_mbim   | 8F9E198F6E |
| 0b95:1790 | ASIX El... | AX88179 Gigabit Ethernet     | 11    | ax88179... | EB40372085 |
| 0bda:1724 | Realtek... | RTL8723AU 802.11n WLAN Ad... | 11    | rtl8723au  | B19B3500F2 |
| 216f:0043 | Altair ... | Modem YOTA 4G LTE            | 11    | cdc_ether  | 6763D9FF56 |
| 0bda:8187 | Realtek... | RTL8187 Wireless LAN Adapter | 10    | rtl8187    | A8B0FBE3A8 |
| 03f0:581d | Hewlett... | lt4112 Gobi 4G Module Net... | 9     | qcserial   | AC96DD45F9 |
| 0b95:772b | ASIX El... | AX88772B                     | 9     | asix       | 2D4AA64C10 |
| 12d1:15bb | Huawei ... | ME936 LTE/HSDPA+ 4G modem    | 9     | cdc_ncm... | AE9CAC19CA |
| 05c6:f00e | Qualcomm   | DEXP Ixion X LTE 4.5"        | 8     | rndis_host | 42BA5CAA2F |
| 03f0:9d1d | Hewlett... | lt4120 Snapdragon X5 LTE     | 7     | qmi_wwan   | B8392B7335 |
| 1076:8002 | GCT Sem... | LU150 LTE Modem [Yota LU150] | 7     | rndis_host | F72210300B |
| 1199:9041 | Sierra ... | EM7305                       | 7     | cdc_mbim   | 734FCAFBD7 |
| 22b8:2e24 | Motorol... | Moto G (4)                   | 7     | rndis_host | 4CA32F1015 |
| 413c:81b6 | Dell       | DW5811e Snapdragon X7 LTE    | 7     | qcserial   | B9281326D7 |
| 0b95:7e2b | ASIX El... | AX88772B Fast Ethernet Co... | 6     | asix       | 377E8B594A |
| 1199:9011 | Sierra ... | MC8305                       | 6     | qcserial   | 8F2E060D39 |
| 1199:a001 | Sierra ... | EM7345 4G LTE                | 6     | cdc_mbim   | 97DC44DC84 |
| 12d1:14f1 | Huawei ... | Gobi 3000 HSPA+ Modem        | 5     | qcseria... | 01A4BD5E7F |
| 17ef:3069 | Lenovo     | ThinkPad TBT3 LAN            | 5     | r8152      | C15E0482CA |
| 2001:330f | D-Link     | RTL8188ETV DWA-125 11n Ad... | 5     | r8188eu    | 4A46D49BB1 |
| 03f0:371d | Hewlett... | un2430 Mobile Broadband M... | 4     | qcserial   | D16CE79DB7 |
| 0b95:772a | ASIX El... | AX88772A Fast Ethernet       | 4     | asix       | 8C487BE380 |
| 1199:9079 | Sierra ... | EM7455 Qualcomm Snapdrago... | 4     | qcseria... | 1038A34C39 |
| 15a9:003e | Gemtek     | Yota Many                    | 4     | rndis_host | 0341BE76C2 |
| 17ef:7436 | Lenovo     | MT65xx Android Phone         | 4     | rndis_host | 586A6A9F8A |
| 2357:0601 | TP-Link    | RTL8153 TP-Link UE300 USB... | 4     | r8152      | C5279AB6E4 |
| 04b4:3610 | Cypress... | USB-C PD Docking             | 3     | ax88179... | BF4A9DEE07 |
| 05ac:1402 | Apple      | Ethernet Adapter [A1277]     | 3     | asix       | 27D23CBDAA |
| 05c6:9205 | Qualcomm   | Gobi 2000                    | 3     | qmi_wwa... | 6F637899C4 |
| 0846:9053 | NetGear    | A6210                        | 3     | mt76x2u    | F87D092FC1 |
| 0bb4:0004 | HTC (Hi... | MT65xx Android Phone         | 3     | rndis_host | 12AEA08876 |
| 0bb4:0ffe | HTC (Hi... | Desire HD (modem mode)       | 3     | rndis_w... | 7B011531E4 |
| 0fe6:9700 | Kontron... | DM9601 Fast Ethernet Adapter | 3     | dm9601     | 8B1D65721A |
| 1199:68a2 | Sierra ... | MC7710                       | 3     | qcseria... | 36D07088D1 |
| 1376:4e61 | Vimtron... | Mobile Composite Device Bus  | 3     | rndis_host | 61A1DE10C0 |
| 15a9:002b | Gemtek     | Yota Many                    | 3     | rndis_host | 94A0B982A5 |
| 17e9:4307 | Display... | USB3.0 Dual Video Dock       | 3     | cdc_ncm... | 3EF3EE8B41 |
| 17e9:436e | Display... | Dell USB3.0 Dock             | 3     | usbfs      | 0FCCED0386 |
| 1bbb:2026 | T & A M... | ALCATEL ONETOUCH PIXI 3 (... | 3     | cdc_eem    | 0E00B75FEA |
| 2001:3c15 | D-Link     | DWA-140 RangeBooster N Ad... | 3     | rt2800usb  | 253874A181 |
| 2e04:c022 | MediaTek   | Android                      | 3     | rndis_host | 0F7C3DFCD0 |
| 0424:7500 | Standar... | LAN7500 Ethernet 10/100/1... | 2     | smsc75xx   | 63B03B7E15 |
| 056a:0084 | Wacom      | Wireless adapter for Bamb... | 2     | usbhid     | D6D7F9CC30 |
| 0846:6a00 | NetGear    | WG111v2 54 Mbps Wireless ... | 2     | rtl8187    | 709C282E30 |
| 0b05:5f04 | ASUSTek... | Android                      | 2     | rndis_host | 3ADE9B2F82 |
| 0e8d:2005 | MediaTek   | X5max_PRO                    | 2     | rndis_host | 5825DE04E9 |
| 0fca:8017 | Researc... | BlackBerry                   | 2     | cdc_ncm... | C33C8BA4E4 |
| 0fca:8035 | Researc... | BlackBerry                   | 2     | cdc_ncm... | C7ECD4A0AE |
| 12d1:1050 | Huawei ... | Android Adapter              | 2     | rndis_host | B2185404AA |
| 12d1:260d | Huawei ... | TIT-L01                      | 2     | rndis_host | 1DEA266689 |
| 17ef:304b | Lenovo     | AX88179 Gigabit Ethernet ... | 2     | ax88179... | EE8564DC35 |
| 19d2:1052 | ZTE WCD... | ZTE Technologies MSM         | 2     | cdc_ether  | 0D741D736E |
| 19d2:1365 | ZTE WCD... | MT65xx Android Phone         | 2     | rndis_host | AC739516CE |
| 2001:1a02 | D-Link     | DUB-E100 Fast Ethernet Ad... | 2     | asix       | D750A74B23 |
| 2001:4a00 | D-Link     | DUB-1312                     | 2     | ax88179... | 9E5C841959 |
| 2970:2004 | MediaTek   | FS504                        | 2     | rndis_host | F0F1775D83 |
| 2cb7:0210 | FIBOCOM    | L830-EB-00                   | 2     | cdc_acm    | 14AFD9EA12 |
| 0421:0705 | Nokia M... | Nokia_X2 (RM-1013)           | 1     | rndis_host | B2690984E6 |
| 0502:15b1 | Acer       | PDA n311                     | 1     | rndis_w... | F7D9186BB2 |
| 0502:3687 | Acer       | Z200                         | 1     | rndis_host | F82CBB8F52 |
| 05ac:12ab | Apple      | iPad 4/Mini1                 | 1     | ipheth     | D75EFC8713 |
| 05c6:9024 | Qualcomm   | Android                      | 1     | rndis_host | 1A6BB14B80 |
| 067b:25a1 | Prolifi... | PL25A1 Host-Host Bridge      | 1     | plusb      | 4662D43A44 |
| 07a6:8513 | ADMtek     | AN8513 Ethernet              | 1     | pegasus    | 67156F85AE |
| 07d1:3c0a | D-Link ... | DWA-140 RangeBooster N Ad... | 1     | rt2800usb  | DE10405623 |
| 09c1:1337 | Arris I... | TOUCHSTONE DEVICE            | 1     | rndis_w... | B94A660042 |
| 0a5c:6300 | Broadcom   | Pirelli Remote NDIS Device   | 1     | cdc_ether  | 3673023593 |
| 0b05:2004 | ASUSTek... | ASUS_Z00YD                   | 1     | rndis_host | A0EB5DA51A |
| 0b05:620a | ASUSTek... | Remote NDIS Device           | 1     | cdc_ether  | A110151EEA |
| 0b05:7783 | ASUSTek... | Android                      | 1     | rndis_host | 97FFB9471C |
| 0b95:7720 | ASIX El... | AX88772                      | 1     | asix       | 27D23CBDAA |
| 0bb4:0ee7 | HTC (Hi... | Desire 620G dual sim         | 1     | rndis_host | F221A91CA5 |
| 0bb4:0fb5 | HTC (Hi... | Android Phone                | 1     | rndis_w... | E8FD73E224 |
| 0bda:0823 | Realtek... | 802.11ac WLAN Adapter        | 1     | btusb      | 8E2C99692B |
| 0cf3:0001 | Qualcom... | AR5523                       | 1     | ar5523     | A79789524B |
| 0fce:712e | Sony Er... | SEMC HSUSB Device            | 1     | rndis_host | 74904D703D |
| 0fce:7197 | Sony Er... | C5503                        | 1     | rndis_host | 2033ADD4C1 |
| 0fce:719b | Sony Er... | Android                      | 1     | rndis_host | C60B1F0E31 |
| 0fce:71af | Sony Er... | D6503                        | 1     | rndis_host | FDC4AD9FAD |
| 0fce:71b1 | Sony Er... | SGP521                       | 1     | rndis_host | 591EF7D849 |
| 0fce:71bc | Sony Er... | D2203                        | 1     | rndis_host | 4038731BC3 |
| 0fce:71eb | Sony Er... | G3112                        | 1     | rndis_host | E21BE77DBC |
| 0fce:81af | Sony Er... | D6503                        | 1     | rndis_host | FB46583581 |
| 1004:61da | LG Elec... | G2 Android Phone [tetheri... | 1     | rndis_host | 7077729E39 |
| 1004:6344 | LG Elec... | G2 Android Phone [tetheri... | 1     | rndis_h... | 60ADC60B28 |
| 1199:9063 | Sierra ... | EM7305                       | 1     | qcseria... | 80EAE9ED5F |
| 1271:052a | Android    | Android                      | 1     | rndis_host | 64CE9B2168 |
| 15a9:0030 | Gemtek     | Wi-Fi Modem Yota 4G LTE      | 1     | rndis_host | 72A9100732 |

### Phone (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 18d1:4ee1 | Google     | Nexus Device (MTP)           | 6     | usbfs      | 6E694F87C3 |
| 1004:631c | LG Elec... | G2/Optimus Android Phone ... | 5     | usbfs      | F271B8993E |
| 1bbb:0168 | T & A M... | ALCATEL ONETOUCH PIXI 3 (... | 4     | usbfs      | 3A50E0F27D |
| 2717:ff48 | MediaTek   | MT65xx Android Phone         | 3     |            | 1069F23A4C |
| 0bb4:0c02 | HTC (Hi... | Dream / ADP1 / G1 / Magic... | 2     | uas, us... | 73EAA82EFF |
| 0bb4:0f0b | HTC (Hi... | Android Phone                | 2     | usbhid     | 25B5BF978A |
| 0bb4:2008 | HTC (Hi... | Android Phone via MTP [Wi... | 2     |            | F513215EC6 |
| 0fce:019b | Sony Er... | Android Phone                | 2     |            | 49389100FC |
| 1004:6300 | LG Elec... | G2/Optimus Android Phone ... | 2     | usbhid     | BAF6A2731A |
| 17ef:7497 | Lenovo     | A789 (MTP mode)              | 2     |            | B60CD6FFC3 |
| 2717:1240 | Xiaomi     | HM1 Android Phone            | 2     |            | 01AA74496F |
| 2916:f003 | Yota De... | YotaPhone C9660              | 2     |            | E591D8BDD0 |
| 045e:04ec | Microsoft  | Windows Phone (Zune)         | 1     |            | A8E7938691 |
| 0471:2008 | Philips... | Android Phone                | 1     |            | 2DBB267463 |
| 04e8:685d | Samsung... | GT-I9100 Phone [Galaxy S ... | 1     | cdc_acm    | 3F7F72D7A2 |
| 05c6:9092 | Qualcomm   | DEXP Ixion X LTE 4.5"        | 1     |            | 3C92D03C69 |
| 0bb4:0df8 | HTC (Hi... | Android Phone                | 1     |            | F4F1952C9C |
| 0bb4:f006 | HTC (Hi... | Android Phone                | 1     | usbhid     | 39061A0026 |
| 0fce:0189 | Sony Er... | Xperia ZL C6503              | 1     |            | 97D00825B7 |
| 0fce:0197 | Sony Er... | Xperia ZR C5502              | 1     |            | 9C65E049FE |
| 0fce:01c4 | Sony Er... | Xperia M4                    | 1     |            | FD936C6C93 |
| 0fce:51ad | Sony Er... | Android Phone                | 1     |            | E6AB329109 |
| 1004:61fc | LG Elec... | Optimus 3                    | 1     | cdc_acm... | 1EBB6C2E61 |
| 17ef:74f8 | Lenovo     | MT65xx Android Phone         | 1     |            | B89D04A41B |
| 19d2:0306 | ZTE WCD... | MT65xx Android Phone         | 1     |            | 6AD005D6B7 |
| 19d2:0343 | ZTE WCD... | V985 Android Phone           | 1     |            | 4215162CA5 |
| 2a45:2008 | Meizu      | MX Phone (MTP)               | 1     |            | 88C466E27F |

### Printer (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:2b17 | Hewlett... | LaserJet 1020                | 12    | usblp      | CA2CE3447F |
| 03f0:002a | Hewlett... | LaserJet P1102               | 9     | usblp      | 5D8546A3D9 |
| 03f0:4117 | Hewlett... | LaserJet 1018                | 9     | usblp      | E5658355FA |
| 04e8:341b | Samsung... | SCX-4200 series              | 7     | usblp      | 93852EC758 |
| 04da:0f0b | Panason... | KX-MB1520RU                  | 6     | usblp      | E8FBCD39AA |
| 04f9:0054 | Brother... | HL-1110 series               | 6     | usblp      | A83B92D6B0 |
| 04e8:3321 | Samsung... | M2020 Series                 | 5     | usblp      | 3751892E56 |
| 03f0:032a | Hewlett... | LaserJet Professional P1102w | 4     | usblp      | A662253C3C |
| 04b8:08a1 | Seiko E... | L210 Series                  | 4     | usblp      | 5202AAE85E |
| 04e8:3469 | Samsung... | M2070 Series                 | 4     | usblp      | 23DAC0F1B6 |
| 03f0:0317 | Hewlett... | LaserJet 1200                | 3     | usblp      | 6F823AF237 |
| 03f0:0c17 | Hewlett... | LaserJet 1010                | 3     | usblp      | B334FD2F51 |
| 03f0:7611 | Hewlett... | DeskJet F2492 All-in-One     | 3     | usblp      | 98257DE332 |
| 03f0:8711 | Hewlett... | Deskjet 2050 J510            | 3     | usblp      | 8F377B38F2 |
| 03f0:8911 | Hewlett... | Deskjet 1050 J410 series     | 3     | usblp      | 542C9CBC52 |
| 04a9:10dc | Canon      | iP7200 series                | 3     | usblp      | AD610739B6 |
| 04a9:176d | Canon      | PIXMA MG2500 Series          | 3     | usblp      | 7EECEB0DB3 |
| 04a9:2676 | Canon      | CAPT Device                  | 3     | usblp      | 594560455F |
| 04a9:271a | Canon      | CAPT USB Device              | 3     | usblp      | 7DEA09C3AC |
| 04a9:2737 | Canon      | MF4410                       | 3     | usblp      | 929306E8A0 |
| 04a9:2759 | Canon      | MF3010                       | 3     | usblp      | FE9A690216 |
| 04a9:2771 | Canon      | CAPT USB Device              | 3     | usblp      | F12E05AEE4 |
| 04e8:3441 | Samsung... | SCX-3200 Series              | 3     | usblp      | 191BA97155 |
| 04e8:344f | Samsung... | SCX-3400 Series              | 3     | usblp      | 3D5822E22A |
| 04f9:0027 | Brother... | HL-2030 Laser Printer        | 3     | usblp      | 6420DFC899 |
| 0924:3cf7 | Xerox      | WorkCentre 3315              | 3     | usblp      | 839442574A |
| 0924:3cf9 | Xerox      | Phaser 3040                  | 3     | usblp      | 08608CC899 |
| 03f0:3d17 | Hewlett... | LaserJet P1005               | 2     | usblp      | 4B5FA82DB4 |
| 03f0:5d17 | Hewlett... | LaserJet P2035n              | 2     | usblp      | FB609ABE2E |
| 03f0:6004 | Hewlett... | DeskJet 5550                 | 2     | usblp      | 21317BB405 |
| 03f0:7e04 | Hewlett... | DeskJet F4100 Printer series | 2     | usblp      | 864C281AE7 |
| 03f0:9311 | Hewlett... | Deskjet 3050 J610 series     | 2     | usbfs      | 852F7E08D5 |
| 03f0:d811 | Hewlett... | DeskJet 4530 series          | 2     | usblp      | BD423E808F |
| 04a9:10d3 | Canon      | iP2700 series                | 2     | usblp      | 882E80F014 |
| 04a9:173a | Canon      | PIXMA MP250 series printer   | 2     | usblp      | A801EDF088 |
| 04a9:1746 | Canon      | PIXMA MP280 series           | 2     | usblp      | F1235A8C4C |
| 04a9:176c | Canon      | MG2400 series                | 2     | usblp      | C05628D84A |
| 04a9:1780 | Canon      | PIXMA MG2900 Series          | 2     | usblp      | 949EC692A8 |
| 04a9:1796 | Canon      | G1000 series                 | 2     | usblp      | 699BFFB694 |
| 04a9:2684 | Canon      | MF3200 series                | 2     | usblp      | 396BD73341 |
| 04a9:26b4 | Canon      | MF4010 series                | 2     | usblp      | B98B8362E0 |
| 04a9:26da | Canon      | LBP3010B printer             | 2     | usblp      | 3DCD149CA9 |
| 04a9:271c | Canon      | CAPT USB Device              | 2     | usblp      | 9E5B615341 |
| 04b8:0005 | Seiko E... | Printer                      | 2     | usblp      | BC6957326F |
| 04b8:002a | Seiko E... | USB2.0 Printer (Hi-speed)    | 2     | usblp      | FE2AB22987 |
| 04b8:004c | Seiko E... | L110 Series                  | 2     | usblp      | 366AFD6B06 |
| 04b8:0873 | Seiko E... | L200 Series                  | 2     | usblp      | 24338FF1DE |
| 04b8:08d2 | Seiko E... | L366 Series                  | 2     | usblp      | 17FD2B6444 |
| 04e8:300c | Samsung... | ML-1210 Printer              | 2     | usblp      | 46D4B91CF0 |
| 04e8:300e | Samsung... | Laser Printer                | 2     | usblp      | BF7D967CAC |
| 04e8:3292 | Samsung... | ML-1640 Series Laser Printer | 2     | usblp      | 87E87C1894 |
| 04e8:330c | Samsung... | ML-1865                      | 2     | usblp      | D4EC4EAE87 |
| 04e8:342e | Samsung... | SCX-4300 Series              | 2     | usblp      | A451EDB119 |
| 04e8:3434 | Samsung... | SCX-4623 Series              | 2     | usblp      | 7DB5CAC433 |
| 04f9:0248 | Brother... | DCP-7055 scanner/printer     | 2     | usblp      | 7D38422BE6 |
| 04f9:02d0 | Brother... | DCP-1510                     | 2     | usblp      | F3C2B4FEC0 |
| 067b:2305 | Prolifi... | PL2305 Parallel Port         | 2     | usblp      | 93106FC9FA |
| 0924:3d6c | Xerox      | Phaser 6000B                 | 2     | usblp      | 64CB2969A3 |
| 232b:0e20 | Pantum     | M6500 series                 | 2     | usblp      | E4E9ADBCE5 |
| 03f0:0517 | Hewlett... | LaserJet 1000                | 1     | usblp      | 4F8DAEA0E4 |
| 03f0:0a2a | Hewlett... | LaserJet Professional P16... | 1     | usblp      | 26323296FC |
| 03f0:0b2a | Hewlett... | LaserJet CP1025nw            | 1     | usblp      | A2A356B7E7 |
| 03f0:0f2a | Hewlett... | LaserJet 400 color M451dn    | 1     | usblp      | 82D7130F18 |
| 03f0:1214 | Hewlett... | Designjet T520 24in          | 1     | usblp      | 7DB50CAB7D |
| 03f0:152a | Hewlett... | LaserJet 400 M401dw          | 1     | usblp      | 12FAAC55A6 |
| 03f0:1c2a | Hewlett... | Color LaserJet M855          | 1     | usblp      | BA3BC5C887 |
| 03f0:1f04 | Hewlett... | Deskjet D4300 series         | 1     | usblp      | 2AB0B7C87D |
| 03f0:2c2a | Hewlett... | LaserJet Pro M201dw          | 1     | usblp      | 9D87C5C350 |
| 03f0:372a | Hewlett... | Color LaserJet M750          | 1     | usblp      | AF6B592B04 |
| 03f0:3817 | Hewlett... | LaserJet P2015 series        | 1     | usblp      | 93F413D666 |
| 03f0:3c2a | Hewlett... | Color LaserJet Pro M252dw    | 1     | usblp      | E266C1D86A |
| 03f0:3e17 | Hewlett... | LaserJet P1006               | 1     | usblp      | 76C7304A6D |
| 03f0:4712 | Hewlett... | Officejet 4500 G510a-f       | 1     | usblp      | A74C3BAF50 |
| 03f0:4b11 | Hewlett... | OfficeJet 6200               | 1     | usblp      | 4D7DF702B8 |
| 03f0:4c11 | Hewlett... | PSC 1500 series              | 1     | usblp      | 09863C9A1F |
| 03f0:4f11 | Hewlett... | OfficeJet 5600 (USBHUB)      | 1     | usblp      | C081ADAAE3 |
| 03f0:5417 | Hewlett... | Color LaserJet CP2025dn      | 1     | usblp      | D21BC7358D |
| 03f0:5c17 | Hewlett... | LaserJet P2055 series        | 1     | usblp      | 11B5A7A444 |
| 03f0:6412 | Hewlett... | Officejet 4620 series        | 1     | usblp      | F590C9BDB7 |
| 03f0:7304 | Hewlett... | DeskJet 35xx                 | 1     | usblp      | D766B00367 |
| 03f0:7317 | Hewlett... | LaserJet P3005               | 1     | usblp      | 836238ED9F |
| 03f0:7404 | Hewlett... | Printing Support             | 1     | usblp      | 8DEF17FA59 |
| 03f0:7804 | Hewlett... | DeskJet D1360                | 1     | usblp      | 241E62A5E2 |
| 03f0:8604 | Hewlett... | DeskJet 5440                 | 1     | usblp      | 11C4A1EA1A |
| 03f0:8904 | Hewlett... | DeskJet 6940 series          | 1     | usblp      | A1B43FF61F |
| 03f0:8c11 | Hewlett... | Deskjet F4500 series         | 1     | usblp      | DC63DC0B06 |
| 03f0:a617 | Hewlett... | LaserJet 700 M712            | 1     | usblp      | 93D0854F53 |
| 03f0:ac11 | Hewlett... | Deskjet 2510 series          | 1     | usblp      | DA28D4A9D0 |
| 03f0:ad11 | Hewlett... | Deskjet 3510 series          | 1     | usblp      | 1CD14256CC |
| 03f0:b011 | Hewlett... | Deskjet 3520 series          | 1     | usblp      | ED83569D3C |
| 03f0:c302 | Hewlett... | DeskJet D2300                | 1     | usblp      | 8E8D82A3F0 |
| 03f0:c711 | Hewlett... | Deskjet 3540 series          | 1     | usblp      | 5C0BCB0583 |
| 03f0:d711 | Hewlett... | ENVY 4520 series             | 1     | usblp      | 31AFE6635F |
| 03f0:df11 | Hewlett... | DeskJet 1110 series          | 1     | usblp      | 91F3874CDB |
| 043d:0226 | Lexmark... | Lexmark MS317dn              | 1     | usblp      | 50AEEEC380 |
| 0482:0496 | Kyocera    | FS-1120MFP                   | 1     | usbfs      | 353AF0786C |
| 04a9:10cd | Canon      | iP1900 series                | 1     | usblp      | F85AA7527B |
| 04a9:10e4 | Canon      | iP2800 series                | 1     | usblp      | D6F2C36514 |
| 04a9:1714 | Canon      | MP160                        | 1     | usblp      | B0F58259FC |
| 04a9:1747 | Canon      | PIXMA MP495 series           | 1     | usblp      | E96BC61345 |

### Scanner (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04a9:220d | Canon      | CanoScan N670U/N676U/LiDE 20 | 6     |            | 12F0D701D6 |
| 04a9:1909 | Canon      | CanoScan LiDE 110            | 5     |            | 47C94E2F61 |
| 03f0:0a01 | Hewlett... | ScanJet 2400c                | 4     |            | 192AE6829E |
| 04a9:2220 | Canon      | CanoScan LIDE 25             | 4     | usbfs      | A83B92D6B0 |
| 03f0:1c05 | Hewlett... | Scanjet 200                  | 3     |            | AE0972B81D |
| 04b8:0142 | Seiko E... | GT-F730 [GT-S630/Perfecti... | 3     |            | F66EB39C74 |
| 055f:021c | Mustek ... | BearPaw 1200 CU Plus         | 3     | usbfs      | A3F8EC2423 |
| 04a5:2137 | Acer Pe... | Benq 5150/5250               | 2     |            | 2D25B17958 |
| 04a9:190a | Canon      | CanoScan LiDE 210            | 2     |            | AD610739B6 |
| 04a9:190e | Canon      | CanoScan LiDE 120            | 2     |            | A94B76ED20 |
| 04a9:190f | Canon      | CanoScan LiDE 220            | 2     | usbfs      | 0715C80BE6 |
| 04b8:0121 | Seiko E... | GT-F500/GT-F550 [Perfecti... | 2     |            | 3AFA544363 |
| 04b8:0122 | Seiko E... | GT-F520/GT-F570 [Perfecti... | 2     |            | B91B3063A8 |
| 04b8:012e | Seiko E... | GT-F670 [Perfection V200 ... | 2     |            | CA5588BC3D |
| 05d8:4002 | Ultima ... | Artec Ultima 2000 (GT6801... | 2     |            | 6E34B13E7F |
| 03f0:0405 | Hewlett... | ScanJet 3400cse              | 1     |            | 856D5A57FF |
| 03f0:2005 | Hewlett... | ScanJet 3570c                | 1     |            | 9C696B9A08 |
| 03f0:2205 | Hewlett... | ScanJet 3500c                | 1     |            | B855712EEF |
| 03f0:2505 | Hewlett... | ScanJet 3770                 | 1     |            | 18035E7160 |
| 0458:2014 | KYE Sys... | ColorPage-Vivid4             | 1     |            | DD360E1D5E |
| 0458:201f | KYE Sys... | ColorPage-Vivid 1200 XE      | 1     |            | C676410AA7 |
| 04a5:20b0 | Acer Pe... | S2W 3300U/4300U              | 1     |            | 1A3A0269DD |
| 04a7:0477 | Visioneer  | DocuMate 152                 | 1     |            | DE18CDDB21 |
| 04a9:1904 | Canon      | CanoScan LiDE 100            | 1     |            | C80CA1930B |
| 04a9:1906 | Canon      | CanoScan 5600F               | 1     |            | D14CE30BB6 |
| 04a9:1907 | Canon      | CanoScan LiDE 700F           | 1     |            | 06C93EC404 |
| 04a9:1908 | Canon      | CanoScan                     | 1     |            | 1A2050B00F |
| 04a9:2224 | Canon      | CanoScan LiDE 600F           | 1     |            | 1FB7EFF26A |
| 04a9:2225 | Canon      | CanoScan LiDE 70             | 1     |            | 049109A77F |
| 04a9:2228 | Canon      | CanoScan 4400F               | 1     |            | FE2AB22987 |
| 04b8:011c | Seiko E... | GT-9800F [Perfection 3200]   | 1     |            | 86869022FA |
| 04b8:011f | Seiko E... | GT-8400UF [Perfection 167... | 1     |            | EABDFA2312 |
| 04b8:0120 | Seiko E... | GT-7400U [Perfection 1270]   | 1     |            | 21F455B486 |
| 04b8:0126 | Seiko E... | ES-7000H [GT-15000]          | 1     |            | F6CC7FBA3A |
| 04b8:012a | Seiko E... | GT-X800 [Perfection 4990 ... | 1     |            | EF74EB0B9B |
| 04b8:012d | Seiko E... | GT-F650 [GT-S600/Perfecti... | 1     |            | 3A0F823175 |
| 04b8:0802 | Seiko E... | CC-570L [Stylus CX3100/CX... | 1     | usblp      | 44AA7D667E |
| 04c5:1041 | Fujitsu    | fi-4120c Scanner             | 1     |            | 7593BA7D90 |
| 055f:0219 | Mustek ... | BearPaw 2400 TA Plus         | 1     |            | 49BA2B0298 |
| 055f:021b | Mustek ... | BearPaw 1200 CU Plus         | 1     |            | A1913AAB0F |
| 055f:021d | Mustek ... | BearPaw 2400 CU Plus         | 1     |            | 3572002C76 |
| 055f:021e | Mustek ... | BearPaw 1200 TA/CS           | 1     |            | A6D13752DA |
| 055f:021f | Mustek ... | SNAPSCAN e22                 | 1     |            | D39275B174 |
| 055f:0409 | Mustek ... | BearPaw 2448 TA Pro          | 1     |            | 8913A41223 |
| 0681:0005 | Siemens... | ID-Mouse with Fingerprint... | 1     | idmouse    | F6FA4AEB51 |
| 23de:f808 | Papillo... | Scanner DS45USB              | 1     | ds45_usb   | 0A99E4D896 |

### Serial controller (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 05c6:9204 | Qualcomm   | Qualcomm Gobi 2000           | 19    | qcserial   | 14BF9C4413 |
| 067b:2303 | Prolifi... | PL2303 Serial Port           | 19    | pl2303     | DC2CE35421 |
| 1a86:7523 | QinHeng... | HL-340 USB-Serial adapter    | 18    | ch341      | 517CAD6069 |
| 03f0:241d | Hewlett... | Gobi 2000 Wireless Modem ... | 16    | qcserial   | AB17752168 |
| 0403:6001 | Future ... | FT232 USB-Serial (UART) IC   | 16    | ftdi_sio   | D3AFE1DB4A |
| 1199:9013 | Sierra ... | Sierra Wireless Gobi 3000... | 10    | qcserial   | 6D5F1234C2 |
| 10c4:ea60 | Cygnal ... | CP2102/CP2109 UART Bridge... | 8     | cp210x     | AAB68A3B33 |
| 03f0:521d | Hewlett... | hs3110 HSPA+ Mobile Broad... | 6     | cdc_mbim   | BD22949D99 |
| 19d2:2003 | ZTE WCD... | ZTE WCDMA Technologies MSM   | 6     | option     | 1476158702 |
| 05c6:9224 | Qualcomm   | Sony Gobi 2000 Wireless M... | 5     | qcserial   | 94C60C4371 |
| 1199:9000 | Sierra ... | Gobi 2000 Wireless Modem ... | 5     | qcserial   | A6CA528D43 |
| 413c:8185 | Dell       | Gobi 2000 Wireless Modem ... | 5     | qcserial   | C81404F4CA |
| 0af0:6901 | Option     | Globetrotter HSDPA Modem     | 4     | option     | 46B096153A |
| 19d2:0016 | ZTE WCD... | ZTE WCDMA Technologies MSM   | 4     | option     | F4021DEE94 |
| 1199:683c | Sierra ... | Mobile Broadband 3G/UMTS ... | 3     | sierra     | BA912D76A5 |
| 1bbb:022c | T & A M... | HSPA+ USB Modem              | 3     | usbseri... | A348E53594 |
| 03f0:1e1d | Hewlett... | hs2300 HSDPA Broadband Wi... | 2     | sierra     | 31732E50F6 |
| 03f0:201d | Hewlett... | un2400 Gobi Wireless Mode... | 2     | qcserial   | 0DB5880016 |
| 04da:250c | Panason... | Gobi Wireless Modem (QDL ... | 2     | qcserial   | FDCC1DFB81 |
| 05c6:9008 | Qualcomm   | Gobi Wireless Modem (QDL ... | 2     | qcserial   | 985F45DA27 |
| 091e:0003 | Garmin ... | GPS (various models)         | 2     | garmin_gps | 037CAC7A3D |
| 1bbb:00b7 | T & A M... | HSPA Data Card               | 2     | option     | E6C9D1B45A |
| 045e:00ce | Microsoft  | SiRF GPS HH                  | 1     | ipaq       | A587AAD1F9 |
| 0557:2008 | ATEN In... | UC-232A Serial Port [pl2303] | 1     | pl2303     | 09E1F3C46D |
| 05c6:9221 | Qualcomm   | Gobi Wireless Modem (QDL ... | 1     | qcserial   | F46BDC061A |
| 06cd:0121 | Keyspan    | USA-19hs serial adapter      | 1     | keyspan    | FB2C692CC5 |
| 0d9f:0002 | Powercom   | Black Knight PRO / WOW Un... | 1     | cypress... | 1749B0B757 |
| 0f3d:68aa | Airprim... | AirCard 313U                 | 1     | sierra     | DB976F7ED5 |
| 0fcf:1008 | Dynastr... | ANTUSB2 Stick                | 1     | usb_ser... | 8D11089A12 |
| 0fcf:1009 | Dynastr... | ANTUSB-m Stick               | 1     | usb_ser... | C96EB40E5F |
| 114f:68a2 | Wavecom    | MC7750                       | 1     | qcserial   | 3479D8614B |
| 1199:0020 | Sierra ... | MC5725 Modem                 | 1     | sierra     | D2C42C6C14 |
| 1199:0218 | Sierra ... | MC5720 Wireless Modem        | 1     | sierra     | F7A90892DC |
| 1199:6812 | Sierra ... | MC8775 Device                | 1     | sierra     | 527DEFC159 |
| 1199:6832 | Sierra ... | MC8780 Device                | 1     | sierra     | 6639C529C5 |
| 1199:6856 | Sierra ... | ATT "USB Connect 881"        | 1     | sierra     | 5A1CA7D377 |
| 1199:6890 | Sierra ... | AC504                        | 1     | sierra     | 83B675C99C |
| 1199:68a3 | Sierra ... | MC8700 Modem                 | 1     | sierra     | 55167C5D7C |
| 1199:68c0 | Sierra ... | MC7304                       | 1     | qcserial   | 787245DA69 |
| 12d1:1569 | Huawei ... | EM680 w/Gobi Technology      | 1     | option     | F05FDF7D0F |
| 12d1:15c1 | Huawei ... | ME906s LTE M.2 Module        | 1     | option     | C61C8F1ABC |
| 1410:2110 | Novatel... | Ovation U720/MCD3000         | 1     | option     | 441D60EA88 |
| 1410:2410 | Novatel... | Expedite EU740               | 1     | option     | 4938E3EC42 |
| 1410:2420 | Novatel... | Expedite EU850D/EU860D/EU... | 1     | option     | EE82411E6C |
| 19d2:0117 | ZTE WCD... | WCDMA Technologies MSM       | 1     | option     | 432E535ED8 |
| 413c:8116 | Dell       | Wireless 5505 Mobile Broa... | 1     | option     | 6296C79EF5 |
| 413c:8138 | Dell       | Wireless 5520 Voda I Mobi... | 1     | option     | F88C2A43B9 |
| 6547:0232 | Arkmicr... | ARK3116 Serial               | 1     | ark3116    | B43383EB91 |

### Smartcard reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 072f:2100 | Advance... | ACR128U                      | 1     |            | DC6D6E889A |

### Sound (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:4014 | Realtek... | USB Audio                    | 15    | snd_usb... | 5D2CB1E1B0 |
| 0d8c:013c | C-Media... | CM108 Audio Controller       | 14    | snd_usb... | 67B170F4F4 |
| 0d8c:0014 | C-Media... | Audio Adapter (Unitek Y-2... | 6     | snd_usb... | E9ADA78450 |
| 046d:0a44 | Logitech   | Wired headset H390           | 5     | snd_usb... | 9CF46196FA |
| 17ef:306a | Lenovo     | ThinkPad Thunderbolt 3 Do... | 5     | snd_usb... | C15E0482CA |
| 1b3f:2008 | General... | USB Audio Device             | 5     | snd_usb... | 4E9E1B53EE |
| 041e:30df | Creativ... | SB X-Fi Surround 5.1 Pro     | 4     | snd_usb... | BE2EC9D692 |
| 08bb:2912 | Texas I... | PCM2912A Audio Codec         | 4     | snd_usb... | BF4A9DEE07 |
| 0d8c:0126 | C-Media... | USB Audio Device             | 4     | snd_usb... | 87AFEF045F |
| 1210:000a | DigiTech   | Lexicon Alpha                | 4     | snd_usb... | BD2B6260C9 |
| 1852:7022 | GYROCOM... | Grant Fidelity dac-11        | 4     | snd_usb... | 0C958765CA |
| 041e:3237 | Creativ... | SB X-Fi Surround 5.1 Pro     | 3     | snd_usb... | 748C0A2FA7 |
| 046d:0a29 | Logitech   | H600 [Wireless Headset]      | 3     | snd_usb... | 97DC44DC84 |
| 046d:0a38 | Logitech   | Headset H340                 | 3     | snd_usb... | D0634D2728 |
| 046d:0a4d | Logitech   | G430 Surround Sound Gamin... | 3     | snd_usb... | 57349A1931 |
| 08bb:2704 | Texas I... | PCM2704 16-bit stereo aud... | 3     | snd_usb... | A12FF60DCD |
| 0d8c:000c | C-Media... | Audio Adapter                | 3     | snd_usb... | 7F239B5732 |
| 0d8c:0102 | C-Media... | CM106 Like Sound Device      | 3     | snd_usb... | 8B270D4228 |
| 0d8c:0103 | C-Media... | CM102-A+/102S+ Audio Cont... | 3     | snd_usb... | E8FE24FA20 |
| 0d8c:0105 | C-Media... | CM108 Audio Controller       | 3     | snd_usb... | D21BC7358D |
| 8086:0808 | Intel      | USB PnP Sound Device         | 3     | snd_usb... | 417000B97B |
| 041e:30d3 | Creativ... | Sound Blaster Play!          | 2     | snd_usb... | E6D790C032 |
| 041e:30dd | Creativ... | Sound Blaster X-Fi Go! Pro   | 2     | snd_usb... | 802D03D867 |
| 041e:324d | Creativ... | Sound Blaster Play! 3        | 2     | snd_usb... | 331420F489 |
| 045e:070f | Microsoft  | LifeChat LX-3000 Headset     | 2     | snd_usb... | 908C7999DB |
| 047f:c010 | Plantro... | GameCom 780                  | 2     | snd_usb... | CD59CC78C0 |
| 047f:c014 | Plantro... | .Audio 622 USB               | 2     | snd_usb... | A78D2DC653 |
| 0572:1804 | Conexan... | HP Dock Audio                | 2     | snd_usb... | 3240076AA6 |
| 0763:2012 | M-Audio    | M-Audio Fast Track Pro       | 2     | snd_usb... | 72E1B5D4A9 |
| 08bb:2902 | Texas I... | PCM2902 Audio Codec          | 2     | snd_usb... | B98CC2C5BC |
| 09da:2701 | A4Tech     | Bloody Gaming Audio Device   | 2     | snd_usb... | 8D5F16B3AC |
| 0a12:1243 | Cambrid... | CSRA64110 USB Audio          | 2     |            | 2A3F7B30CC |
| 0c76:161f | JMTek      | USB PnP Audio Device         | 2     | snd_usb... | 57C89FEBD9 |
| 0d8c:0012 | C-Media... | USB Audio Device             | 2     | snd_usb... | 565DF74D4A |
| 0d8c:0021 | C-Media... | USB Advanced Audio Device    | 2     | snd_usb... | 063C5CE7E2 |
| 0d8c:0024 | C-Media... | USB Advanced Audio Device    | 2     | snd_usb... | AC3CF92791 |
| 17ef:3049 | Lenovo     | ThinkPad OneLink Dock USB... | 2     | snd_usb... | EE8564DC35 |
| 1852:7921 | GYROCOM... | Audiotrak ProDigy CUBE       | 2     | snd_usb... | 0BA17818C4 |
| 1b3f:2007 | General... | USB Audio Device             | 2     | snd_usb... | 94058A82CA |
| 413c:a503 | Dell       | AC511 USB SoundBar           | 2     | snd_usb... | 02B9759D77 |
| 03f0:0269 | Hewlett... | USB Audio                    | 1     | snd_usb... | 0C96DDC649 |
| 041e:0004 | Creativ... | USB Speaker                  | 1     | snd_usb... | B2DF76FA94 |
| 041e:0414 | Creativ... | HS-720 Headset               | 1     | snd_usb... | 3E31BE830B |
| 041e:30d7 | Creativ... | USB Sound Blaster HD         | 1     | snd_usb... | FDA82526F9 |
| 046d:08c6 | Logitech   | QuickCam for DELL Notebooks  | 1     | snd_usb... | C4448E72DA |
| 046d:0a07 | Logitech   | Z-10 Speakers                | 1     | snd_usb... | CCB7142AC0 |
| 046d:0a10 | Logitech   | Speaker                      | 1     | snd_usb... | F753FD41A8 |
| 046d:0a15 | Logitech   | G35 Headset                  | 1     | snd_usb... | 006708BEC5 |
| 046d:0a1a | Logitech   | Speaker Lapdesk N700         | 1     | snd_usb... | C4026BE662 |
| 046d:0a24 | Logitech   | Speaker Lapdesk N550         | 1     | snd_usb... | DFEBD3D19E |
| 046d:0a37 | Logitech   | USB Headset H540             | 1     | snd_usb... | 2064D92892 |
| 046d:0a5b | Logitech   | G933 Wireless Headset Dongle | 1     | snd_usb... | 8B627F307E |
| 046d:0a5c | Logitech   | G633 Gaming Headset          | 1     | snd_usb... | A2290674F8 |
| 0471:2118 | Philips... | SHG7980                      | 1     | snd_usb... | BC5D100BBF |
| 047f:02f7 | Plantro... | BT600                        | 1     | snd_usb... | 4D5087B262 |
| 047f:aa01 | Plantro... | C620-M                       | 1     | snd_usb... | F887B76EAB |
| 047f:c008 | Plantro... | Audio 655 DSP                | 1     | snd_usb... | 593D3F6DCA |
| 047f:c011 | Plantro... | .Audio 478 USB               | 1     | snd_usb... | 92ADB62CE7 |
| 047f:c012 | Plantro... | .Audio 628 USB               | 1     | snd_usb... | D43361263C |
| 047f:c013 | Plantro... | .Audio 648 USB               | 1     | snd_usb... | 1A2F1A6717 |
| 047f:c023 | Plantro... | C310-M                       | 1     | snd_usb... | A7AF1B2FB0 |
| 047f:c056 | Plantro... | Blackwire 3220 Series        | 1     | snd_usb... | 418C3E7B73 |
| 04c5:14a3 | Fujitsu    | USB Audio                    | 1     | snd_usb... | E540F9D719 |
| 04c5:1570 | Fujitsu    | USB Audio                    | 1     | snd_usb... | 0E5871ABB5 |
| 04d8:fb56 | Microch... | FUNcube Dongle V1.0          | 1     | snd_usb... | 148AAF71EB |
| 04e8:686c | Samsung... | Android                      | 1     | snd_usb... | 9A5BEC0B95 |
| 054c:09cc | Sony       | DualShock 4 [CUH-ZCT2x]      | 1     | snd_usb... | 6E5F98DA72 |
| 0572:1400 | Conexan... | USB Audio                    | 1     | snd_usb... | 9A145A3041 |
| 0582:0052 | Roland     | EDIROL UM-1SX                | 1     | snd_usb... | 5C5DC64FA1 |
| 05ac:2912 | Apple      | USB audio device             | 1     | snd_usb... | AA03FE7442 |
| 06f8:2100 | Guillemot  | XPS DIAMOND 2.0 USB          | 1     | snd_usb... | 5D64997B98 |
| 06f8:b000 | Guillemot  | Hercules DJ Console          | 1     | snd_usb... | 76659D6F7A |
| 06f8:b109 | Guillemot  | Hercules DJ Console 4-Mx     | 1     | snd_usb... | AB3B50FF87 |
| 0763:2013 | Midiman    | M-Audio JamLab               | 1     | snd_usb... | 4BB18A30E8 |
| 0763:202a | Midiman    | MobilePre                    | 1     | snd_usb... | D1CDAB3074 |
| 07ca:0036 | AVerMed... | AVerTV USB 2.0 Plus          | 1     | snd_usb... | 6CCD41E3D2 |
| 08bb:27c4 | Texas I... | PCM2704C stereo audio DAC    | 1     | snd_usb... | C284096B2B |
| 08bb:29c0 | Texas I... | PCM2900C Audio CODEC         | 1     | snd_usb... | 26FBFEC0EA |
| 093a:2625 | Pixart ... | Multimedia audio controller  | 1     | gspca_p... | 6BADAF723C |
| 0a92:0054 | EGO SYS... | Dr. DAC nano                 | 1     | snd_usb... | C890FCA2C0 |
| 0b05:17a0 | ASUSTek... | Xonar U3 sound card          | 1     | snd_usb... | 6006F3386E |
| 0b05:7778 | ASUSTek... | Android                      | 1     | snd_usb... | ABD3BB72F0 |
| 0b0e:0050 | GN Netcom  | Jabra LINK 220a USB          | 1     | snd_usb... | 2BB43D69D6 |
| 0b0e:0306 | GN Netcom  | Jabra EVOLVE LINK            | 1     | snd_usb... | B037BD5050 |
| 0b0e:0312 | GN Netcom  | Jabra EVOLVE 30 II           | 1     | snd_usb... | 9D1947C3A7 |
| 0b0e:0343 | GN Netcom  | Jabra UC VOICE 150a          | 1     | snd_usb... | CCCD96C6F3 |
| 0b0e:0349 | GN Netcom  | Jabra UC VOICE 550a          | 1     | snd_usb... | 4C056DECEA |
| 0b0e:034d | GN Netcom  | Jabra UC VOICE 750a          | 1     | snd_usb... | 270133C428 |
| 0b0e:0412 | GN Netcom  | Jabra SPEAK 410 USB          | 1     | snd_usb... | AACC137FAA |
| 0b0e:0422 | GN Netcom  | Jabra SPEAK 510 USB          | 1     | snd_usb... | ACCBBD5508 |
| 0b0e:1016 | GN Netcom  | Jabra PRO 930                | 1     | snd_usb... | 1EFF42042A |
| 0b0e:1052 | GN Netcom  | Jabra DIAL 550               | 1     | snd_usb... | 4765B7AED1 |
| 0b0e:2302 | GN Netcom  | Jabra BIZ 2300               | 1     | snd_usb... | BBFDF26E6C |
| 0b0e:245e | GN Netcom  | Jabra Link 370               | 1     | snd_usb... | F76CACECA0 |
| 0bda:4813 | Realtek... | USB Audio                    | 1     | snd_usb... | EA8F4068E3 |
| 0c76:1617 | JMTek      | USB PnP Audio Device         | 1     | snd_usb... | FF2A67C355 |
| 0c76:1665 | JMTek      | GS3                          | 1     | snd_usb... | 9104464264 |
| 0d8c:0001 | C-Media... | Audio Device                 | 1     | snd_usb... | D2B02AE1EC |
| 0d8c:0002 | C-Media... | Composite Device             | 1     | snd_usb... | A84223D35F |
| 0d8c:0008 | C-Media... | USB Audio Device             | 1     | snd_usb... | FBC2F0A547 |

### Touchpad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 06cb:2970 | Synaptics  | touchpad                     | 25    | usbhid     | 8D6D195DA2 |
| 06cb:5710 | Synaptics  | Touch Pad V 103S             | 2     | usbhid     | A3FFD5C89F |
| 06cb:5711 | Synaptics  | Touch Pad V 103u9            | 1     | usbhid     | 2D9527DBD4 |

### Touchscreen (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04f3:20d0 | Elan Mi... | Touchscreen                  | 8     | usbhid     | ECFA149223 |
| 04f3:000a | Elan Mi... | Touchscreen                  | 7     | usbhid     | 10D1795169 |
| 04f3:250e | Elan Mi... | Touchscreen                  | 7     | usbhid     | 8B699D7E6C |
| 04f3:0254 | Elan Mi... | Touchscreen                  | 6     | usbhid     | 83F2FF723D |
| 04f3:2494 | Elan Mi... | Touchscreen                  | 6     | usbhid     | 112EB80A9E |
| 04f3:012d | Elan Mi... | Touchscreen                  | 5     | usbhid     | 197C08E66F |
| 04f3:2013 | Elan Mi... | Touchscreen                  | 5     | usbhid     | 13E9034651 |
| 04f3:2234 | Elan Mi... | Touchscreen                  | 5     | usbhid     | 200F73880C |
| 04f3:016f | Elan Mi... | Touchscreen                  | 4     | usbhid     | 41A0E2A118 |
| 04f3:24a1 | Elan Mi... | Touchscreen                  | 4     | usbhid     | 65F14CA77F |
| 04f3:005a | Elan Mi... | Touchscreen                  | 3     | usbhid     | F95BBD54DA |
| 04f3:0074 | Elan Mi... | Touchscreen                  | 3     | usbhid     | 10FB53EFCA |
| 04f3:0085 | Elan Mi... | Touchscreen                  | 3     | usbhid     | 46820DB730 |
| 04f3:009d | Elan Mi... | Touchscreen                  | 3     | usbhid     | 26C2CAF634 |
| 04f3:010c | Elan Mi... | Touchscreen                  | 3     | usbhid     | AD06395996 |
| 04f3:036e | Elan Mi... | Touchscreen                  | 3     | usbhid     | C72380C4E8 |
| 04f3:20d6 | Elan Mi... | Touchscreen                  | 3     | usbhid     | CC12571867 |
| 0eef:0001 | D-WAV S... | eGalax TouchScreen           | 3     | usbhid     | 4A6A073320 |
| 04f3:0018 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 72500924FF |
| 04f3:002a | Elan Mi... | Touchscreen                  | 2     | usbhid     | D8DB450D73 |
| 04f3:0034 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 6C7804AC48 |
| 04f3:0117 | Elan Mi... | Touchscreen                  | 2     | usbhid     | A3FFD5C89F |
| 04f3:015d | Elan Mi... | Touchscreen                  | 2     | usbhid     | 44DC7D96C7 |
| 04f3:016c | Elan Mi... | Touchscreen                  | 2     | usbhid     | 35F9DBEE89 |
| 04f3:024b | Elan Mi... | Touchscreen                  | 2     | usbhid     | 18BB588ACC |
| 04f3:0348 | Elan Mi... | Touchscreen                  | 2     | usbhid     | C0DCEE7A80 |
| 04f3:0396 | Elan Mi... | Touchscreen                  | 2     | usbhid     | D5CD1F682E |
| 04f3:0406 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 5CAD0D6150 |
| 04f3:0418 | Elan Mi... | Touchscreen                  | 2     | usbhid     | F2DB78FEFC |
| 04f3:0422 | Elan Mi... | Touchscreen                  | 2     | usbhid     | FDCF5773E1 |
| 04f3:2021 | Elan Mi... | Touchscreen                  | 2     | usbhid     | C6BC466B79 |
| 04f3:2083 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 759D048AD1 |
| 04f3:2085 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 39115F2454 |
| 04f3:21f9 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 720FCA0EF9 |
| 04f3:228a | Elan Mi... | Touchscreen                  | 2     | usbhid     | 6CB660F70F |
| 04f3:228c | Elan Mi... | Touchscreen                  | 2     | usbhid     | 7611B00DAF |
| 04f3:22c3 | Elan Mi... | Touchscreen                  | 2     | usbhid     | AE39DC6976 |
| 04f3:2313 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 82C02D11DB |
| 04f3:2356 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 44219C33C7 |
| 04f3:24a0 | Elan Mi... | Touchscreen                  | 2     | usbhid     | 329485275D |
| 04f3:24d4 | Elan Mi... | Touchscreen                  | 2     | usbhid     | D302412809 |
| 04f3:2544 | Elan Mi... | Touchscreen                  | 2     | usbhid     | B4ED65654C |
| 04e7:0020 | Elo Tou... | Touchscreen Interface (2700) | 1     | usbhid     | 612620FA7A |
| 04f3:000c | Elan Mi... | Touchscreen                  | 1     | usbhid     | 8BF9F9DFC3 |
| 04f3:0017 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 7C82C563B9 |
| 04f3:0021 | Elan Mi... | Touchscreen                  | 1     | usbhid     | B0F9ABFE8D |
| 04f3:0022 | Elan Mi... | Touchscreen                  | 1     | usbhid     | FA5BBECCF2 |
| 04f3:0023 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 4DED2DA172 |
| 04f3:002f | Elan Mi... | Touchscreen                  | 1     | usbhid     | CA13E88F55 |
| 04f3:003d | Elan Mi... | Touchscreen                  | 1     | usbhid     | 9D8F98B831 |
| 04f3:007f | Elan Mi... | Touchscreen                  | 1     | usbhid     | 3E12B22705 |
| 04f3:0086 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 96108AA65D |
| 04f3:009b | Elan Mi... | Touchscreen                  | 1     | usbhid     | C6F56A5676 |
| 04f3:0125 | Elan Mi... | Touchscreen                  | 1     | usbhid     | E4421675DE |
| 04f3:012c | Elan Mi... | Touchscreen                  | 1     | usbhid     | EFCBC58CE9 |
| 04f3:012e | Elan Mi... | Touchscreen                  | 1     | usbhid     | D6A2D267C5 |
| 04f3:013b | Elan Mi... | Touchscreen                  | 1     | usbhid     | 0F518ACFC3 |
| 04f3:0143 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 5834730131 |
| 04f3:0154 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 859AAD148A |
| 04f3:0155 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 106E86F531 |
| 04f3:0194 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 2D9527DBD4 |
| 04f3:0201 | Elan Mi... | Touchscreen                  | 1     | usbhid     | D703E6C3B3 |
| 04f3:0206 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 4202327E1A |
| 04f3:020b | Elan Mi... | Touchscreen                  | 1     | usbhid     | 37B056E670 |
| 04f3:020d | Elan Mi... | Touchscreen                  | 1     | usbhid     | F380930D83 |
| 04f3:021f | Elan Mi... | Touchscreen                  | 1     | usbhid     | E8CAF6C2A9 |
| 04f3:0220 | Elan Mi... | Touchscreen                  | 1     | usbhid     | C1DD7C5ECB |
| 04f3:0224 | Elan Mi... | Touchscreen                  | 1     | usbhid     | C4A77532B2 |
| 04f3:0248 | Elan Mi... | Touchscreen                  | 1     | usbhid     | D96AD40896 |
| 04f3:024d | Elan Mi... | Touchscreen                  | 1     | usbhid     | 56CBE06615 |
| 04f3:0256 | Elan Mi... | Touchscreen                  | 1     | usbhid     | CC3C253FCD |
| 04f3:0261 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 26E0937896 |
| 04f3:0263 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 802A559148 |
| 04f3:0266 | Elan Mi... | Touchscreen                  | 1     | usbhid     | FB0189FEEC |
| 04f3:0280 | Elan Mi... | Touchscreen                  | 1     | usbhid     | CCCDC8BB55 |
| 04f3:0303 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 72EBF685C2 |
| 04f3:034e | Elan Mi... | Touchscreen                  | 1     | usbhid     | 645368FC41 |
| 04f3:034f | Elan Mi... | Touchscreen                  | 1     | usbhid     | 88D364869F |
| 04f3:0363 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 66663BA5E9 |
| 04f3:036d | Elan Mi... | Touchscreen                  | 1     | usbhid     | 6F94FCD1C0 |
| 04f3:0389 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 50FFA5DCEC |
| 04f3:042f | Elan Mi... | Touchscreen                  | 1     | usbhid     | E15FF03F59 |
| 04f3:0436 | Elan Mi... | Touchscreen                  | 1     | usbhid     | A2974D854D |
| 04f3:0441 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 6B7CB439E3 |
| 04f3:2020 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 06EADCEDD8 |
| 04f3:2033 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 6A35A77419 |
| 04f3:2044 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 2533E6DD3D |
| 04f3:204c | Elan Mi... | Touchscreen                  | 1     | usbhid     | FE059A167E |
| 04f3:2070 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 910D564E8E |
| 04f3:207a | Elan Mi... | Touchscreen                  | 1     | usbhid     | ACD1BCBBC4 |
| 04f3:2084 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 15E96C4288 |
| 04f3:2088 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 4C8175225E |
| 04f3:2089 | Elan Mi... | Touchscreen                  | 1     | usbhid     | E9B3793CE9 |
| 04f3:2098 | Elan Mi... | Touchscreen                  | 1     | usbhid     | E1AC9A711F |
| 04f3:20aa | Elan Mi... | Touchscreen                  | 1     | usbhid     | 3F7F72D7A2 |
| 04f3:20ab | Elan Mi... | Touchscreen                  | 1     | usbhid     | 91BF899CC4 |
| 04f3:20b7 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 0AD47572C3 |
| 04f3:20cd | Elan Mi... | Touchscreen                  | 1     | usbhid     | ADBD3D0645 |
| 04f3:20d3 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 4C5CA8AAB3 |
| 04f3:20d8 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 1BCC05F783 |

### Tv card (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0402:5602 | ALi        | M5602 Video Camera Contro... | 23    | gspca_m... | A28F7B2623 |
| 046d:0896 | Logitech   | OrbiCam                      | 19    | gspca_v... | 1CE26C391A |
| 05e1:0501 | Syntek     | DC-1125 Webcam               | 19    | stkwebcam  | B304C61746 |
| 174f:a311 | Syntek     | 1.3MPixel Web Cam - Asus ... | 9     | stkwebcam  | C1DA7EE91F |
| 0c45:624f | Microdia   | PC Camera (SN9C201 + OV9650) | 6     | gspca_s... | 5B4BAFA432 |
| 045e:00f5 | Microsoft  | LifeCam VX-3000              | 4     | gspca_s... | E2791951A5 |
| 0ac8:c002 | Z-Star ... | Visual Communication Came... | 4     | gspca_v... | 8D7F285234 |
| 046d:08d7 | Logitech   | QuickCam Communicate STX     | 3     | gspca_z... | 9E5C916F89 |
| 045e:00f7 | Microsoft  | LifeCam VX-1000              | 2     | gspca_s... | A2290674F8 |
| 046d:089d | Logitech   | QuickCam E2500 series        | 2     | gspca_z... | 41B1AE7140 |
| 093a:2620 | Pixart ... | TV Card                      | 2     | gspca_p... | 26DE378A54 |
| 0ac8:303b | Z-Star ... | ZC0303 Webcam                | 2     | gspca_z... | 609E372F78 |
| 041e:4028 | Creativ... | Vista Plus cam [VF0090]      | 1     | gspca_p... | EFA320E41A |
| 041e:4061 | Creativ... | Live! Cam Notebook Pro [V... | 1     | gspca_o... | 9CFDE39B4B |
| 046d:0892 | Logitech   | OrbiCam                      | 1     | gspca_v... | 7CE7D6F7C3 |
| 046d:08d9 | Logitech   | QuickCam IM/Connect          | 1     | gspca_z... | 6234954C98 |
| 07ca:0889 | AVerMed... | AVerTV Satellite 2           | 1     |            | 174EC665C6 |
| 093a:2470 | Pixart ... | SoC PC-Camera                | 1     | gspca_p... | A28F7B2623 |
| 093a:2624 | Pixart ... | Webcam                       | 1     | gspca_p... | CB91962270 |
| 093a:262c | Pixart ... | TV Card                      | 1     | gspca_p... | D70072A75B |
| 0ac8:0321 | Z-Star ... | Vimicro generic vc0321 Ca... | 1     | gspca_v... | 1CF43D844D |
| 0ac8:301b | Z-Star ... | ZC0301 Webcam                | 1     | gspca_z... | 609E372F78 |
| 0ac8:307b | Z-Star ... | USB 1.1 Webcam               | 1     | gspca_z... | AF824F1950 |
| 0c45:6009 | Microdia   | VideoCAM ExpressII           | 1     | gspca_s... | 120E7702AF |
| 0c45:613a | Microdia   | PC Camera (SN9C120)          | 1     | gspca_s... | 418E8D9D45 |
| 1415:2000 | Nam Tai... | Sony Playstation Eye         | 1     | gspca_o... | D1DBC0C0E5 |
| 6000:dec1 | Beholde... | TV Voyage                    | 1     | tm6000     | 39E3030E0A |
| eb1a:2860 | eMPIA T... | USB 2860 Device              | 1     | em28xx     | 5764250D85 |

### Ups (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 051d:0002 | America... | Back-UPS Pro 500/1000/1500   | 1     | usbhid     | C7CF235523 |
| 051d:0003 | America... | UPS                          | 1     | usbhid     | AE0142F417 |
| 0764:0501 | Cyber P... | CP1500 AVR UPS               | 1     | usbhid     | 15AB3C7ABF |

### Video (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0fd9:0051 | Elgato ... | GameCapture HD               | 1     |            | 05F988930D |
| 1b80:a41c | Afatech    | Polaris AV Capture           | 1     |            | F20674C0B8 |
| 5555:3382 | Epiphan... | VGA2USB Frame Grabber        | 1     |            | 37A831391B |

### Webcam (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 145f:013c | Trust      | Multimedia audio controller  | 1     | gspca_p... | C52868D73E |

### Wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 2717:ff88 | Android    | Android                      | 5     | rndis_host | E50BACC566 |
| 0fce:71e8 | Sony Er... | F5321                        | 2     | rndis_host | 70D66C5819 |
| 12d1:1039 | Huawei ... | Ideos (tethering mode)       | 2     | rndis_host | CA1FB717A1 |
| 0fce:71aa | Sony Er... | Android                      | 1     | rndis_host | 0C9E0F4767 |
| 0fce:71ba | Sony Er... | D6603                        | 1     | rndis_host | 5974A74BFD |
| 1286:4e31 | Marvell... | Mobile Composite Device Bus  | 1     | rndis_host | 1FBA14F166 |
| 15a9:003a | Gemtek     | Modem YOTA 4G LTE            | 1     |            | 02E0DBC8D3 |
| 271d:90b5 | Android    |                              | 1     | rndis_host | 65345FD237 |
| 2916:f00e | Android    | Android                      | 1     | rndis_host | 79937B3042 |
| 2970:0004 | Fly        | Evo Chic 3                   | 1     | rndis_host | A83B92D6B0 |

### Others (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 06cb:009a | Synaptics  | Synaptics WBDI               | 30    |            | 497E22D203 |
| 138a:0097 | Validit... | Synaptics WBDI               | 18    |            | 259A32BE33 |
| 05ac:8300 | Apple      | Built-in iSight (no firmw... | 16    | isight_... | FDEA938323 |
| 2717:ff40 | MediaTek   | SDM636-MTP _SN:02BF3EA3      | 16    | usbfs      | C1DC3FE193 |
| 0e8d:2008 | MediaTek   | Impress_Luck                 | 13    | usbfs      | 9D975244E9 |
| 06cb:00a2 | Synaptics  | Synaptics WBDI               | 11    |            | CE2A6FB934 |
| 06cb:0081 | Synaptics  | Synaptics WBDI               | 10    |            | 878BBF3E54 |
| 138a:00ab | Validit... | Synaptics VFS7552 Touch F... | 10    |            | 0C96DDC649 |
| 22b8:2e82 | Motorol... | Moto Z (2)                   | 6     | usbfs      | 3831423C95 |
| 0af0:7601 | Option     | Globetrotter MO40x 3G Mod... | 5     | hso        | 03DEB29A41 |
| 1934:5168 | Feature... | F71610A or F71612A Consum... | 4     | mceusb     | B7561ADE6F |
| 0489:e031 | Foxconn... | BCM20702A0                   | 3     | btusb      | 8F5D5F5C26 |
| 0489:e080 | Foxconn... | BT                           | 3     |            | 0E146447BF |
| 04b4:5217 | Cypress... | Billboard Device             | 3     | usbhid     | BF4A9DEE07 |
| 054c:01bb | Sony       | FeliCa S320 [PaSoRi]         | 3     |            | F3208D8466 |
| 06cb:00c7 | Synaptics  |                              | 3     |            | 94C3F1BC72 |
| 0e8d:201d | MediaTek   | Tele2_Maxi_LTE               | 3     |            | 70DE2F36AE |
| 18d1:4ee7 | Google     | ZTE A2017U                   | 3     |            | A2BCE1F421 |
| 045e:02d1 | Microsoft  | Xbox One Controller          | 2     | xpad       | 892ED196EE |
| 0483:3748 | STMicro... | ST-LINK/V2                   | 2     |            | 5DB2420E7B |
| 04f3:0c07 | Elan Mi... | ELAN:Fingerprint             | 2     |            | A0CF277545 |
| 06cb:00a8 | Synaptics  |                              | 2     |            | 0CAC406018 |
| 0b05:7772 | ASUSTek... | ASUS Zenfone GO (ZB500KL)... | 2     | usbfs      | 2BC400D84F |
| 17e9:6000 | Display... | USB 3.0 5K Graphic Docking   | 2     | cdc_ncm    | 4CED599618 |
| 187f:0600 | Siano M... | MDTV Receiver                | 2     | smsusb     | 4422093EAE |
| 18d1:4ee2 | Google     | Nexus Device (debug)         | 2     |            | 566F024B92 |
| 18d1:d00d | Google     | Android                      | 2     |            | C8033B809F |
| 19d2:0244 | ZTE WCD... | Android                      | 2     |            | 0D9A8C2133 |
| 1d5c:5100 | Fresco ... | Generic Billboard Device     | 2     |            | B2B217C7BA |
| 1fc9:5002 | NXP Sem... | PTN5002                      | 2     |            | 24A04CA275 |
| 2109:0102 | VIA Labs   | USB 2.0 BILLBOARD            | 2     |            | 32E6709E79 |
| 22b8:2e76 | Motorol... | moto e5 play                 | 2     | usbfs      | 55CD396670 |
| 27c6:538c | Goodix     | FingerPrint                  | 2     |            | 1E82DEB58E |
| 27c6:5584 | Generic    | Goodix FingerPrint Device    | 2     |            | 2DE7F8E811 |
| 2970:2008 | Mediatek   | IQ4418                       | 2     | usbfs      | 14BAB68F41 |
| 2ae5:f003 | Fairphone  | Android                      | 2     | usbfs      | E191D48B36 |
| 03c3:120d | ZWO        | ASI120MM-S                   | 1     |            | 5278A91530 |
| 03eb:2104 | Atmel      | AVR ISP mkII                 | 1     |            | 664332711F |
| 03f0:0667 | Hewlett... | WinUSB                       | 1     | usbhid     | 0C96DDC649 |
| 03f0:434a | Hewlett... | USB-C HDMI 2.0 Adapter       | 1     |            | 8B517CA2E8 |
| 03f0:5c1d | Hewlett... | 8 G2                         | 1     |            | 6B2EF56671 |
| 041e:4055 | Creativ... | Live! Cam Video IM Pro       | 1     |            | 830568FEB4 |
| 0421:0720 | Nokia M... | X (RM-980)                   | 1     |            | C02B0CEA7D |
| 0424:2740 | Standar... | Hub Controller               | 1     |            | D906E8F498 |
| 0451:e001 | Texas I... | GraphLink [SilverLink]       | 1     |            | FA99AE0DA3 |
| 0458:2019 | KYE Sys... | ColorPage-HR6X Slim          | 1     |            | 6271451326 |
| 045e:02f9 | Microsoft  | Xbox Embedded Wireless       | 1     |            | 5912BCA027 |
| 0471:060c | Philips... | Consumer Infrared Transce... | 1     | mceusb     | 13E0F20FD0 |
| 0489:e096 | Foxconn... | BCM43142A0                   | 1     | btusb      | 94FF3D26D8 |
| 04b4:0004 | Cypress... | USB-Serial (Single Channel)  | 1     |            | 0576000875 |
| 04b4:00fa | Cypress... | USBSC MFG                    | 1     |            | 2933DDC278 |
| 04b4:1002 | Cypress... | CY7C63001 R100 FM Radio      | 1     | dsbr100    | 4A6A073320 |
| 04b8:0136 | Seiko E... | ES-D400 [GT-S80]             | 1     |            | 351DEDF708 |
| 04b8:0156 | Seiko E... | ES-400                       | 1     | usbfs      | 3B4014A4BB |
| 04ca:f016 | Lite-On... | TVT-1060                     | 1     |            | 384CBE7132 |
| 04d8:e11c | Microch... | TL866CS EEPROM Programmer... | 1     |            | 0432DDBB6C |
| 04da:250e | Panason... | Qualcomm Gobi 2000           | 1     |            | 73DBF23913 |
| 0547:4018 | Anchor ... | AmScope MU1803               | 1     |            | C3258F5B65 |
| 0572:c68a | Conexan... | EyeTV Stick                  | 1     | dvb_usb... | 4863E51684 |
| 0582:0096 | Roland     | EDIROL UA-1EX                | 1     | snd_usb... | 20677BC8F3 |
| 05ac:1460 | Apple      |                              | 1     |            | 606C70C0DB |
| 0609:0338 | SMK Man... | MCE RECEIVER Emulator Dev... | 1     | mceusb     | 4052714D62 |
| 067b:aaa3 | Prolifi... | PL2303x Serial Adapter       | 1     |            | 9D5A19DCF4 |
| 06cb:007a | Synaptics  |                              | 1     |            | D4C31F7792 |
| 06cb:009b | Synaptics  |                              | 1     |            | 7AE8288182 |
| 06cb:00bd | Synaptics  |                              | 1     |            | 93D5C57FFC |
| 0711:5100 | Magic C... | Magic Control Technology ... | 1     |            | 67EFA34926 |
| 0711:5804 | Magic C... | MCT USB3.0 External Graph... | 1     |            | E2BA57AC38 |
| 0763:2806 | M-Audio    | M-Audio Transit DFU          | 1     |            | 8D0745E388 |
| 07ca:1827 | AVerMed... | AVerTV                       | 1     |            | 0CB6B8D2E6 |
| 07ca:3867 | AVerMed... | A867                         | 1     |            | 159AB17857 |
| 07ca:a110 | AVerMed... | TD110                        | 1     | dvb_usb... | 92200DFCBA |
| 07ca:a301 | AVerMed... | AVerTV                       | 1     |            | CF3A7AD203 |
| 07d0:4100 | Dazzle     | Kingsun SF-620 Infrared A... | 1     | ksdazzl... | 421C474023 |
| 0835:2a01 | Action ... | BILLBOARD DEVICE             | 1     |            | 9CF289ACE8 |
| 0846:9055 | NetGear    | A6150                        | 1     |            | CF32C265C9 |
| 085c:0400 | ColorVi... | Spyder 4                     | 1     |            | 21729711F3 |
| 0955:0024 | Nvidia     | BenQ Q41 Series              | 1     |            | F248142F16 |
| 0955:7321 | Nvidia     | APX                          | 1     |            | 09CAC84221 |
| 09fb:6001 | Altera     | Blaster                      | 1     |            | 770C1DB77C |
| 0a5c:640b | Broadcom   | BCM20702A0                   | 1     | btusb      | 9028FF8E63 |
| 0a82:6620 | Syscan     | TravelScan 662               | 1     |            | 0AA32C1479 |
| 0af0:7211 | Option     | Globetrotter HSUPA Modem     | 1     | hso        | 2CFFF15DFF |
| 0b05:561f | ASUSTek... | Android                      | 1     |            | 2BD69A999F |
| 0b05:7770 | ASUSTek... | Android                      | 1     |            | 2227196AFC |
| 0b05:7771 | ASUSTek... | Android                      | 1     |            | 0D36FA2146 |
| 0bda:5400 | Realtek... | BillBoard Device             | 1     |            | EA8F4068E3 |
| 0bda:5442 | Realtek... | Cable Matters USB-C Video... | 1     |            | B7B6BDE9C2 |
| 0c72:000c | PEAK Sy... | PCAN-USB                     | 1     | peak_usb   | 49D8F9A43E |
| 0ccd:00d7 | TerraTe... | RTL2838UHIDIR                | 1     | dvb_usb... | FBBFE649A4 |
| 0da4:0010 | Polar E... | Polar M600                   | 1     |            | 8F1CFE4955 |
| 0e6f:1314 | Logic3     | Afterglow Wireless Contro... | 1     | xpad       | EC06650BC6 |
| 0e79:544a | Archos     | Archos                       | 1     | usbfs      | EA5862A6BA |
| 0e8d:201c | MediaTek   | M5                           | 1     |            | 5825DE04E9 |
| 0e8d:7650 | MediaTek   | MediaTek                     | 1     |            | 00F329DACA |
| 0fce:0186 | Sony Er... | LT25i                        | 1     |            | E27FD79A69 |
| 0fce:0193 | Sony Er... | Xperia Z                     | 1     |            | F08B28F23F |
| 0fce:0195 | Sony Er... | C5303                        | 1     |            | 2D9126A5BF |
| 0fce:01a7 | Sony Er... | D5503                        | 1     |            | FA6690A21B |
| 0fce:01b8 | Sony Er... | Xperia M2                    | 1     |            | 9FA35C9053 |

