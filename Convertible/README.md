Most popular USB devices in Convertibles
========================================

See more info in the [README](https://github.com/linuxhw/LsUSB).

Contents
--------

1. [ USB Devices in Convertibles ](#usb-devices)
   * [ Bluetooth ](#bluetooth-usb)
   * [ Camera ](#camera-usb)
   * [ Card reader ](#card-reader-usb)
   * [ Cdrom ](#cdrom-usb)
   * [ Disk ](#disk-usb)
   * [ Fingerprint reader ](#fingerprint-reader-usb)
   * [ Gamepad ](#gamepad-usb)
   * [ Hub ](#hub-usb)
   * [ Human interface ](#human-interface-usb)
   * [ Input/keyboard ](#inputkeyboard-usb)
   * [ Input/mouse ](#inputmouse-usb)
   * [ Net/wireless ](#netwireless-usb)
   * [ Network ](#network-usb)
   * [ Printer ](#printer-usb)
   * [ Serial controller ](#serial-controller-usb)
   * [ Sound ](#sound-usb)
   * [ Touchscreen ](#touchscreen-usb)
   * [ Others ](#others-usb)

USB Devices
-----------

Count  — number of computers with this device installed,
Driver — driver in use for this device,
Probe  — latest probe ID of this device.

### Bluetooth (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8087:0a2b | Intel      | Bluetooth Device             | 29    | btusb      | 323C96D0DE |
| 8087:0a2a | Intel      | Bluetooth Device             | 22    | btusb      | 7EB57A3809 |
| 0bda:b00a | Realtek... | Bluetooth Radio              | 8     | btusb      | 2F6DFEC51C |
| 0bda:b00b | Realtek... | Bluetooth Radio              | 6     | btusb      | 314C52C853 |
| 8087:0aaa | Intel      | Bluetooth Device 9460/9560   | 5     | btusb      | 74DD313167 |
| 0cf3:e300 | Qualcom... | Qualcomm Atheros Bluetoot... | 4     | btusb      | 941DF897AA |
| 0cf3:e360 | Qualcom... | Qualcomm Atheros Bluetoot... | 4     | btusb      | 5B8D494C04 |
| 0bda:b009 | Realtek... | 802.11n WLAN Adapter         | 3     | btusb      | C4D6A7BC54 |
| 0bda:b023 | Realtek... | Bluetooth Radio              | 3     | btusb      | BC69AFD822 |
| 0cf3:e500 | Qualcom... | Qualcomm Atheros Bluetoot... | 3     | btusb      | F8913A087C |
| 8087:0025 | Intel      | Bluetooth Device             | 3     | btusb      | 83192A2228 |
| 0489:e0a2 | Foxconn... | Bluetooth Device             | 2     | btusb      | 62AF0556D3 |
| 0bda:b006 | Realtek... | Bluetooth Radio              | 2     | btusb      | 2DE25CD685 |
| 0a5c:216c | Broadcom   | BCM43142A0 Bluetooth Device  | 1     | btusb      | 07B5ECDCCD |
| 0a5c:6410 | Broadcom   | BCM20703A1 Bluetooth 4.1 ... | 1     | btusb      | C77AFE79C3 |
| 13d3:3491 | IMC Net... | Bluetooth Device             | 1     | btusb      | 2C6BCC75CC |
| 13d3:3526 | IMC Net... | Bluetooth Radio              | 1     | btusb      | 9565CCD6A2 |

### Camera (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04ca:707f | Lite-On... | HP Wide Vision HD Camera     | 8     | uvcvideo   | 2F6DFEC51C |
| 04f2:b634 | Chicony... | HP Wide Vision FHD Camera    | 3     | uvcvideo   | 74DD313167 |
| 058f:5608 | Alcor M... | USB 2.0 Web Camera           | 3     | uvcvideo   | 43E4578544 |
| 064e:3401 | Suyin      | HP TrueVision FHD RGB-IR     | 3     | uvcvideo   | 7E10A5E689 |
| 0bda:57c4 | Realtek... | HP Truevision HD             | 3     | uvcvideo   | 97ED13A8C0 |
| 0bda:58eb | Realtek... | HP Wide Vision HD Camera     | 3     | uvcvideo   | C4D6A7BC54 |
| 174f:2426 | Syntek     | Integrated Camera            | 3     | uvcvideo   | 52036BD95B |
| 5986:0711 | Acer       | EasyCamera                   | 3     | uvcvideo   | 5B8D494C04 |
| 0408:5251 | Quanta ... | HP Wide Vision FHD Camera    | 2     | uvcvideo   | CFDBBF139B |
| 0408:5300 | Quanta ... | HP Wide Vision HD Camera     | 2     | uvcvideo   | 7DEF867A41 |
| 04f2:b57e | Chicony... | EasyCamera                   | 2     | uvcvideo   | B9FC5DC357 |
| 04f2:b593 | Chicony... | HP Wide Vision FHD Camera    | 2     | uvcvideo   | 65B956E887 |
| 04f2:b5a4 | Chicony... | EasyCamera                   | 2     | uvcvideo   | 941DF897AA |
| 04f2:b5da | Chicony... | EasyCamera                   | 2     | uvcvideo   | C3AA237F35 |
| 04f2:b61e | Chicony... | Integrated Camera            | 2     | uvcvideo   | 1510224C03 |
| 04f2:b653 | Chicony... | HP Wide Vision HD Camera     | 2     | uvcvideo   | F720A735A8 |
| 04f2:b654 | Chicony... | HP IR Camera                 | 2     | uvcvideo   | F720A735A8 |
| 058f:3822 | Alcor M... | USB 2.0 Camera               | 2     | uvcvideo   | 56A7BF3D18 |
| 058f:3841 | Alcor M... | USB 2.0 PC Camera            | 2     | snd_usb... | E95114CACB |
| 05c8:0379 | Cheng U... | HP Truevision HD             | 2     | uvcvideo   | FD3B38A3F5 |
| 05c8:038e | Cheng U... | HP Wide Vision HD            | 2     | uvcvideo   | 84C8A9779B |
| 05c8:0396 | Cheng U... | HP IR Camera                 | 2     | uvcvideo   | AE7D79F749 |
| 05c8:080b | Cheng U... | HP HD Camera                 | 2     | uvcvideo   | AE7D79F749 |
| 0bda:58e6 | Realtek... | HP Wide Vision FHD Camera    | 2     | uvcvideo   | 22FE75A663 |
| 0bda:58f4 | Realtek... | Integrated_Webcam_HD         | 2     | uvcvideo   | 62AF0556D3 |
| 0bda:58f5 | Realtek... | 2SF001                       | 2     | uvcvideo   | 7EB57A3809 |
| 13d3:56a2 | IMC Net... | USB2.0 HD UVC WebCam         | 2     | uvcvideo   | 7BBD1AB6FE |
| 13d3:5a03 | IMC Net... | USB2.0 VGA UVC WebCam        | 2     | uvcvideo   | 9565CCD6A2 |
| 1bcf:2c7d | Sunplus... | HP Truevision Full HD        | 2     | uvcvideo   | C894E5633B |
| 00ca:5803 | Generic    | WEB CAMERA                   | 1     | uvcvideo   | 323C96D0DE |
| 0408:7040 | Quanta ... | RGB Camera                   | 1     | uvcvideo   | 7DA5C4A4FA |
| 0408:7080 | Quanta ... | IRcamera                     | 1     | uvcvideo   | 7DA5C4A4FA |
| 046d:085c | Logitech   | C922 Pro Stream Webcam       | 1     | snd_usb... | 65B956E887 |
| 04f2:b40e | Chicony... | HP Truevision HD camera      | 1     | uvcvideo   | 07B5ECDCCD |
| 04f2:b50d | Chicony... | HP Truevision HD             | 1     | uvcvideo   | 2DE25CD685 |
| 04f2:b568 | Chicony... | USB2.0 VGA UVC WebCam        | 1     | uvcvideo   | 93A44B1518 |
| 04f2:b56b | Chicony... | USB2.0 HD UVC WebCam         | 1     | uvcvideo   | F65847676C |
| 04f2:b57d | Chicony... | EasyCamera                   | 1     | uvcvideo   | F8913A087C |
| 04f2:b5ce | Chicony... | Integrated Camera            | 1     | uvcvideo   | 1D52B52B65 |
| 04f2:b5d6 | Chicony... | HP Wide Vision HD Camera     | 1     | uvcvideo   | 46D1308039 |
| 04f2:b604 | Chicony... | Integrated Camera            | 1     | uvcvideo   | DBA78E9C22 |
| 04f2:b627 | Chicony... | HP Wide Vision HD Camera     | 1     | uvcvideo   | C8C863DB3A |
| 04f2:b62f | Chicony... | HP Full-HD Camera            | 1     | uvcvideo   | 7F01433E42 |
| 04f2:b67a | Chicony... | LG HD WebCam                 | 1     | uvcvideo   | 6D2724E820 |
| 0585:3841 | FlashPo... | USB 2.0 Camera               | 1     | snd_usb... | 6663E08482 |
| 05ac:12a8 | Apple      | iPhone5/5C/5S/6              | 1     | ipheth     | B9FC5DC357 |
| 05c8:022a | Cheng U... | HP Webcam                    | 1     | uvcvideo   | BE4128010B |
| 05c8:03b1 | Cheng U... | HP HD Camera                 | 1     | uvcvideo   | 7FC2D9173E |
| 05c8:0815 | Cheng U... | HP Wide Vision FHD Camera    | 1     | uvcvideo   | 831911B060 |
| 090c:037c | Silicon... | 300k Pixel Camera            | 1     | uvcvideo   | 5E348A018A |
| 0bda:564b | Realtek... | WebCamera                    | 1     |            | 5EA0111CED |
| 0bda:57f8 | Realtek... | EasyCamera                   | 1     | uvcvideo   | A295EE15E6 |
| 0bda:5830 | Realtek... | USB Camera                   | 1     | uvcvideo   | 00D3EF2D8A |
| 0bda:58c8 | Realtek... | Integrated Webcam HD         | 1     | uvcvideo   | 57D57D82FE |
| 13d3:5621 | IMC Net... | EasyCamera                   | 1     | uvcvideo   | F445F1B326 |
| 13d3:5666 | IMC Net... | USB2.0 HD UVC WebCam         | 1     | uvcvideo   | FA7C5C4F64 |
| 13d3:5682 | IMC Net... | Integrated Camera            | 1     | uvcvideo   | 3AC5B5C4AD |
| 13d3:56b2 | IMC Net... | Integrated Camera            | 1     | uvcvideo   | 83192A2228 |
| 13d3:5755 | IMC Net... | USB2.0 VGA UVC WebCam        | 1     | uvcvideo   | 42FB487F36 |
| 13d3:584b | IMC Net... | Integrated Camera            | 1     | uvcvideo   | 0818236221 |
| 174f:241a | Syntek     | EasyCamera                   | 1     | uvcvideo   | 799D18044C |
| 1bcf:2b96 | Sunplus... | Integrated_Webcam_HD         | 1     | uvcvideo   | BC69AFD822 |
| 5986:210e | Acer       | EasyCamera                   | 1     | uvcvideo   | AC774199A8 |
| 5986:2115 | Acer       | Integrated Camera            | 1     | uvcvideo   | 67454C18A3 |

### Card reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0bda:0129 | Realtek... | RTS5129 Card Reader Contr... | 9     | rtsx_usb   | 56A7BF3D18 |

### Cdrom (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 8564:1000 | Transcend  | TS32GJF370 JetFlash 32GB     | 2     | uas, us... | 62AF0556D3 |
| 0b05:7774 | ASUSTek... | Zenfone GO (ZB500KL) (RND... | 1     | rndis_host | 42FB487F36 |
| 13fd:1040 | Initio     | INIC-1511L PATA Bridge       | 1     | usb_sto... | 799D18044C |
| 174c:55aa | ASMedia... | Name: ASM1051E SATA 6Gb/s... | 1     | uas        | 7E10A5E689 |

### Disk (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 090c:1000 | Silicon... | Flash Voyager                | 4     | uas, us... | C8C863DB3A |
| 058f:6387 | Alcor M... | Transcend JetFlash Flash ... | 2     | uas, us... | 7F01433E42 |
| 05e3:0749 | Genesys... | STORAGE DEVICE 8GB           | 2     | uas, us... | 74DD313167 |
| 0781:5571 | SanDisk    | Cruzer Fit 16GB              | 1     | uas, us... | 62A9EF950E |
| 0781:5575 | SanDisk    | Cruzer Glide 32GB            | 1     | uas, us... | D103A0F533 |
| 0781:5583 | SanDisk    | Ultra Fit 123GB              | 1     | uas, us... | F8913A087C |
| 0781:5591 | SanDisk    | Ultra Flair USB 3.0 124GB    | 1     | uas, us... | 54189E8E76 |
| 0930:6545 | Toshiba    | Kingston DataTraveler 102... | 1     | uas, us... | 9565CCD6A2 |
| 0bc2:ab24 | Seagate    | Backup Plus Portable Drive   | 1     | uas        | 2F95A6FDE1 |
| 0bda:0177 | Realtek... | SD/MMC/MS PRO 31GB           | 1     | ums_rea... | 93A44B1518 |
| 0bda:0328 | Realtek... | SD/MMC CRW                   | 1     | uas, us... | BC69AFD822 |
| 1058:0827 | Western... | My Passport 0827             | 1     | uas, us... | E7BCF2AD7C |
| 1058:083c | Western... | My Passport 083C             | 1     | uas, us... | E7BCF2AD7C |
| 1058:1110 | Western... | My Book Essential (WDBAAF... | 1     | uas, us... | D103A0F533 |
| 1058:259d | Western... | My Passport Ultra (WDBBKD)   | 1     | uas, us... | E20DBED4CE |
| 1058:25e1 | Western... | My Passport 25E1             | 1     | usb_sto... | 7E10A5E689 |
| 11b0:6298 | ATECH F... | SNA-DC/U 250GB               | 1     | uas, us... | 7EB57A3809 |
| 1307:0165 | Transcend  | 2GB/4GB/8GB Flash Drive 16GB | 1     | uas, us... | 6B3C9DB759 |
| 152d:0583 | JMicron... | GLOTRENDS USB3.1 NVME DISK   | 1     | uas        | 7E10A5E689 |
| 17ef:7435 | Lenovo     | A789 (Mass Storage mode, ... | 1     | uas, us... | 93A44B1518 |
| 2537:1081 | NORELSYS   | 1081 32GB                    | 1     | usb_sto... | 7E10A5E689 |
| 9570:0301 | Generic    | USB3.0 CRW -2                | 1     | usb_sto... | 7E10A5E689 |
| ffff:5678 | VendorCo   | ProductCode 16GB             | 1     | usb_sto... | 0D56BF6D3F |

### Fingerprint reader (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 138a:003f | Validit... | VFS495 Fingerprint Reader    | 2     | usbfs      | A9BAF3BF1B |
| 04f3:0903 | Elan Mi... | ELAN:Fingerprint             | 1     |            | FA7C5C4F64 |
| 06cb:0082 | Synaptics  | My Lockey                    | 1     |            | C77AFE79C3 |
| 138a:0017 | Validit... | VFS 5011 fingerprint sensor  | 1     |            | 3C1A4327F5 |
| 138a:0091 | Validit... | VFS7552 Touch Fingerprint... | 1     |            | 57D57D82FE |

### Gamepad (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0e6f:0201 | Logic3     | Gamepad                      | 1     | usbfs      | E20DBED4CE |

### Hub (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 1d6b:0002 | Linux F... | 2.0 root hub                 | 104   | hub        | 74DD313167 |
| 1d6b:0003 | Linux F... | 3.0 root hub                 | 103   | hub        | 74DD313167 |
| 05e3:0608 | Genesys... | USB-2.0 4-Port HUB           | 7     | hub        | DBA78E9C22 |
| 05e3:0610 | Genesys... | 4-port hub                   | 4     | hub        | 7E10A5E689 |
| 0bda:5411 | Realtek... | 4-Port USB 2.0 Hub           | 4     | hub        | 65B956E887 |
| 1a40:0101 | incompl... | 4-Port HUB                   | 4     | hub        | 8B7A2D2A08 |
| 2109:0817 | VIA Labs   | USB3.0 Hub                   | 4     | hub        | 74DD313167 |
| 2109:2817 | VIA Labs   | USB2.0 Hub                   | 4     | hub        | 74DD313167 |
| 0bda:0411 | Realtek... | 4-Port USB 3.0 Hub           | 3     | hub        | 65B956E887 |
| 0438:7900 | AMD        | Hub                          | 2     | hub        | B9FC5DC357 |
| 05e3:0620 | Genesys... | USB3.0 Hub                   | 2     | hub        | 7E10A5E689 |
| 2109:0813 | VIA Labs   | USB3.0 Hub                   | 2     | hub        | 7E10A5E689 |
| 2109:2813 | VIA Labs   | USB2.0 Hub                   | 2     | hub        | 7E10A5E689 |
| 03f0:0620 | Hewlett... | USB3.1 Hub                   | 1     | hub        | A462F23545 |
| 03f0:1847 | Hewlett... | USB2.1 Hub                   | 1     | hub        | A462F23545 |
| 0424:2514 | Standar... | USB 2.0 Hub                  | 1     | hub        | 7A4891D141 |
| 0424:2734 | Standar... | USB2734                      | 1     | hub        | 1D52B52B65 |
| 0424:5734 | Standar... | USB5734                      | 1     | hub        | 1D52B52B65 |
| 047d:8021 | Kensington | K38231_01                    | 1     | hub        | A9BAF3BF1B |
| 047d:8023 | Kensington | K38231_02                    | 1     | hub        | A9BAF3BF1B |
| 05e3:0617 | Genesys... | USB3.0 Hub                   | 1     | hub        | D103A0F533 |
| 08e4:01e7 | Pioneer    | USB-C PD Docking             | 1     | hub        | 323C96D0DE |
| 08e4:6504 | Pioneer    | USB-C PD Docking             | 1     | hub        | 323C96D0DE |
| 08e4:6506 | Pioneer    | USB-C PD Docking             | 1     | hub        | 323C96D0DE |
| 08e4:6508 | Pioneer    | USB-C PD Docking             | 1     | hub        | 323C96D0DE |
| 174c:2074 | ASMedia... | ASM1074 High-Speed hub       | 1     | hub        | A462F23545 |
| 174c:3074 | ASMedia... | ASM1074 SuperSpeed hub       | 1     | hub        | A462F23545 |
| 1a40:0201 | Terminu... | FE 2.1 7-port Hub            | 1     | hub        | 323C96D0DE |
| 2109:0210 | VIA Labs   | USB3.0 Hub                   | 1     | hub        | 42FB487F36 |
| 2109:2210 | VIA Labs   | USB2.0 Hub                   | 1     | hub        | 42FB487F36 |
| 2109:2811 | VIA Labs   | USB2.0 Hub                   | 1     | hub        | 8B7A2D2A08 |
| 2109:8110 | VIA Labs   | USB 3.0 Hub                  | 1     | hub        | 8B7A2D2A08 |
| 413c:1003 | Dell       | Keyboard Hub                 | 1     | hub        | D103A0F533 |
| 8087:8001 | Intel      | Hub                          | 1     | hub        | 462F2D5347 |

### Human interface (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 0483:91d1 | STMicro... | SGS Thomson Microelectronics | 6     | usbhid     | 97ED13A8C0 |
| 03eb:8b11 | Atmel      | maXTouch Digitizer           | 1     | usbhid     | 323C96D0DE |
| 0424:274c | Standar... | Hub Controller               | 1     | usbhid     | 1D52B52B65 |
| 048d:8350 | Integra... | ITE Device(8350)             | 1     | usbhid     | D9CA1DBE13 |
| 06c4:c411 | Bizlink... | D6000 Controller             | 1     | usbhid     | 57D57D82FE |
| 1050:0120 | Yubico.com | Yubikey Touch U2F Securit... | 1     | usbhid     | 7FC2D9173E |
| 17ef:3066 | Lenovo     | ThinkPad Thunderbolt 3 Do... | 1     | usbhid     | 3AC5B5C4AD |

### Input/keyboard (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 046d:c52b | Logitech   | Unifying Receiver            | 14    | usbhid     | 74DD313167 |
| 0911:2188 | Philips... | Hantick Wireless Device      | 5     | usbhid     | 5E348A018A |
| 046d:c534 | Logitech   | Unifying Receiver            | 3     | usbhid     | 7DEF867A41 |
| 2109:d101 | VIA Labs   | USB Keyboard                 | 2     | usbhid     | 62AF0556D3 |
| 03f0:064a | Hewlett... | USB Keyboard                 | 1     | usbhid     | A462F23545 |
| 046d:c318 | Logitech   | Illuminated Keyboard         | 1     | usbhid     | DBA78E9C22 |
| 05ac:0267 | Apple      | Magic Keyboard A1644         | 1     | usbhid     | 8B7A2D2A08 |
| 0951:16b7 | Kingsto... | HyperX Alloy FPS Mechanic... | 1     | usbhid     | 7E10A5E689 |
| 09da:9090 | A4Tech     | XL-730K / XL-750BK / XL-7... | 1     | usbhid     | 7A4891D141 |
| 1050:0116 | Yubico.com | Yubikey NEO(-N) OTP+U2F+CCID | 1     | usbhid     | 0818236221 |
| 1c4f:0063 | SiGma M... | USB Keyboard                 | 1     | usbhid     | FAB48926D9 |
| 2516:001a | Cooler ... | Keyboard -- QuickFire XT     | 1     | usbhid     | 323C96D0DE |
| 413c:2010 | Dell       | Keyboard                     | 1     | usbhid     | D103A0F533 |
| 413c:8505 | Dell       | Universal Receiver           | 1     | usbhid     | 7A4891D141 |

### Input/mouse (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04fc:05da | Sunplus... | SPEEDLINK SNAPPY Wireless... | 3     | usbhid     | 43E4578544 |
| 046d:c52f | Logitech   | Unifying Receiver            | 2     | usbhid     | 52036BD95B |
| 0458:003a | KYE Sys... | NetScroll+ Mini Traveler ... | 1     | usbhid     | 93A44B1518 |
| 045e:0797 | Microsoft  | Optical Mouse 200            | 1     | usbhid     | DBA78E9C22 |
| 046d:c062 | Logitech   | M-UAS144 [LS1 Laser Mouse]   | 1     | usbhid     | 57D57D82FE |
| 046d:c077 | Logitech   | M105 Optical Mouse           | 1     | usbhid     | 7E10A5E689 |
| 046d:c332 | Logitech   | G502 Proteus Spectrum Opt... | 1     | usbhid     | D103A0F533 |
| 047d:2048 | Kensington | Orbit Trackball with Scro... | 1     | usbhid     | B9FC5DC357 |
| 04d9:fa50 | Holtek ... | USB Laser Game Mouse         | 1     | usbhid     | CAD34C74AF |
| 04f3:0212 | Elan Mi... | Laser Mouse                  | 1     | usbhid     | 11EC0315D7 |
| 04f3:303d | Elan Mi... | ITE Device(8910)             | 1     | usbhid     | 7DA5C4A4FA |
| 056a:504a | Wacom      | Pen and multitouch sensor    | 1     | usbhid     | 3C1A4327F5 |
| 056a:5087 | Wacom      | Pen and multitouch sensor    | 1     | usbhid     | 3AC5B5C4AD |
| 056a:50b6 | Wacom      | Pen and multitouch sensor    | 1     | usbhid     | 1D52B52B65 |
| 056a:50d9 | Wacom      | Pen and multitouch sensor    | 1     | usbhid     | 7DA5C4A4FA |
| 056a:5155 | Wacom      | Pen and multitouch sensor    | 1     | usbhid     | 0818236221 |
| 056a:5157 | Wacom      | Pen and multitouch sensor    | 1     | usbhid     | DBA78E9C22 |
| 056a:51a6 | Wacom      | Pen and multitouch sensor    | 1     | usbhid     | 6D2724E820 |
| 10c4:8105 | Cygnal ... | USB OPTICAL MOUSE            | 1     | usbhid     | 8B7A2D2A08 |
| 13ee:0001 | MosArt     | Optical Mouse                | 1     | usbhid     | 48E3CC8030 |
| 15d9:0a4d | Trust I... | Optical Mouse                | 1     | usbhid     | 62AF0556D3 |
| 1bcf:0005 | Sunplus... | USB Optical Mouse            | 1     | usbhid     | 323C96D0DE |
| 1bcf:0007 | Sunplus... | Optical Mouse                | 1     | usbhid     | E721D571FE |
| 1c4f:0034 | SiGma M... | Usb Mouse                    | 1     | usbhid     | FAB48926D9 |
| 1ea7:0064 | SHARKOO... | 2.4G Wireless Mouse          | 1     | usbhid     | D9CA1DBE13 |
| 248a:8366 | Maxxter    | Wireless Optical Mouse AC... | 1     | usbhid     | E95114CACB |
| 3938:1146 | Blackweb   | Gaming Mouse                 | 1     | usbhid     | A06F0EE056 |
| 413c:3016 | Dell       | Optical 5-Button Wheel Mouse | 1     | usbhid     | FA7C5C4F64 |

### Net/wireless (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 148f:7601 | Ralink ... | MT7601U Wireless Adapter     | 3     | mt7601u    | 57DFE20895 |
| 0bda:8179 | Realtek... | RTL8188EUS 802.11n Wirele... | 2     | r8188eu    | 92B2AFA8F9 |
| 057c:8503 | AVM        | FRITZ!WLAN AC 860            | 1     | mt76x2u    | C77AFE79C3 |
| 0b05:17d2 | ASUSTek... | USB-AC56 802.11a/b/g/n/ac... | 1     |            | 2F6DFEC51C |
| 0bda:b812 | Realtek... | RTL8812BU USB3.0 802.11ac... | 1     |            | 799D18044C |

### Network (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04e8:6863 | Samsung... | GT-I9500 [Galaxy S4] / GT... | 6     | rndis_host | 4D93987E41 |
| 0bda:8153 | Realtek... | RTL8153 Gigabit Ethernet ... | 5     | r8152      | 74DD313167 |
| 0fe6:9700 | Kontron... | DM9601 Fast Ethernet Adapter | 4     | dm9601,... | BF2A311AD2 |
| 0b05:5f04 | ASUSTek... | Android                      | 3     | rndis_host | 43E4578544 |
| 04b4:3610 | Cypress... | USB-C PD Docking             | 2     | ax88179... | 323C96D0DE |
| 04e8:6864 | Samsung... | GT-I9070 (network tetheri... | 1     | rndis_host | 8952B7B287 |
| 0b95:1790 | ASIX El... | AX88179 Gigabit Ethernet     | 1     | ax88179... | DBA78E9C22 |
| 1199:9079 | Sierra ... | EM7455 Qualcomm Snapdrago... | 1     | qcseria... | 3AC5B5C4AD |
| 13b1:0041 | Linksys    | Gigabit Ethernet Adapter     | 1     | r8152      | 48E3CC8030 |
| 17e9:4307 | Display... | USB3.0 Dual Video Dock       | 1     | cdc_ncm    | 8B7A2D2A08 |
| 17e9:6006 | Display... | Dell Universal Dock D6000    | 1     | cdc_ncm... | 57D57D82FE |
| 17ef:3069 | Lenovo     | ThinkPad TBT3 LAN            | 1     | r8152      | 3AC5B5C4AD |
| 1bbb:2026 | T & A M... | ALCATEL ONETOUCH PIXI 3 (... | 1     | cdc_eem    | C4D6A7BC54 |

### Printer (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04f9:0075 | Brother... | HL-L2305 series              | 1     | usblp      | FA7C5C4F64 |

### Serial controller (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 03f0:a31d | Hewlett... | lt4132 LTE/HSPA+ 4G Module   | 1     | cdc_mbim   | 7F01433E42 |

### Sound (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 08bb:2912 | Texas I... | PCM2912A Audio Codec         | 2     | snd_usb... | 323C96D0DE |
| 045e:070f | Microsoft  | LifeChat LX-3000 Headset     | 1     | snd_usb... | A462F23545 |
| 0572:1804 | Conexan... | HP Dock Audio                | 1     | snd_usb... | A462F23545 |
| 0c76:161f | JMTek      | USB PnP Audio Device         | 1     | snd_usb... | 7E10A5E689 |
| 0d8c:0012 | C-Media... | USB Audio Device             | 1     | snd_usb... | D103A0F533 |
| 17ef:306a | Lenovo     | ThinkPad Thunderbolt 3 Do... | 1     | snd_usb... | 3AC5B5C4AD |

### Touchscreen (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 04f3:2070 | Elan Mi... | Touchscreen                  | 1     | usbhid     | D9CA1DBE13 |
| 04f3:2071 | Elan Mi... | Touchscreen                  | 1     | usbhid     | C894E5633B |
| 04f3:20a3 | Elan Mi... | Touchscreen                  | 1     | usbhid     | FD3B38A3F5 |
| 04f3:20a4 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 62A9EF950E |
| 04f3:2236 | Elan Mi... | Touchscreen                  | 1     | usbhid     | A06F0EE056 |
| 04f3:2278 | Elan Mi... | Touchscreen                  | 1     | usbhid     | 2DE25CD685 |
| 04f3:22fe | Elan Mi... | Touchscreen                  | 1     | usbhid     | FA7C5C4F64 |

### Others (USB)

| ID        | MFG        | Name                         | Count | Driver     | Probe      |
|-----------|------------|------------------------------|-------|------------|------------|
| 06cb:0081 | Synaptics  | Synaptics WBDI               | 9     |            | 83192A2228 |
| 138a:0094 | Validit... | Synaptics WBDI               | 3     |            | 941DF897AA |
| 04b4:5217 | Cypress... | Billboard Device             | 2     | usbhid     | 323C96D0DE |
| 06cb:00bb | Synaptics  |                              | 2     |            | 74DD313167 |
| 138a:0092 | Validit... | Synaptics VFS7552 Touch F... | 2     |            | AE7D79F749 |
| 138a:0097 | Validit... | Synaptics WBDI               | 2     |            | 3AC5B5C4AD |
| 2109:0102 | VIA Labs   | USB 2.0 BILLBOARD            | 2     |            | 74DD313167 |
| 06cb:00a2 | Synaptics  | Synaptics WBDI               | 1     |            | DBA78E9C22 |
| 0bda:5400 | Realtek... | BillBoard Device             | 1     |            | DBA78E9C22 |
| 138a:009d | Validit... | Synaptics WBDI               | 1     |            | 0818236221 |
| 138a:00ab | Validit... | Synaptics VFS7552 Touch F... | 1     |            | 7F01433E42 |
| 17e9:6000 | Display... | USB 3.0 5K Graphic Docking   | 1     | snd_usb... | 7E10A5E689 |
| 2109:0100 | VIA Labs   | USB 2.0 BILLBOARD            | 1     |            | 1D52B52B65 |
| 2109:0101 | VIA Labs   | USB 2.0 BILLBOARD            | 1     |            | 42FB487F36 |

